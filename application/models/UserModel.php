<?php

class UserModel extends CI_Model {

    function __construct() {
        $this->proTable = 'products';
        $this->custTable = 'users';
        $this->ordTable = 'orders';
        $this->ordItemsTable = 'order_items';
    }

    function getPropertyImg($propID) {
        $where = "propertyID = $propID AND propertyF = 'Y'";
        $this->db->select("*");
        $this->db->from('images');
        $this->db->where($where);
        //$this->db->limit(10, 0);
        //$this->db->order_by("id", "desc");
        $query = $this->db->get();
        return $query->result();
    }
     function getAllPropertyImg($propID) {
        $where = "propertyID = $propID";
        $this->db->select("*");
        $this->db->from('images');
        $this->db->where($where);
        //$this->db->limit(10, 0);
        //$this->db->order_by("id", "desc");
        $query = $this->db->get();
        return $query->result();
    }

    function getpropertWithURL($properyURL) {
        $where = "propertyURL = '$properyURL'";
        $this->db->select("*");
        $this->db->from('property');
        $this->db->where($where);
        //$this->db->limit(10, 0);
        //$this->db->order_by("id", "desc");
        $query = $this->db->get();
        return $query->result(); 
    }

    function getAllProducts($category) {
        $this->db->select("*");
        $this->db->from('products');
        $this->db->where('category', $category);
        //$this->db->limit(10, 0);	
        $this->db->order_by("id", "desc");
        $query = $this->db->get();
        return $query->result();
    }

    function getproductWithURL($url_slug) {
        $this->db->select("*");
        $this->db->from('products');
        $this->db->where('url', $url_slug);
        // $this->db->limit(10, 0);	
        //	$this->db->order_by("id", "desc");		
        $query = $this->db->get();
        return $query->result();
    }

    function getCategoryWithURL($url_slug) {
        $this->db->select("*");
        $this->db->from('category');
        $this->db->where(array('url' => $url_slug));
        // $this->db->limit(10, 0);	
        //	$this->db->order_by("id", "desc");		
        $query = $this->db->get();
        return $query->result();
    }

    function getAllData($table) {
        $this->db->select("*");
        $this->db->from($table);
        //$this->db->where('category', $category);
        //$this->db->limit(10, 0);	
        $this->db->order_by("id", "desc");
        $query = $this->db->get();
        return $query->result();
    }

    //login API
    function getLoginDetail($user, $pass) {
        $where = "username='$user' AND password='$pass'";
        $this->db->select('id,username,email,mobile,user_token,device_token');
        $this->db->where($where);
        $q = $this->db->get("users");
//    if($q->num_rows() > 0) {
//        return $q->result();
//    }
        return $q->result();
    }

    function getproductWithID($product_id) {
        $where = "id = $product_id";
        $this->db->select("*");
        $this->db->from('products');
        //$this->db->limit(10, 0);	
        //$this->db->order_by("id", "desc");
        $this->db->where($where);
        $query = $this->db->get();
        return $query->result();
    }

    function updateFcmToken($token, $user_id) {
        //Make an array to update to the db : FCM token
        $updateToken = array(
            "push_token" => $token
        );
        $where = "user_token = $user_id";
        $this->db->where($where);
        $this->db->update("users", $updateToken);
    }

    function getblogWithURL($url_slug) {
        $where = "url_slug = '$url_slug'";
        $this->db->select("*");
        $this->db->from('blog');
        //$this->db->limit(10, 0);	
        //$this->db->order_by("id", "desc");
        $this->db->where($where);
        $query = $this->db->get();
        return $query->result();
    }

    public function insertCustomer($data) {
        // Add created and modified date if not included
        if (!array_key_exists("created", $data)) {
            $data['created'] = date("Y-m-d H:i:s");
        }
        if (!array_key_exists("modified", $data)) {
            $data['modified'] = date("Y-m-d H:i:s");
        }

        // Insert customer data
        $insert = $this->db->insert($this->custTable, $data);

        // Return the status
        return $insert ? $this->db->insert_id() : false;
    }

    /*
     * Insert order data in the database
     * @param data array
     */

    public function insertOrder($data) {
        // Add created and modified date if not included
        if (!array_key_exists("created", $data)) {
            $data['created'] = date("Y-m-d H:i:s");
        }
        if (!array_key_exists("modified", $data)) {
            $data['modified'] = date("Y-m-d H:i:s");
        }

        // Insert order data
        $insert = $this->db->insert($this->ordTable, $data);

        // Return the status
        return $insert ? $this->db->insert_id() : false;
    }

    /*
     * Insert order items data in the database
     * @param data array
     */

    public function insertOrderItems($data = array()) {

        // Insert order items
        $insert = $this->db->insert_batch($this->ordItemsTable, $data);

        // Return the status
        return $insert ? true : false;
    }

    public function getOrder($id) {
        $this->db->select('o.*, c.name, c.email, c.contactno, c.shippingAddress');
        $this->db->from($this->ordTable . ' as o');
        $this->db->join($this->custTable . ' as c', 'c.id = o.userId', 'left');
        $this->db->where('o.id', $id);
        $query = $this->db->get();
        $result = $query->row_array();

        // Get order items
        $this->db->select('i.*, p.cloudnary_img, p.productName, p.productPrice');
        $this->db->from($this->ordItemsTable . ' as i');
        $this->db->join($this->proTable . ' as p', 'p.id = i.product_id', 'left');
        $this->db->where('i.order_id', $id);
        $query2 = $this->db->get();
        $result['items'] = ($query2->num_rows() > 0) ? $query2->result_array() : array();

        // Return fetched data
        return !empty($result) ? $result : false;
    }

    function getCategoryWithGroupBY($table, $url) {
        $where = "url = '$url'";
        $this->db->select("*");
        $this->db->from($table);
        $this->db->group_by("categoryName");
        $this->db->where($where);
        $data = $this->db->get();
        return $data->result();
    }

}

?>