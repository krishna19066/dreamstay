<!DOCTYPE HTML>
<html lang="en">
<?php include 'header.php'; ?>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script>
        /* Update item quantity */
        function updateCartItem(obj, rowid){
        	$.get("<?php echo base_url('cart/updateItemQty/'); ?>", {rowid:rowid, qty:obj.value}, function(resp){
        		if(resp == 'ok'){
        			location.reload();
        		}else{
        			alert('Cart update failed, please try again.');
        		}
        	});
        }
        </script>
  <body>
        <!--loader-->
        <div class="loader-wrap" style="display: none;">
            <div class="pin"></div>
            <div class="pulse"></div>
        </div>
        <!--loader end-->
        <!-- Main  -->
        <div id="main" style="opacity: 1;">
            <!-- header-->
            <?php include 'menu.php'; ?>
            <!--  header end -->	
            <!-- wrapper -->	
            <div id="wrapper">
                <!--content-->  
                <div class="content">
                   
                    <!-- section end -->
                    <!--section -->  
                    <section id="sec1">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="list-single-main-item fl-wrap">
                                        <div class="list-single-main-item-title fl-wrap">
                                            <h3>Order <span>Status </span></h3>
                                        </div>
                                        <div class="list-single-main-media fl-wrap">
                                           <!-- <img src="https://i.ibb.co/KmxjWP8/contact-page-banner.png" class="respimg" alt="">-->
                                        </div>
                                    
                                      <p style="color: #8BC34A; font-size: 24px;">Your order has been placed successfully.</p>
<hr/>
                                        <!-- Order status & shipping info -->
                                        <div class="row col-lg-12 ord-addr-info well">
                                            <div class="col-sm-8 adr">
                                               <p style="color: #607D8B;font-size: 24px;" >Shipping Address</p>
                                                <p><?php echo $order['name']; ?></p>
                                                <p><?php echo $order['email']; ?></p>
                                                <p><?php echo $order['contactno']; ?></p>
                                                <p><?php echo $order['shippingAddress']; ?></p>
                                            </div>
                                            <div class="col-sm-4 info">
                                                <p style="color: #607D8B;font-size: 24px;">Order Info</p>
                                                <p><b>Reference ID</b> #<?php echo $order['id']; ?></p>
                                                <p><b>Total</b> <?php echo '₹'.$order['grand_total'].' INR'; ?></p>
                                            </div>
                                        </div>
<hr/>
                                        <!-- Order items -->
                                        <div class="row ord-items">
                                            <?php if(!empty($order['items'])){ foreach($order['items'] as $item){ ?>
                                            <div class="col-lg-12 item">
                                                <div class="col-sm-2">
                                                    <div class="img" style="height: 75px; width: 75px;">
                                                       
                                                        <img src="https://res.cloudinary.com/ds89q0bap/image/upload/v1579079829/earthwater/product/<?php echo $item['cloudnary_img'] ?>.jpg" width="75"/>
                                                    </div>
                                                </div>
                                                <div class="col-sm-4">
                                                    <p><b><?php echo $item["productName"]; ?></b></p>
                                                    <p><?php echo '₹'.$item["productPrice"].' INR'; ?></p>
                                                    <p>QTY: <?php echo $item["quantity"]; ?></p>
                                                </div>
                                                <div class="col-sm-2">
                                                    <p><b><?php echo '₹'.$item["sub_total"].' INR'; ?></b></p>
                                                </div>
                                            </div>
                                           
                                            <?php } } ?>
                                        </div>
                                       
                                    </div>
                                </div>
                                </div>
                              
                </div>
                <!-- contentend -->
            </div>
       
                
            </div>
            <!-- wrapper end -->
            <!--footer -->
               <?php include 'footer.php'; ?>
            <!--footer end  -->
            <!--register form -->
          	 <?php include 'inquiry-form.php'; ?>
            <!--register form end -->
        </div>
        <!-- Main end -->
        <!--=============== scripts  ===============-->
        <script type="text/javascript" src="js/jquery.min.js"></script>
        <script type="text/javascript" src="js/plugins.js"></script>
        <script type="text/javascript" src="js/scripts.js"></script>   
        <script type="text/javascript" src="js/map_infobox.js"></script>
        <script type="text/javascript" src="js/markerclusterer.js"></script>  
        <script type="text/javascript" src="js/maps.js"></script>
</html>