<!DOCTYPE HTML>
<html lang="en">

<?php include 'header.php'; ?>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
  <body>
        <!--loader-->
        <div class="loader-wrap" style="display: none;">
            <div class="pin"></div>
            <div class="pulse"></div>
        </div>
        <!--loader end-->
        <!-- Main  -->
        <div id="main" style="opacity: 1;">
            <!-- header-->
            <?php include 'menu.php'; ?>
            <!--  header end -->	
            <!-- wrapper -->	
            <div id="wrapper">
                <!--content-->  
                <div class="content">
                    <section class="parallax-section" data-scrollax-parent="true">
                        <div class="bg par-elem " data-bg="<?=base_url()?>/assets/images/bg/6.jpg" data-scrollax="properties: { translateY: '30%' }" style="background-image: url(&quot;<?=base_url()?>assets/images/bg/6.jpg&quot;); transform: translateZ(0px) translateY(-5.44218%); background-size: cover;"></div>
                        <div class="overlay"></div>
                        <div class="bubble-bg"><div class="bubbles"><div class="individual-bubble" style="left: 506px; width: 15px; height: 15px; opacity: 0.00084872;"></div><div class="individual-bubble" style="left: 83px; width: 20px; height: 20px; opacity: 0.00591872;"></div><div class="individual-bubble" style="left: 96px; width: 20px; height: 20px; opacity: 0.0158776;"></div><div class="individual-bubble" style="left: 236px; width: 5px; height: 5px; opacity: 0.0308016;"></div><div class="individual-bubble" style="left: 418px; width: 10px; height: 10px; opacity: 0.051136;"></div><div class="individual-bubble" style="left: 937px; width: 20px; height: 20px; opacity: 0.07605;"></div><div class="individual-bubble" style="left: 1170px; width: 5px; height: 5px; opacity: 0.105708;"></div><div class="individual-bubble" style="left: 236px; width: 5px; height: 5px; opacity: 0.139603;"></div><div class="individual-bubble" style="left: 770px; width: 20px; height: 20px; opacity: 0.18024;"></div><div class="individual-bubble" style="left: 1144px; width: 20px; height: 20px; opacity: 0.22445;"></div><div class="individual-bubble" style="left: 1117px; width: 15px; height: 15px; opacity: 0.273948;"></div><div class="individual-bubble" style="left: 927px; width: 15px; height: 15px; opacity: 0.326755;"></div><div class="individual-bubble" style="left: 688px; width: 5px; height: 5px; opacity: 0.387904;"></div><div class="individual-bubble" style="left: 16px; width: 5px; height: 5px; opacity: 0.45125;"></div></div></div>
                        <div class="container">
                            <div class="section-title center-align">
                                <h2><span>Our Contacts</span></h2>
                                <div class="breadcrumbs fl-wrap"><a href="#">Home</a> <span>Contacts</span></div>
                                <span class="section-separator"></span>
                            </div>
                        </div>
                        <div class="header-sec-link">
                            <div class="container"><a href="#sec1" class="custom-scroll-link">Let's Start</a></div>
                        </div>
                    </section>
                    <!-- section end -->
                    <!--section -->  
                    <section id="sec1">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="list-single-main-item fl-wrap">
                                        <div class="list-single-main-item-title fl-wrap">
                                            <h3>Contact <span>Details </span></h3>
                                        </div>
                                        <div class="list-single-main-media fl-wrap">
                                            <img src="https://i.ibb.co/KmxjWP8/contact-page-banner.png" class="respimg" alt="">
                                        </div>
                                     
                                        <div class="list-author-widget-contacts">
                                            <ul>
                                                <li><span><i class="fa fa-map-marker"></i> Adress :</span> Cross Road Mall, FF-18 1st Floor, Central Spine, Vidyadhar Nagar, Jaipur, Rajasthan 302039</li>
                                                <li><span><i class="fa fa-phone"></i> Phone :</span> <a href="#">+91 8769114059</a></li>
                                                <li><span><i class="fa fa-envelope-o"></i> Mail :</span> info@earthwaterpurifier.com</li>
                                                <li><span><i class="fa fa-globe"></i> Website :</span> <a href="https://earthwaterpurifier.com">https://earthwaterpurifier.com/</a></li>
                                            </ul>
                                        </div>
                                       
                                    </div>
                                </div>
                                <div class="col-md-6">
                                        <div class="list-single-main-item-title fl-wrap">
                                            <h3>Order History Update</h3>
                                        </div>
                                    
                                          <!-- Category section -->
                            <section class="category-section spad">
                                <div class="row">
                                   <form action='<?=base_url()?>Home/SubmitOderHistory' method='post'>
                                      <div class="form-row">
                                        <div class="form-group col-md-6">
                                          <label for="inputEmail4">Order ID</label><label for="inputEmail4" style = "color: red">*</label>
                                          <input type="text" class="form-control" id="inputEmail4" placeholder="Order id" name='order_id' required>
                                        </div>
                                        <div class="form-group col-md-6">
                                          <label for="inputPassword4">Order Date.</label>
                                          <input type="date" class="form-control" id="inputPassword4" placeholder="Contact Number" name='order_date' >
                                        </div>
                                      </div>
                                      <div class="form-row">
                                      <div class="form-group col-md-6">
                                        <label for="inputAddress">Contact Name</label>
                                        <input type="text" class="form-control" id="inputAddress" placeholder="Contact Name" name='contact_name'>
                                      </div>
                                        <div class="form-group col-md-6">
                                          <label for="inputPassword4">Contact No.</label><label for="inputPassword4" style = "color: red">*</label>
                                          <input type="number" class="form-control" id="inputPassword4" placeholder="Contact Number" name='contact_number' required>
                                        </div>
                                        </div>
                                      <div class="form-group">
                                           <div class="form-group col-md-12">
                                        <label for="inputAddress2">Source</label>
                                         <label for="inputCity">Order</label>
                                         <select id="inputState" class="form-control" name='source'>
                                            <option selected>Choose...</option>
                                                <option value="Flipkart">Flipkart</option>
                                                <option value="Paytm">Paytm</option>
                                                <option value="Snapdeal">Snapdeal</option>
                                                <option value="Udan">Udan</option>
                                                <option value="Earthwater">Earthwater</option>
                                                <option value="Etsy">Etsy</option>
                                                <option value="Other">Other</option>
                                          </select>
                                      </div>
                                      </div>
                                      <div class="form-row">
                                        <div class="form-group col-md-6">
                                          <label for="inputCity">Status</label>
                                         <select id="inputState" class="form-control" name='status'>
                                            <option selected>Choose...</option>
                                                <option value="Pending">Pending</option>
                                                <option value="Technician Alloted">Technician Alloted</option>
                                                <option value="Processing">Processing</option>
                                                <option value="Ticket closed">Ticket closed</option>
                                          </select>
                                        
                                        </div>
                                      </div>
                                       <div class="form-row">
                                        <div class="form-group col-md-6">
                                          <label for="inputCity">Payment Mode</label>
                                         <select id="inputState" class="form-control" name='payment_mode'>
                                            <option selected>Choose...</option>
                                               <option value="Online">Online</option>
                                                <option value="Paytm">Paytm</option>
                                                <option value="Bank Transfer">Bank Transfer</option>
                                                <option value="UPI">UPI</option>
                                          </select>
                                        </div>
                                      </div>
                                       <div class="form-row">
                                            <div class="form-group col-md-6">
                                              <label for="inputCity">City</label>
                                              <input type="text" class="form-control" id="inputCity" name='city'>
                                            </div>
                                            <div class="form-group col-md-4">
                                              <label for="inputState">State</label>
                                              <select id="inputState" class="form-control" name='state'>
                                                <option selected>Choose...</option>
                                                <option>...</option>
                                              </select>
                                            </div>
                                            <div class="form-group col-md-2">
                                              <label for="inputZip">Zip</label>
                                              <input type="text" class="form-control" id="inputZip" name='pincode'>
                                            </div>
                                          </div>
                                       <div class="form-group">
                                            <div class="form-group col-md-12">
                                        <label for="exampleFormControlTextarea1">Comments</label>
                                        <textarea class="form-control" id="exampleFormControlTextarea1" name='comments' rows="3"></textarea>
                                      </div>
                                      </div>
                                      <button type="submit" class="btn btn-primary" name="submit_info">Submit Order History</button>
                                    </form>
                            </section>
        <!-- Category section end --> 
                    <!-- section end -->
                    <div class="limit-box fl-wrap"></div>
                   </div>
                </div>
                </div>
                <!-- contentend -->
            </div>
            <!-- wrapper end -->
            <!--footer -->
               <?php include 'footer.php'; ?>
            <!--footer end  -->
            <!--register form -->
          	 <?php include 'inquiry-form.php'; ?>
            <!--register form end -->
        </div>
        <!-- Main end -->
        <!--=============== scripts  ===============-->
        <script type="text/javascript" src="js/jquery.min.js"></script>
        <script type="text/javascript" src="js/plugins.js"></script>
        <script type="text/javascript" src="js/scripts.js"></script>   
        <script type="text/javascript" src="js/map_infobox.js"></script>
        <script type="text/javascript" src="js/markerclusterer.js"></script>  
        <script type="text/javascript" src="js/maps.js"></script>
</html>