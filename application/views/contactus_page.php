
<!DOCTYPE HTML>
<html lang="es">

    <head>
        <!--=============== basic  ===============-->

        <?php include 'header.php'; ?>
        <style>

            @media only screen and (min-width: 367px) and (max-width: 434px){
                .show-reg-form {/*
                    margin-right: 58px!important;*/
                }
                .nav-button-wrap{
                    /*margin-top: -103px;*/
                }
            }

            @media only screen and (max-width: 366px){
                .nav-button-wrap{
                    /*margin-top: -66px;*/
                }    
            }
            @media only screen and (max-width: 540px){
                .show-reg-form {
                    /*   margin-top:-112px !important;*/
                }
                .para{
                    display: none;
                }
            }


            .lang_btn_mobile_li{
                border: 1px solid #1a1a1b8c;
                padding: 7px;
                font-size: 12px;
                border-radius: 6px;
                font-weight: 500;
                width: 115px;
                margin-left: 61px;
                margin-bottom: 5px;
                color: #1a1a1b8c;
            }

            .imagebottom{
                width: 100%;
                height: auto;
                margin-bottom: 20px;
            }

        </style>
      
        <style type="text/css">

            .nice-select-search-box{
                display: none;
            }
            .custom-form .nice-select .list{
                padding:10px 12px 10px;
            }
            .main-search-input-item .nice-select .list{
                height: 181px !important;
                padding: 10px 12px 10px !important;
            }

            .fb-messengermessageus{
                position: fixed!important;
                bottom: 10px!important;
                right: 10px!important;
                z-index: 3;
            }
        </style>
       
    </head>
    <style>
        .list-author-widget-contacts li a {
            margin-left:0px;
        }
        .list-widget-social li a{
            background:white;
        }
        .list-widget-social li a:hover{
            background:white;   
        }
    </style>
    <body>
        <!--loader-->
        <div class="loader-wrap">
            <div class="pin"></div>
            <div class="pulse"></div>
        </div>
        <!--loader end-->
        <!-- Main  -->
       
        <div id="main">
            <!-- header-->
            <style>
                .skiptranslate { display:none; }

                @media only screen and (max-width: 450px){
                    .logo-img{
                        width:163px!important;
                    }
                }
            </style>

            <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
           <?php include 'menu.php'; ?> 
        <!--  header end -->	
        <!-- wrapper -->	
        <div id="wrapper">
            <!--content-->  
            <div class="content">
                <section class="parallax-section" data-scrollax-parent="true">
                    <div class="bg par-elem "  data-bg="https://res.cloudinary.com/dx5wgzxxo/image/upload/v1592120382/dreamstay/property/2020-06-14_03-39-41.jpg" data-scrollax="properties: { translateY: '30%' }"></div>                      
                    <div class="overlay"></div>
                    <div class="bubble-bg"></div>
                    <div class="container">
                        <div class="section-title center-align">
                            <h2><span>Contact Us</span></h2>
                            <div class="breadcrumbs fl-wrap"><a href="/">Home</a> <span>Contacts</span></div>
                            <span class="section-separator"></span>
                        </div>
                    </div>
                    <div class="header-sec-link">
                        <div class="container"><a href="#sec1" class="custom-scroll-link">Let's Start</a></div>
                    </div>
                </section>
                <!-- section end -->
                <!--section -->  
                <section id="sec1">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="list-single-main-item fl-wrap">
                                    <div class="list-single-main-item-title fl-wrap">
                                        <h3>Contact <span>Details </span></h3>
                                    </div>
                                    <div class="list-single-main-media fl-wrap">
                                        <img src="https://res.cloudinary.com/dx5wgzxxo/image/upload/v1592112217/dreamstay/property/2020-06-14_01-23-36.jpg" class="respimg" alt="">
                                    </div>
                                  <!--  <div class="list-author-widget-contacts">
                                        <ul>
                                            <li><span><i class="fa fa-map-marker"></i> </span> <a style="width: 94%; " href="#"><p>215-40 27th Ave, Flushing, NY 11360, USA</p>
                                                </a></li>
                                            <li><span><i class="fa fa-phone"></i> </span> <a href="tel:<p>+1 (212) 300-5200</p>
                                                                                                 "><p>+1 (212) 300-5200</p>
                                                </a></li>
                                            <li><span><i class="fa fa-envelope-o"></i> </span> <a href="mailto:airbnbofmalik@gmail.com
                                                                                                      "><p>airbnbofmalik@gmail.com</p>
                                                </a></li>
                                            <li><span><i class="fa fa-clock-o"></i> </span><a href="#">24-Hour Front Desk</a></li>
                                            
                                        </ul>
                                    </div>-->
                                    <div class="list-widget-social">
                                        <ul>
                                            <li><a href="#" target="_blank">
    <!-- <i class="fa fa-facebook"></i> -->
                                                    <img src="https://res.cloudinary.com/dx8xpz8co/image/upload/v1591346806/reputize/ContactUs/facebook_ydgtwb.png" style="width: 30px;height: 30px;">
                                                </a>
                                            </li>
                                            <li><a href="#" target="_blank">
<!-- <i class="fa fa-twitter"></i> -->
                                                    <img src="https://res.cloudinary.com/dx8xpz8co/image/upload/v1591346806/reputize/ContactUs/twitter_gbuqaj.png" style="width: 30px;height: 30px;">
                                                </a>
                                            </li>
                                            <li><a href="#" target="_blank">
<!--  <i class="fa fa-linkedin"></i> -->
                                                    <img src="https://res.cloudinary.com/dx8xpz8co/image/upload/v1591346806/reputize/ContactUs/linkedin_xqf1kn.png" style="width: 30px;height: 30px;">
                                                </a>
                                            </li>
                                            <li><a href="#" target="_blank">
<!-- <i class="fa fa-google-plus"></i> -->
                                                    <img src="https://res.cloudinary.com/dx8xpz8co/image/upload/v1591347118/reputize/ContactUs/google-plus_bizrww.png" style="width: 30px;height: 30px;">
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="list-single-main-wrapper fl-wrap">
                                    <div class="list-single-main-item-title fl-wrap">
                                        <h3>Our Location</h3>
                                    </div>
                                    <div class="map-container">
                                        <!--<div id="singleMap" data-latitude="40.7427837" data-longitude="-73.11445617675781"></div>-->
                                        <iframe width="100%" height="400" frameborder="0" style="border:0" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3021.2387056370103!2d-73.77398668509335!3d40.778766041465985!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x89c28a6caef43461%3A0x253e3548ba681baf!2s215-40%2027th%20Ave%2C%20Flushing%2C%20NY%2011360%2C%20USA!5e0!3m2!1sen!2sin!4v1592297642595!5m2!1sen!2sin%22%20width=%22600%22%20height=%22450%22%20frameborder=%220%22%20style=%22border:0;%22%20allowfullscreen=%22%22%20aria-hidden=%22false%22%20tabindex=%220%22" allowfullscreen></iframe>
                                    </div>
                                    <div class="list-single-main-item-title fl-wrap" style="margin-top: 20px;">
                                        <h3></h3>
                                    </div>
                                    <div id="contact-form">
                                        <div id="message"></div>
                                        <form  class="custom-form" action="<?= base_url() ?>home/SubmitInquiry" method="post" name="contactform" id="contactform99">
                                            <fieldset>
                                                <label><i class="fa fa-user-o"></i></label>
                                                <input type="text" name="name" id="name" oninput="this.value = this.value.replace(/^\s+/, ''); this.value = this.value.replace(/[^a-zA-Z.]/g, ' ')" placeholder="Your Name *" value="" required=""/>
                                                <div class="clearfix"></div>
                                                <label><i class="fa fa-envelope-o"></i></label>
                                                <input type="email"  name="email" id="email" placeholder="Email Address*" value="" required=""/>
                                                <label><i class="fa fa-phone"></i></label>
                                                <input type="text" maxlength="10" name="phone" onkeyup="if (/\D/g.test(this.value))
                                                            this.value = this.value.replace(/\D/g, '')" id="email" placeholder="Mobile*" value="" required=""/>
                                                <textarea name="message"  id="comments" cols="40" rows="3" placeholder="Your Message*" required="" maxlength="300" oninput="this.value = this.value.replace(/^\s+/, ''); this.value = this.value.replace(/[^a-zA-Z.]/g, ' ')"></textarea>
                                                <input type="text" name="spcheck2" style="display:none;" />
                                                <!-- <div class="col-lg-7 col-sm-12" style="position: inherit!important;margin-left: -16px;">
                                                     <input  type="text" placeholder="Enter Captcha*" id="captcha" name="captcha" style="margin-top:20px" required=""/>
                                                 </div>
                                                 <div class="col-lg-5 " style="position: inherit!important; margin-top: 28px;">
                                                     <img src="demo_captcha.php" class="imgcaptcha" alt="captcha"  />
                                                     <img src="images/refresh.png" alt="reload" class="refresh" />
                                                 </div>-->
                                            </fieldset>
                                            <button type="submit" name="contact-us" name="contact-us" class="btn  big-btn  color-bg flat-btn">Send<i class="fa fa-angle-right"></i></button>
                                        </form>
                                    </div>
                                    <!-- contact form  end--> 
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <!-- section end -->
                <div class="limit-box fl-wrap"></div>

            </div>
            <!-- contentend -->
        </div>
        <script language="javascript">
            $(document).ready(function () {
                $(".refresh").click(function () {
                    $(".imgcaptcha").attr("src", "demo_captcha.php?_=" + ((new Date()).getTime()));
                });
                $('#register').submit(function () {
                    if ($('#password').val() != $('#cpassword').val()) {
                        alert("Please re-enter confirm password");
                        $('#cpassword').val('');
                        $('#cpassword').focus();
                        return false;
                    }
                    $.post("submit_demo_captcha.php?" + $("#register").serialize(), {}, function (response) {
                        if (response == 1) {
                            $(".imgcaptcha").attr("src", "demo_captcha.php?_=" + ((new Date()).getTime()));
                            clear_form();
                            alert("Data Submitted Successfully.")
                        } else {
                            alert("wrong captcha code!");
                        }
                    });
                    return false;
                });
                function clear_form()
                {
                    $("#fname").val('');
                    $("#lname").val('');
                    $("#email").val('');
                    $("#phone").val('');
                    $("#username").val('');
                    $("#password").val('');
                    $("#cpassword").val('');
                    $("#captcha").val('');
                }
            });
        </script>
        <!-- wrapper end -->
        <!--footer -->



        <!--register form -->
        <!-- <style>
            .footer-contacts li {
                font-size:15px;
            }
            /*.tab-content section {
                float: none;
        width: 100%;
        position: relative;
        padding: 0px;
        overflow: visible;
        background: #fff;
        z-index: auto;
            }*/
            .guesty-root-element.small-size.guesty-widget__container .guesty-widget__item #guesty-search-widget__datepicker .lightpick{
                top:0px!important;
            }
        </style> -->
        
        <!-- Content end --> 
        <!--register form end -->
        <?php include 'footer.php'; ?>
          <!--   <a class="to-top"><i class="fa fa-angle-up"></i></a> -->
    </div>
    <!-- Main end -->
    <script type="text/javascript" src="<?= base_url() ?>assets/js/jquery.min.js"></script>
    <script type="text/javascript" src="<?= base_url() ?>assets/js/plugins.js"></script>
    <script type="text/javascript" src="<?= base_url() ?>assets/js/scripts.js"></script> 
</body>
</html>