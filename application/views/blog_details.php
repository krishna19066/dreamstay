<!DOCTYPE HTML>
<html lang="en">
<?php
$table = 'cloudnary';
$cloudnary = $this->UserModel->getAllData($table);
foreach($cloudnary as $res){
   $cloud_cdnName = $res->cloud_name;
   $cloud_cdnKey = $res->api_key;
   $cloud_cdnSecret = $res->api_secret;
}
foreach($blog_details as $res1){
   
}
?>
    
<?php include 'header.php'; ?>

    <body>
        <!--loader-->
        <div class="loader-wrap">
            <div class="pin"></div>
            <div class="pulse"></div>
        </div>
        <!--loader end-->
        <!-- Main  -->
        <div id="main">
            <!-- header-->
               <?php include 'menu.php'; ?>
            <!--  header end -->
            <!-- wrapper -->
           <!-- wrapper -->	
            <div id="wrapper">
                <!--  content  --> 
                <div class="content">
                    <!--  section  --> 
                    <section class="parallax-section single-par list-single-section" data-scrollax-parent="true" id="sec1">
                        <div class="bg par-elem "  data-bg="https://res.cloudinary.com/<?php echo $cloud_cdnName; ?>/image/upload/v1579079829/earthwatery/blog/<?php echo $res1->img ?>.jpg" data-scrollax="properties: { translateY: '30%' }" style="background-size: cover;"></div>
                        <div class="overlay"></div>
                        <div class="bubble-bg"></div>
                        <div class="list-single-header absolute-header fl-wrap">
                            <div class="container">
                                <div class="list-single-header-item">
                                      
                                  
                                    <div class="listing-rating card-popup-rainingvis" data-starrating2="5">
                             
                                    </div>
                                    <div class="list-post-counter single-list-post-counter"><span style="font-size:25px"></span></div>
                                    
                                    <div class="clearfix"></div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="list-single-header-contacts fl-wrap">
                                                <ul>
                                                    <li><i class="fa fa-phone"></i><a  href="tel:+91-8769114059">+91 8769114059</a></li>
                                                    <li><i class="fa fa-map-marker"></i><a  href="#">All India</a></li>
                                                    <li><i class="fa fa-envelope-o"></i><a  href="mailto:info@earthwaterpurifier.com">info@earthwaterpurifier.com</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                           
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <!--  section end --> 
                    <div class="scroll-nav-wrapper fl-wrap">
                        <div class="container">
                            <nav class="scroll-nav scroll-init">
                                <ul>
                                    <li><a class="act-scrlink" href="#sec1">Top</a></li>
                                    <li><a href="#sec2">Details</a></li>
                                    <li><a href="#sec3">Gallery</a></li>
                                    <li><a href="#sec4">Reviews</a></li>
                                </ul>
                            </nav>
                            <a href="#" class="save-btn"> <i class="fa fa-heart"></i> Save </a>
                        </div>
                    </div>
                    <!--  section  --> 
                    <section class="gray-section no-top-padding">
                        <div class="container" style="font-size:17px;">
                            <div class="row">
                                <div class="col-md-8">
                                    <div class="list-single-main-wrapper fl-wrap" id="sec2">
                                        <div class="breadcrumbs gradient-bg  fl-wrap"><a href="#">Home</a><a href="#">Blog</a><span></span></div>
                                        <div class="list-single-main-media fl-wrap">
                                            <img src="https://res.cloudinary.com/<?php echo $cloud_cdnName; ?>/image/upload/v1579079829/earthwatery/blog/<?php echo $res1->img ?>.jpg" class="respimg" alt="">
                                           
                                        </div>
                                        <div class="list-single-main-item fl-wrap">
                                            <div class="list-single-main-item-title fl-wrap">
                                                <h3><?php echo $res1->title; ?> </h3>
                                            </div>
                                            <style>.accordion{font-size: 24px;
    font-weight: 600;}</style>
                                           <p><?php echo $res1->content; ?></p>
                                              <span class="fw-separator"></span>
                                        </div>
                                                               
                                    </div>
                                </div>
                                <!--box-widget-wrap -->
                                <div class="col-md-4">
                                    <div class="box-widget-wrap">
                                    
                                        <!--box-widget-item -->
                                        <div class="box-widget-item fl-wrap">
                                            <div class="box-widget-item-header">
                                                <h3>Book Now : </h3>
                                            </div>
                                            <div class="box-widget opening-hours">
                                                <div class="box-widget-content">
                                                    <form class="add-comment custom-form" action='<?=base_url()?>index.php/Home/SubmitInquiry' method='POST'>
                                                        <fieldset>
                                                            <label><i class="fa fa-user-o"></i></label>
                                                            <input type="text" name='name' placeholder="Your Name *" value="" required/>
                                                            <div class="clearfix"></div>
                                                            <label><i class="fa fa-envelope-o"></i></label>
                                                            <input type="text" placeholder="Email Address*" name='email' value="" required/>
                                                             <label><i class="fa fa-phone"></i>  </label>
                                                            <input type="text" placeholder="Phone/Mobile*" name='mobile' value="" required />
                                                            <textarea cols="40" rows="3" name ='message' placeholder="Additional Information:"></textarea>
                                                        </fieldset>
                                                        <button type='submit' name="inquiry" class="btn  big-btn  color-bg flat-btn">Book Now<i class="fa fa-angle-right"></i></button>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                        <!--box-widget-item end -->                                      
                                        <!--box-widget-item -->
                                     
                                        <!--box-widget-item end -->
                                      	
                                        
                                   </div>
                                </div>
                                <!--box-widget-wrap end -->
                            </div>
                        </div>
                    </section>
                    <!--  section end --> 
                    <div class="limit-box fl-wrap"></div>
                    <!--  section  --> 
                    
                    <!--  section  end--> 
                </div>
                <!--  content end  --> 
            </div>
            <!-- wrapper end -->
            <!--footer -->
            <?php include 'footer.php'; ?>
            <!--footer end  -->
            <!--register form -->
            <?php include 'inquiry-form.php'; ?>
            <!--register form end -->
            <a class="to-top"><i class="fa fa-angle-up"></i></a>
        </div>
       
        <!-- Main end -->
        <!--=============== scripts  ===============-->
        <script type="text/javascript" src="<?=base_url()?>assets/js/jquery.min.js"></script>
        <script type="text/javascript" src="<?=base_url()?>assets/js/plugins.js"></script>
        <script type="text/javascript" src="<?=base_url()?>assets/js/scripts.js"></script>			
    </body>
</html>