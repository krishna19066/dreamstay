
<!DOCTYPE HTML>
<html lang="en">
    <head>
        <?php include 'header.php'; ?>
        <style >

            @media only screen and (min-width: 367px) and (max-width: 434px){
                .show-reg-form {/*
                    margin-right: 58px!important;*/
                }
                .nav-button-wrap{
                    /*margin-top: -103px;*/
                }
            }

            @media only screen and (max-width: 366px){
                .nav-button-wrap{
                    /*margin-top: -66px;*/
                }    
            }
            @media only screen and (max-width: 540px){
                .show-reg-form {
                    /*   margin-top:-112px !important;*/
                }
                .para{
                    display: none;
                }
            }


            .lang_btn_mobile_li{
                border: 1px solid #1a1a1b8c;
                padding: 7px;
                font-size: 12px;
                border-radius: 6px;
                font-weight: 500;
                width: 115px;
                margin-left: 61px;
                margin-bottom: 5px;
                color: #1a1a1b8c;
            }


        </style>

        <style type="text/css">

            .nice-select-search-box{
                display: none;
            }
            .custom-form .nice-select .list{
                padding:10px 12px 10px;
            }
            .main-search-input-item .nice-select .list{
                height: 181px !important;
                padding: 10px 12px 10px !important;
            }

            .fb-messengermessageus{
                position: fixed!important;
                bottom: 10px!important;
                right: 10px!important;
                z-index: 3;
            }
        </style>
       
    </head>
    <style>
        .process-wrap p {
            text-align: center;
        }
        .process-item{
            height: 330px;
        }
    </style>
    <body>
        <!--loader-->
        <div class="loader-wrap">
            <div class="pin"></div>
            <div class="pulse"></div>
        </div>
        <!--loader end-->
        <!-- Main  -->
        <div id="main">
            <!-- header-->

            <style>
                .skiptranslate { display:none; } 
            </style>

            <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
            <?php include 'menu.php'; ?>           
            <!--  header end -->
            <!-- wrapper -->
            <div id="wrapper">
                <!--  content  -->
                <div class="content">
                    <!--  section  end-->
                    <!--  section  -->
                    <section class="parallax-section" data-scrollax-parent="true">
                        <div class="bg par-elem " data-bg="https://res.cloudinary.com/dx5wgzxxo/image/upload/v1592119035/dreamstay/property/2020-06-14_03-17-14.webp" data-scrollax="properties: { translateY: '30%' }"></div>
                        <div class="overlay"></div>
                        <div class="container">
                            <div class="section-title center-align">
                                <h2><span></span>Our Dream Place to Stay</h2>
                                <div class="breadcrumbs fl-wrap"><a href="/">Home</a><a href="#">Listings</a><span>New York</span></div>
                                <span class="section-separator"></span>
                            </div>
                        </div>
                        <div class="header-sec-link">
                            <div class="container"><a href="#sec1" class="custom-scroll-link">Let's Start</a></div>
                        </div>
                    </section>
                    <!--  section  end-->
                    <!--  section  -->
                    <section class="gray-bg no-pading no-top-padding" id="sec1">
                        <div class="col-list-wrap fh-col-list-wrap  left-list">
                            <div class="container">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="listsearch-header fl-wrap">
                                            <h3>Results For : <span>Dream Stay Vacation Rental - All Listing</span></h3>
                                            <div class="listing-view-layout">
                                                <ul>
                                                    <li><a class="grid " href="#"><i class="fa fa-th-large"></i></a></li>
                                                    <li><a class="list active" href="#"><i class="fa fa-list-ul"></i></a></li>
                                                </ul>
                                            </div>
                                        </div><br/>
                                        <section>
                                            <div class="container">
                                                <div class="section-title">
                                                    <h2>Our Dream Place to Stay</h2>
                                                    <!-- <div class="section-subtitle">Berkshire </div> -->
                                                    <span class="section-separator"></span>                              
                                                </div>
                                                <!--process-wrap  -->
                                                <div class="process-wrap fl-wrap">
                                                    <ul>
                                                        <li>
                                                            <div class="process-item">
                                                                <span class="process-count">01 . </span>
                                                                <div class="time-line-icon"><i class="fa fa-map-o"></i></div>
                                                                <h4> INSTANT BOOKING</h4>                                           
                                                            </div>
                                                            <span class="pr-dec"></span>
                                                        </li>
                                                        <li>
                                                            <div class="process-item">
                                                                <span class="process-count">02 .</span>
                                                                <div class="time-line-icon"><i class="fa fa-headphones"></i></div>
                                                                <h4> 24x7 CUSTOMER SERVICE</h4>

                                                            </div>
                                                            <span class="pr-dec"></span>
                                                        </li>
                                                        <li>
                                                            <div class="process-item">
                                                                <span class="process-count">03 .</span>
                                                                <div class="time-line-icon"><i class="fa fa-hand-peace-o"></i></div>
                                                                <h4> HOSPITALITY EXPERTS</h4>

                                                            </div>
                                                        </li>
                                                    </ul>
                                                    <div class="process-end"><i class="fa fa-check"></i></div>
                                                </div>
                                                <!--process-wrap   end-->
                                            </div>
                                        </section>

                                        <!-- list-main-wrap-->
                                        <div class="list-main-wrap fl-wrap card-listing">
                                            <section>
                                                <!-- listing-item -->
                                                   <?php foreach ($property_data as $pro) { ?>
                                                <div class="listing-item ">
                                                    <article class="geodir-category-listing fl-wrap" style="height:580px;">
                                                        <div class="geodir-category-img">
                                                           <?php
                                                                $propID = $pro->id;
                                                                $get_images = $this->UserModel->getPropertyImg($propID);
                                                                foreach ($get_images as $img) {
                                                                    ?>
                                                                    <img src="https://res.cloudinary.com/dx5wgzxxo/image/upload/v1592120382/dreamstay/property/<?php echo $img->img; ?>.jpg"  alt="">                                                           
                                                                <?php } ?> 
                                                            <div class="overlay"></div>
                                                            
                                                        </div>
                                                        <div class="geodir-category-content fl-wrap">
                                                            <a style="font-weight: 800;" class="listing-geodir-category" href="<?= base_url() ?>property/<?php echo htmlentities($pro->propertyURL); ?>">View Details</a>

                                                            <h3><a href="<?= base_url() ?>property/<?php echo htmlentities($pro->propertyURL); ?>"><?php echo $pro->propertyName; ?></a></h3>
                                                            <p> <?php echo strip_tags(trim(ucfirst(substr($pro->propertyInfo, 0, 85)))); ?> </p>
                                                            <div class="geodir-category-options fl-wrap">
                                                               
                                                                   <p><?php echo $pro->propertyName; ?></p>                                                                   
                                                                <div class="geodir-category-location">
                                                                    <a href="<?= base_url() ?>property/<?php echo htmlentities($pro->propertyURL); ?>">
                                                                        <i class="fa fa-map-marker" aria-hidden="true"></i>
                                                                       <?php echo strip_tags(trim(ucfirst(substr($pro->city, 0, 85)))); ?>,US                                                                      </a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </article>
                                                </div>
                                                  <?php } ?>     
                                                <!-- listing-item -->
                                               
                                                

                                            </section>

                                            <!--                                            <div class="container9">
                                                                                             listing-item end                                           
                                                                                            <div class="clearfix"></div>  
                                                                                            <div class="section-title">
                                                                                                <h2> Our Apartments</h2>
                                                                                                <div class="section-subtitle">Berkshire</div>
                                                                                                <span class="section-separator"></span>											
                                                                                            </div>	
                                                                                        </div>	-->






                                            <section id="sec2">
                                                <div class="container">
                                                    <div class="section-title">
                                                        <h2>  Health & Hygiene with you in mind..</h2>
                                                        <!-- <div class="section-subtitle">Berkshire</div> -->
                                                        <span class="section-separator"></span>                               
                                                    </div>
                                                    <!--about-wrap -->
                                                    <div class="about-wrap">
                                                        <div class="row">                                   
                                                            <div class="col-md-12">
                                                                <div class="list-single-main-item-title fl-wrap">
                                                                    <h3></h3>

                                                                        <!-- <span class="section-separator fl-sec-sep"></span> -->
                                                                </div>
                                                                <p>Welcome to your home away from home - we would love to go the extra mile so you enjoy this home as much as we do!</p>
                                                                <p>&nbsp;</p>

                                                               
                                                                <p>We will be happy to tailor to all of your needs during your stay to make your trip to New York City an unforgettable one. We will meet you at check-in, answer any questions you have, provide local advice and directions as required. Don’t hesitate to contact us if you need anything. We will be happy to assist with all your needs during your visit. Besides that, we’ll just leave you to enjoy your stay!
                                                            </p>

                                                            </div>

                                                        </div>
                                                    </div>
                                                    <!-- about-wrap end  -->
                                                </div>
                                            </section>
                                            <!-- portfolio start -->
                                            <div class="gallery-items fl-wrap mr-bot spad" style="position: relative; height: 556.485px;">                                                               
                                                <!-- gallery-item-->
                                                <div class="gallery-item" style="position: absolute; left: 0px; top: 276px;">
                                                    <div class="grid-item-holder">
                                                        <div class="listing-item-grid">
                                                            <img src="https://res.cloudinary.com/dx8xpz8co/image/upload/c_fill/reputize/awards/image1-all-apartments-2020_05_04_17_39_37.jpg" alt="" alt="" style="height: 235px;">
                                                            <div class="listing-counter"><span>  </span> Shops</div>
                                                            <div class="listing-item-cat">
                                                                <h3><a href="#"></a></h3>                                               
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- gallery-item end-->
                                                <!-- gallery-item-->
                                                <div class="gallery-item" style="position: absolute; left: 407px; top: 279px;">
                                                    <div class="grid-item-holder">
                                                        <div class="listing-item-grid">
                                                            <img src="https://res.cloudinary.com/dx8xpz8co/image/upload/c_fill/reputize/awards/image2-all-apartments-2020_05_04_17_39_40.jpg" alt="" style="height: 235px;">
                                                            <div class="listing-counter"><span> </span> Resturants</div>
                                                            <div class="listing-item-cat">
                                                                <h3><a href="#"></a></h3>                                               
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- gallery-item end-->
                                                <!-- gallery-item-->
                                                <div class="gallery-item" style="position: absolute; left: 815px; top: 279px;">
                                                    <div class="grid-item-holder">
                                                        <div class="listing-item-grid">
                                                            <img src="https://res.cloudinary.com/dx8xpz8co/image/upload/c_fill/reputize/awards/image3-all-apartments-2020_05_07_16_30_09.jpg" alt="" style="height: 235px;">
                                                            <div class="listing-counter"><span>  </span> Bars</div>
                                                            <div class="listing-item-cat">
                                                                <h3><a href="#"></a></h3>                                               
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- gallery-item end-->
                                            </div>
                                            <!-- portfolio end -->



                                        </div>
                                        <!-- list-main-wrap end-->
                                    </div>

                                </div>
                            </div>
                        </div>
                    </section>
                    <!--  section  end-->
                    <div class="limit-box fl-wrap"></div>
                    <!--  section  -->

                    <!--  section  end-->
                </div>
                <!-- content end-->
            </div>
            <!-- wrapper end -->
            <!--footer -->



            <!--register form -->
            
           
            <!-- Content end --> 
            <!--register form end -->
            <?php include 'footer.php'; ?>

            <!--footer end  -->

            <!-- <a class="to-top"><i class="fa fa-angle-up"></i></a> -->
        </div>

        <script>
            $(window).bind("resize", function () {
                console.log($(this).width())
                if ($(this).width() < 500) {
                    $('div').removeClass('listing-item').addClass('listing-layout')
                }
            }).trigger('resize');
        </script>
        <!-- Main end -->
        <script type="text/javascript" src="<?= base_url() ?>assets/js/jquery.min.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>assets/js/plugins.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>assets/js/scripts.js"></script>  

    </body>
</html>