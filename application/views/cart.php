<!DOCTYPE HTML>
<html lang="en">
<?php include 'header.php'; ?>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script>
        /* Update item quantity */
        function updateCartItem(obj, rowid){
        	$.get("<?php echo base_url('cart/updateItemQty/'); ?>", {rowid:rowid, qty:obj.value}, function(resp){
        		if(resp == 'ok'){
        			location.reload();
        		}else{
        			alert('Cart update failed, please try again.');
        		}
        	});
        }
        </script>
  <body>
        <!--loader-->
        <div class="loader-wrap" style="display: none;">
            <div class="pin"></div>
            <div class="pulse"></div>
        </div>
        <!--loader end-->
        <!-- Main  -->
        <div id="main" style="opacity: 1;">
            <!-- header-->
            <?php include 'menu.php'; ?>
            <!--  header end -->	
            <!-- wrapper -->	
            <div id="wrapper">
                <!--content-->  
                <div class="content">
                   
                    <!-- section end -->
                    <!--section -->  
                    <section id="sec1">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="list-single-main-item fl-wrap">
                                        <div class="list-single-main-item-title fl-wrap">
                                            <h3>Shopping <span>Cart </span></h3>
                                        </div>
                                        <div class="list-single-main-media fl-wrap">
                                           <!-- <img src="https://i.ibb.co/KmxjWP8/contact-page-banner.png" class="respimg" alt="">-->
                                        </div>
                                    
                                        <div class="row cart">
                                            <table class="table">
                                            <thead>
                                                <tr>
                                                    <th width="10%"></th>
                                                    <th width="35%">Product</th>
                                                    <th width="15%">Price</th>
                                                    <th width="10%">Quantity</th>
                                                    <th width="20%">Subtotal</th>
                                                    <th width="12%"></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php if($this->cart->total_items() > 0){ foreach($cartItems as $item){    ?>
                                                <tr>
                                                    <td>
                                                     
                                                        <img src="https://res.cloudinary.com/ds89q0bap/image/upload/v1579079829/earthwater/product/<?php echo $item['image'] ?>.jpg" width="50"/>
                                                    </td>
                                                    <td><?php echo $item["name"]; ?></td>
                                                    <td><?php echo '₹'.$item["price"].' INR'; ?></td>
                                                    <td><input type="number" class="form-control text-center" value="<?php echo $item["qty"]; ?>" min='1' onchange="updateCartItem(this, '<?php echo $item["rowid"]; ?>')"></td>
                                                    <td><?php echo '₹'.$item["subtotal"].' INR'; ?></td>
                                                    <td>
                                                        <a href="<?php echo base_url('cart/removeItem/'.$item["rowid"]); ?>" class="btn btn-danger" onclick="return confirm('Are you sure?')"><i class="glyphicon glyphicon-trash"></i></a>
                                                    </td>
                                                </tr>
                                                <?php } }else{ ?>
                                                <tr><td colspan="6"><p>Your cart is empty.....</p></td>
                                                <?php } ?>
                                            </tbody>
                                            <tfoot>
                                                <tr>
                                                    <td><a href="<?php echo base_url('home/'); ?>" class="btn btn-warning"><i class="glyphicon glyphicon-menu-left"></i> Continue Shopping</a></td>
                                                    <td colspan="3"></td>
                                                    <?php if($this->cart->total_items() > 0){ ?>
                                                    <td class="text-left">Grand Total: <b><?php echo '₹'.$this->cart->total().' INR'; ?></b></td>
                                                    <td><a href="<?php echo base_url('checkout/'); ?>" class="btn btn-success btn-block">Checkout <i class="glyphicon glyphicon-menu-right"></i></a></td>
                                                    <?php } ?>
                                                </tr>
                                            </tfoot>
                                            </table>
                                        </div>
                                       
                                    </div>
                                </div>
                              
                </div>
                <!-- contentend -->
            </div>
       
                
            </div>
            <!-- wrapper end -->
            <!--footer -->
               <?php include 'footer.php'; ?>
            <!--footer end  -->
            <!--register form -->
          	 <?php include 'inquiry-form.php'; ?>
            <!--register form end -->
        </div>
        <!-- Main end -->
        <!--=============== scripts  ===============-->
        <script type="text/javascript" src="js/jquery.min.js"></script>
        <script type="text/javascript" src="js/plugins.js"></script>
        <script type="text/javascript" src="js/scripts.js"></script>   
        <script type="text/javascript" src="js/map_infobox.js"></script>
        <script type="text/javascript" src="js/markerclusterer.js"></script>  
        <script type="text/javascript" src="js/maps.js"></script>
</html>