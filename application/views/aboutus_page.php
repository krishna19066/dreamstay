
<!DOCTYPE HTML>
<html lang="en">

    <head>
        <?php include 'header.php'; ?>

        <style >

            @media only screen and (min-width: 367px) and (max-width: 434px){
                .show-reg-form {/*
                    margin-right: 58px!important;*/
                }
                .nav-button-wrap{
                    /*margin-top: -103px;*/
                }
            }

            @media only screen and (max-width: 366px){
                .nav-button-wrap{
                    /*margin-top: -66px;*/
                }    
            }
            @media only screen and (max-width: 540px){
                .show-reg-form {
                    /*   margin-top:-112px !important;*/
                }
                .para{
                    display: none;
                }
            }


            .lang_btn_mobile_li{
                border: 1px solid #1a1a1b8c;
                padding: 7px;
                font-size: 12px;
                border-radius: 6px;
                font-weight: 500;
                width: 115px;
                margin-left: 61px;
                margin-bottom: 5px;
                color: #1a1a1b8c;
            }

            .imagebottom{
                width: 100%;
                height: auto;
                margin-bottom: 20px;
            }

        </style>
        <style type="text/css">

            .nice-select-search-box{
                display: none;
            }
            .custom-form .nice-select .list{
                padding:10px 12px 10px;
            }
            .main-search-input-item .nice-select .list{
                height: 181px !important;
                padding: 10px 12px 10px !important;
            }

            .fb-messengermessageus{
                position: fixed!important;
                bottom: 10px!important;
                right: 10px!important;
                z-index: 3;
            }
        </style>
        <!--=============== favicons ===============-->
    </head>
    <body>
        <!--loader-->
        <div class="loader-wrap">
            <div class="pin"></div>
            <div class="pulse"></div>
        </div>
        <!--loader end-->
        <!-- Main  -->
        <div id="main">
            <!-- header-->

            <style>
                .skiptranslate { display:none; }

                @media only screen and (max-width: 450px){
                    .logo-img{
                        width:163px!important;
                    }
                }
            </style>

            <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
            <?php include 'menu.php'; ?>           
            <!--  header end -->
            <!-- wrapper -->
            <div id="wrapper">
                <!--Content -->
                <div class="content">
                    <!--section -->
                    <section class="parallax-section" data-scrollax-parent="true" id="sec1">
                        <div class="bg par-elem "  data-bg="https://res.cloudinary.com/dx5wgzxxo/image/upload/v1592120107/dreamstay/property/2020-06-14_03-35-06.jpg" data-scrollax="properties: { translateY: '30%' }"></div>
                        <div class="overlay"></div>
                        <div class="container">
                            <div class="section-title center-align">
                                <h2><span>About Us</span></h2>
                                <div class="breadcrumbs fl-wrap"><a href="/">Home</a><span>About</span></div>
                                <span class="section-separator"></span>
                            </div>
                        </div>
                        <div class="header-sec-link">
                            <div class="container"><a href="#sec2" class="custom-scroll-link">Let's Start</a></div>
                        </div>
                    </section>
                    <!-- section end -->
                    <!--section -->
                    <div class="scroll-nav-wrapper fl-wrap">
                        <div class="container">
                            <nav class="scroll-nav scroll-init inline-scroll-container">
                                <ul>
                                    <li><a class="act-scrlink" href="#sec1">Top</a></li>
                                    <li><a href="#sec2">About</a></li>
                                    <li><a href="#sec3">Facts</a></li>
                                    <li><a href="#sec4">Team</a></li>
                                    <li><a href="#sec5">Testimonials</a></li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                    <section id="sec2">
                        <div class="container">
                            <div class="section-title">
                                <h2> Our People</h2>
                                <!-- <div class="section-subtitle"></div> -->
                                <span class="section-separator"></span>
                                <p>Welcome to your home away from home - we would love to go the extra mile so you enjoy this home as much as we do!
From the minute you arrive, you will be embraced by the sleek, flawless arrangement, exquisite furniture,contemporary design,unique fixtures and fittings, thoughtful amenities and the sparkling cleanliness that radiates from every corner of this amazing space.<p>
                            </div>
                            <!--about-wrap -->
                            <div class="about-wrap">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="video-box fl-wrap">
<!--                                            <img src="https://res.cloudinary.com/dx8xpz8co/image/upload/c_fill/reputize/aboutus/aboutus-header2020_05_07_16_12_16.jpg" alt="">-->
                                            <img src="https://res.cloudinary.com/dx5wgzxxo/image/upload/v1592112217/dreamstay/property/2020-06-14_01-23-36.jpg" alt="">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="list-single-main-item-title fl-wrap">
                                            <h3>About Us</span></h3>
                                            <span class="section-separator fl-sec-sep"></span>
                                        </div>
                                        <p style="text-align: justify;"><p>Hello! My name is Malik, a Queens local with a passion for hospitality and traveling. Considering a deep-rooted passion for anything to do with hospitality and traveling, it seemed like the perfect thing to do; to share my properties while we are away—something which actually happened by accident. Luckily, I happened to love hosting. Not only do I love being an Airbnb host, but I also love being on the traveling spectrum too. I have traveled a lot in the States and am now doing more overseas travel (I am winging it here). I have a distinct & in-depth knowledge of must-visit places in the New York area. And even though COVID19 won't allow for up close and personal interaction, you can always reach me by phone at any time. I really enjoy the ability to share the experience of living in these magnificent homes. Definitely check with me for fantastic sunset locations, where to go for superb walks, and where to have mindblowing culinary experiences! I love meeting new people from different walks of life, but as we will be gone most of the time, guests will have all the space they need. If you need anything, call us so we can assist you.
                                        
                                        </p>
                                        
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <!-- section end -->
                    <style>
                        .morecontent span {
                            display: none;
                        }
                        .morelink {
                            display: block;
                        }
                    </style>
                    <script>
                        $(document).ready(function () {
                            // Configure/customize these variables.
                            var showChar = 200;  // How many characters are shown by default
                            var ellipsestext = "";
                            var moretext = "Read More";
                            var lesstext = "Read Less";
                            $('.team-info>p').each(function () {
                                var content = $(this).html();
                                if (content.length > showChar) {
                                    var c = content.substr(0, showChar);
                                    var h = content.substr(showChar, content.length - showChar);
                                    var html = c + '<span class="moreellipses">' + ellipsestext + '&nbsp;</span><span class="morecontent"><span>' + h + '</span>&nbsp;&nbsp;<a href="" class="morelink" style="color: #2792c1;">' + moretext + '</a></span>';
                                    $(this).html(html);
                                }
                            });
                            $(".morelink").click(function () {
                                if ($(this).hasClass("less")) {
                                    $(this).removeClass("less");
                                    $(this).html(moretext);
                                } else {
                                    $(this).addClass("less");
                                    $(this).html(lesstext);
                                }
                                $(this).parent().prev().toggle();
                                $(this).prev().toggle();
                                return false;
                            });
                        });
                    </script>
                    <!--section -->
                    <!-- section end -->                                       
                    <div class="limit-box"></div>
                </div>

            </div>
            <!-- wrapper end -->
            <!--footer -->
 
           <?php //include 'inquiry-form.php'; ?>

            <!--register form end -->
            <!--section -->
            <section class="gradient-bg">
                <div class="cirle-bg">
                    <div class="bg" data-bg="images/bg/circle.png"></div>
                </div>
                <div class="container">
                    <div class="join-wrap fl-wrap">
                        <div class="row">
                            <div class="col-md-8">
                                <h3>Business Traveller?</h3>
                                <p>For best offers contact us</p>
                            </div>
                            <div class="col-md-4"><a href="<?= base_url() ?>contact-us" class="join-wrap-btn">Contact us<i class="fa fa-envelope-o"></i></a></div>
                        </div>
                    </div>
                </div>
            </section>

            <!-- Content end --> 
            <!--register form end -->
            <?php include 'footer.php'; ?>
            <!--footer end  -->
        <!-- <a class="to-top"><i class="fa fa-angle-up"></i></a> -->
        </div>
        <!-- Main end -->
        <!--=============== scripts  ===============-->
        <script type="text/javascript" src="<?= base_url() ?>assets/js/jquery.min.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>assets/js/plugins.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>assets/js/scripts.js"></script> 
    </body>
</html>