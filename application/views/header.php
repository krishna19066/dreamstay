<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<!--=============== basic  ===============-->        
<meta charset="UTF-8">
<title></title>
<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
<meta name="robots" content="index, follow"/>
<meta name="keywords" content=""/>
<meta name="description" content=""/>
<!--=============== css  ===============--> 
<style >

    @media only screen and (min-width: 367px) and (max-width: 434px){
        .show-reg-form {/*
            margin-right: 58px!important;*/
        }
        .nav-button-wrap{
            /*margin-top: -103px;*/
        }
    }

    @media only screen and (max-width: 366px){
        .nav-button-wrap{
            /*margin-top: -66px;*/
        }    
    }
    @media only screen and (max-width: 540px){
        .show-reg-form {
            /*   margin-top:-112px !important;*/
        }
        .para{
            display: none;
        }
    }


    .lang_btn_mobile_li{
        border: 1px solid #1a1a1b8c;
        padding: 7px;
        font-size: 12px;
        border-radius: 6px;
        font-weight: 500;
        width: 115px;
        margin-left: 61px;
        margin-bottom: 5px;
        color: #1a1a1b8c;
    }


</style>
<link type="text/css" rel="stylesheet" href="<?= base_url() ?>assets/css/reset.css">
<link type="text/css" rel="stylesheet" href="<?= base_url() ?>assets/css/plugins.css">
<link type="text/css" rel="stylesheet" href="<?= base_url() ?>assets/css/style.css">
<link type="text/css" rel="stylesheet" href="<?= base_url() ?>assets/css/color.css">
<link rel="stylesheet" href="<?= base_url() ?>assets/css/flags.css">
<style type="text/css">

    .nice-select-search-box{
        display: none;
    }
    .custom-form .nice-select .list{
        padding:10px 12px 10px;
    }
    .main-search-input-item .nice-select .list{
        height: 181px !important;
        padding: 10px 12px 10px !important;
    }

    .fb-messengermessageus{
        position: fixed!important;
        bottom: 10px!important;
        right: 10px!important;
        z-index: 3;
    }
</style>
<!--=============== favicons ===============-->
<link rel="shortcut icon" href="<?= base_url() ?>assets/images/logo.png" type="image/x-icon"/>    
