<!DOCTYPE HTML>
<html lang="en">
    <?php foreach($product as $pro){
}?>
     <title><?php echo $pro->title; ?></title>
     <meta name="description" content="<?php echo $pro->metaDescription; ?>"/>
<?php include 'header.php'; ?>
 <link type="text/css" rel="stylesheet" href="<?=base_url()?>assets/css/prem-style.css">
 <script>
     function myFunction() {
    var name = document.getElementById("name").value;
    var email = document.getElementById("email").value;
    var mobile = document.getElementById("mobile").value;
   // alert(name);
    if(name!='' && email!='' && mobile !=''){
         alert('Thank You! Your Order has been successfuly submited');
        return true;
      
    }else{
          alert('please fill the form');
        return false;
    }
    alert(name);
}
 </script>

    <body>
        <!--loader-->
        <div class="loader-wrap">
            <div class="pin"></div>
            <div class="pulse"></div>
        </div>
        <!--loader end-->
        <!-- Main  -->
        <div id="main">
            <!-- header-->
               <?php include 'menu.php'; ?>
            <!--  header end -->
            <!-- wrapper -->
           <!-- wrapper -->	
            <div id="wrapper">
                <!--  content  --> 
                <div class="content">
                    
                                      <!-- product section -->
        <section class="product-section">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="product-pic">
                            <img class="product-big-img" src="https://res.cloudinary.com/ds89q0bap/image/upload/v1579079829/earthwater/product/<?php echo $pro->cloudnary_img; ?>.jpg">
                        </div>
                        <div class="product-thumbs" tabindex="1" style="overflow: hidden; outline: none;">
                            <div class="product-thumbs-track">
                                <div class="pt active" data-imgbigurl="https://res.cloudinary.com/ds89q0bap/image/upload/v1579079829/earthwater/product/<?php echo $pro->cloudnary_img; ?>.jpg"><img src="https://res.cloudinary.com/ds89q0bap/image/upload/v1579079829/earthwater/product/<?php echo $pro->cloudnary_img; ?>.jpg" alt=""></div>
                                <div class="pt" data-imgbigurl="https://res.cloudinary.com/ds89q0bap/image/upload/v1579079829/earthwater/product/<?php echo $pro->cloudnary_img; ?>.jpg"><img src="https://res.cloudinary.com/ds89q0bap/image/upload/v1579079829/earthwater/product/<?php echo $pro->cloudnary_img; ?>.jpg" alt=""></div>
                                <div class="pt" data-imgbigurl="https://res.cloudinary.com/ds89q0bap/image/upload/v1579079829/earthwater/product/<?php echo $pro->cloudnary_img; ?>.jpg"><img src="https://res.cloudinary.com/ds89q0bap/image/upload/v1579079829/earthwater/product/<?php echo $pro->cloudnary_img; ?>.jpg" alt=""></div>
                               
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 product-details">
                        <h2 class="p-title"><?php echo htmlentities($pro->productName);?></h2>
                        <h4 class="p-stock">Availability: <span><?php echo htmlentities($pro->productAvailability);?></span></h4>
                        <div class="p-rating">
                            <i class="fa fa-star-o"></i>
                            <i class="fa fa-star-o"></i>
                            <i class="fa fa-star-o"></i>
                            <i class="fa fa-star-o"></i>
                            <i class="fa fa-star-o fa-fade"></i>
                        </div>
                        <div class="p-review">
                            <a href="">3 reviews</a>|<a href="">Add your review</a>
                        </div>
                        <h3 class="p-price">₹<?php echo htmlentities($pro->productPrice);?>.00</h3>
                        <div class="quantity">
                            <p>Quantity</p>
                            <div class="pro-qty"><input type="text" value="1"></div>
							
                        </div>
						<div id="accordion" class="accordion-area">
                            <div class="panel">
                                <div class="panel-header" id="headingOne" style="height:0px;">
                                    <button class="panel-link active" data-toggle="collapse" data-target="#collapse1" aria-expanded="true" aria-controls="collapse1"></button>
                                </div>
                                <div id="collapse1" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                                    <div class="panel-body" style="text-align: justify;">
                                        <p style='font-size:14px;font-weight:600'> Total Capacity: <?php echo $pro->capacity; ?></p>
                                    </div>
                                </div>
                            </div>
                        </div>
						<div id="accordion" class="accordion-area">
                            <div class="panel" style="margin-bottom: 10px;">
                                <div class="panel-header" id="headingOne" style="height:0px;">
                                    <button class="panel-link active" data-toggle="collapse" data-target="#collapse1" aria-expanded="true" aria-controls="collapse1"></button>
                                </div>
                                <div id="collapse1" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                                    <div class="panel-body" style="text-align: justify;">
                                        <p style='font-size:14px;font-weight:600'>Highlights: <?php echo $pro->productHighlight; ?></p>
                                    </div>
                                </div>
                            </div>
                        </div>
						<form class="add-comment custom-form"  action='<?=base_url()?>Checkout/QuickOrder' method='POST'>
												 <fieldset>
												<label><i class="fa fa-map-marker"></i></label>
												<input type="text" name='pincode'id='name' placeholder="Your Pincode *" value="" required/>
											
												 <input type="hidden" name='product_id' value="<?php echo $pro->id; ?>" />
												<div class="clearfix"></div>
												<label><i class="fa fa-envelope-o"></i></label>
												<input type="text" placeholder="Email Address*" id='email' name='email' value="" required/>
												 <label><i class="fa fa-phone"></i>  </label>
												<input type="text" placeholder="Phone/Mobile*" id='mobile' name='mobile' value="" required />
												 
												
											</fieldset>
									
												 <a href="<?php echo base_url('product/addToCart/'.$pro->id); ?>" class="site-btn" style="background: #FF5722;"><i class="flaticon-bag"></i>Add to Card </a>
                                              <a href="<?php echo base_url('product/addToCartANDCheckout/'.$pro->id); ?>" class="site-btn" style="background: #053f7d;"><i class="flaticon-bag"></i>Buy Now</a>
												<input type ='submit' name='product_deatils' class="site-btn" style="background: #053f7d;" value='Quick Order'>
												
												</form>
												
                       
                       <!-- <div class="social-sharing">
                            <a href="https://www.instagram.com/earthrosystem/" target="_blank"><i class="fa fa-instagram"></i></a>
                            <a href="https://in.pinterest.com/earthrosystem/"><i class="fa fa-pinterest"></i></a>
                            <a href="https://www.facebook.com/earthwaterpurifier/" target="_blank"><i class="fa fa-facebook"></i></a>
                            <a href="https://twitter.com/Earthrosystem" target="_blank"><i class="fa fa-twitter twitter-share-button"></i></a>
                            <a href="https://earthwaterpurifier.tumblr.com/" target="_blank"><i class="fa fa-tumblr"></i></a>
                        </div>-->
                        <div id="fb-root"></div>
  <script>(function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v3.0";
    fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));</script>

  <!-- Your share button code 
  <div class="fb-share-button" 
    data-href="<?php echo htmlentities($pro->url);?>" 
    data-layout="button_count">
  </div>-->
  
                       <!-- <div id="accordion" class="accordion-area">
                            <div class="panel">
                                <div class="panel-header" id="headingOne">
                                    <button class="panel-link active" data-toggle="collapse" data-target="#collapse1" aria-expanded="true" aria-controls="collapse1"></button>
                                </div>
                                <div id="collapse1" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                                    <div class="panel-body" style="text-align: justify;">
                                        <p><?php //echo $pro->productDescription; ?></p>
                                    </div>
                                </div>
                            </div>
                        </div>-->

                    </div>
                </div>
            </div>
        </section>
                    <!--  section end --> 
                    <div class="scroll-nav-wrapper fl-wrap" id='form'>
                        <div class="container">
                            <nav class="scroll-nav scroll-init">
                                <ul>
                                   
                                </ul>
                            </nav>
                          
                        </div>
                    </div>
                    <!--  section  --> 
                    <section class="gray-section no-top-padding">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-8">
                                    <div class="list-single-main-wrapper fl-wrap" id="sec2">
                                        
                        <div id="sec4" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                                   
                                      <div class="panel-body" style="text-align: justify;">
                                        <p><?php echo $pro->productDescription;?></p>
                                    </div>
                                       
                                  
                                </div>
                                         <!--<div class="list-single-main-item fl-wrap" id="sec3">
                                            <div class="list-single-main-item-title fl-wrap">
                                                <h3>Gallery - Photos</h3>
                                            </div>
                                           
                                           <div class="gallery-items grid-small-pad  list-single-gallery three-coulms lightgallery">
                                               
                                                <div class="gallery-item">
                                                    <div class="grid-item-holder">
                                                        <div class="box-item">
                                                            <img  src="<?=base_url()?>assets/productimages/<?php echo htmlentities($pro->id);?>/<?php echo htmlentities($pro->productImage1);?>"   alt="">
                                                            <a href="<?=base_url()?>assets/productimages/<?php echo htmlentities($pro->id);?>/<?php echo htmlentities($pro->productImage1);?>" class="gal-link popup-image"><i class="fa fa-search"  ></i></a>
                                                        </div>
                                                    </div>
                                                </div>
                                              
                                                <div class="gallery-item">
                                                    <div class="grid-item-holder">
                                                        <div class="box-item">
                                                            <img  src="<?=base_url()?>assets/productimages/<?php echo htmlentities($pro->id);?>/<?php echo htmlentities($pro->productImage2);?>"   alt="">
                                                            <a href="<?=base_url()?>assets/productimages/<?php echo htmlentities($pro->id);?>/<?php echo htmlentities($pro->productImage2);?>" class="gal-link popup-image"><i class="fa fa-search"  ></i></a>
                                                        </div>
                                                    </div>
                                                </div>
                                              
                                                <div class="gallery-item">
                                                    <div class="grid-item-holder">
                                                        <div class="box-item">
                                                            <img  src="<?=base_url()?>assets/productimages/<?php echo htmlentities($pro->id);?>/<?php echo htmlentities($pro->productImage3);?>"   alt="">
                                                            <a href="<?=base_url()?>assets/productimages/<?php echo htmlentities($pro->id);?>/<?php echo htmlentities($pro->productImage3);?>" class="gal-link popup-image"><i class="fa fa-search"  ></i></a>
                                                        </div>
                                                    </div>
                                                </div>
                                              
                                               
                                            </div>
                                            <!-- end gallery items -->                                 
                                        </div>
                                        <!-- list-single-main-item end --> 
                                        
                                          <!--section -->
                 
                                    </div>
                                </div>
                                <!--box-widget-wrap -->
                                <div class="col-md-4">
                                    <div class="box-widget-wrap">
                                        <!--box-widget-item -->
                                        <div class="box-widget-item fl-wrap">
                                            <div class="box-widget-item-header">
                                               
                                            </div>
                                           <!-- <div class="box-widget opening-hours">
                                                <div class="box-widget-content">
                                                    <form class="add-comment custom-form" action='<?=base_url()?>Home/order_process' method='POST'>
                                                        <fieldset>
                                                            <label><i class="fa fa-user-o"></i></label>
                                                            <input type="text" name='name'id='name' placeholder="Your Name *" value="" required/>
                                                            <input type="hidden" name='product_id' value="<?php echo htmlentities($pro->id);?>" />
                                                             <input type="hidden" name='product_name' value="<?php echo htmlentities($pro->productName);?>" />
                                                            <div class="clearfix"></div>
                                                            <label><i class="fa fa-envelope-o"></i></label>
                                                            <input type="text" placeholder="Email Address*" id='email' name='email' value="" required/>
                                                             <label><i class="fa fa-phone"></i>  </label>
                                                            <input type="text" placeholder="Phone/Mobile*" id='mobile' name='mobile' value="" required />
                                                             
                                                            <textarea cols="40" rows="3" name ='address' placeholder="Address:" stype='margin-top: 12px;'></textarea>
                                                             <select id="inputState" class="form-control" name="method" style="height:45px;width:320px;">
                                                                <option selected="">Choose...</option>
                                                               <option value="">Select Payment Method</option>
                                                                    <option value="COD">COD</option>
                                                                    <option value="Online">Online</option>
                                                              </select>
                                                        </fieldset>
                                                        <button type='submit' name="inquiry" class="btn  big-btn  color-bg flat-btn" onclick=" return myFunction();">Order Now<i class="fa fa-angle-right"></i></button>
                                                    </form>
                                                </div>
                                            </div>-->
                                       
                                        <!--box-widget-item end -->                                      
                                      
                                      	
                                        
                                   </div>
                                </div>
                                <!--box-widget-wrap end -->
                            </div>
                        </div>
                    </section>
                    <!--  section end --> 
                    <div class="limit-box fl-wrap"></div>
                   
                </div>
                <!--  content end  --> 
            </div>
            <!-- wrapper end -->
            <!--footer -->
            <?php include 'footer.php'; ?>
            <!--footer end  -->
            <!--register form -->
            <?php include 'inquiry-form.php'; ?>
            <!--register form end -->
            <a class="to-top"><i class="fa fa-angle-up"></i></a>
        </div>
        <script type="application/ld+json">
	{
		"@context": "http://schema.org/",
		"@type": "Product", "name": "<?php echo $pro->productCompany; ?>",
		"image": "<?=base_url()?>assets/productimages/<?php echo htmlentities($pro->id);?>/<?php echo htmlentities($pro->productImage1);?>",
		"description": "<?php echo $pro->productCompany; ?>",
		"brand": { "@type": "Brand", "name": "earthwaterpurifier.com" },
		"review": {
					"@type": "Review",
		        	"reviewRating": { "@type": "Rating", "ratingValue": "4.8", 
                                "bestRating": "5" 
                              },
					"author": { "@type": "person", "name": "Rohit" },
        			"reviewBody": "Nice Service"
                  },
                        
		"aggregateRating": { "@type": "AggregateRating", "ratingValue": "4.8", "reviewCount": "1712" },
		"offers": {
					"@type": "AggregateOffer",
					"priceCurrency": "INR",
					"lowprice": "<?php echo $pro->productPrice; ?>",
					"offercount": "3",                    
					"seller": { "@type": "Organization", "name": "earthwaterpurifier.com" }
					}
				}
</script>
        <!-- Main end -->
        <!--=============== scripts  ===============-->
        <script type="text/javascript" src="<?=base_url()?>assets/js/jquery.min.js"></script>
        <script type="text/javascript" src="<?=base_url()?>assets/js/plugins.js"></script>
        <script type="text/javascript" src="<?=base_url()?>assets/js/scripts.js"></script>			
    </body>
</html>