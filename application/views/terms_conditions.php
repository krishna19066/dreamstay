<!DOCTYPE HTML>
<html lang="en">
<?php include 'header.php'; ?>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
  <body>
        <!--loader-->
        <div class="loader-wrap" style="display: none;">
            <div class="pin"></div>
            <div class="pulse"></div>
        </div>
        <!--loader end-->
        <!-- Main  -->
        <div id="main" style="opacity: 1;">
            <!-- header-->
            <?php include 'menu.php'; ?>
            <!--  header end -->	
            <!-- wrapper -->	
            <div id="wrapper">
                <!--content-->  
                <div class="content">
                    <section class="parallax-section" data-scrollax-parent="true">
                        <div class="bg par-elem " data-bg="<?=base_url()?>/assets/images/bg/6.jpg" data-scrollax="properties: { translateY: '30%' }" style="background-image: url(&quot;<?=base_url()?>assets/images/bg/6.jpg&quot;); transform: translateZ(0px) translateY(-5.44218%); background-size: cover;"></div>
                        <div class="overlay"></div>
                        <div class="bubble-bg"><div class="bubbles"><div class="individual-bubble" style="left: 506px; width: 15px; height: 15px; opacity: 0.00084872;"></div><div class="individual-bubble" style="left: 83px; width: 20px; height: 20px; opacity: 0.00591872;"></div><div class="individual-bubble" style="left: 96px; width: 20px; height: 20px; opacity: 0.0158776;"></div><div class="individual-bubble" style="left: 236px; width: 5px; height: 5px; opacity: 0.0308016;"></div><div class="individual-bubble" style="left: 418px; width: 10px; height: 10px; opacity: 0.051136;"></div><div class="individual-bubble" style="left: 937px; width: 20px; height: 20px; opacity: 0.07605;"></div><div class="individual-bubble" style="left: 1170px; width: 5px; height: 5px; opacity: 0.105708;"></div><div class="individual-bubble" style="left: 236px; width: 5px; height: 5px; opacity: 0.139603;"></div><div class="individual-bubble" style="left: 770px; width: 20px; height: 20px; opacity: 0.18024;"></div><div class="individual-bubble" style="left: 1144px; width: 20px; height: 20px; opacity: 0.22445;"></div><div class="individual-bubble" style="left: 1117px; width: 15px; height: 15px; opacity: 0.273948;"></div><div class="individual-bubble" style="left: 927px; width: 15px; height: 15px; opacity: 0.326755;"></div><div class="individual-bubble" style="left: 688px; width: 5px; height: 5px; opacity: 0.387904;"></div><div class="individual-bubble" style="left: 16px; width: 5px; height: 5px; opacity: 0.45125;"></div></div></div>
                        <div class="container">
                            <div class="section-title center-align">
                                <h2><span>Our Contacts</span></h2>
                                <div class="breadcrumbs fl-wrap"><a href="#">Home</a> <span>Contacts</span></div>
                                <span class="section-separator"></span>
                            </div>
                        </div>
                        <div class="header-sec-link">
                            <div class="container"><a href="#sec1" class="custom-scroll-link">Let's Start</a></div>
                        </div>
                    </section>
                    <!-- section end -->
                    <!--section -->  
                    <section id="sec1">
                        <div class="container">
                            <div class="row">
                              
                                <div class="col-md-12" style="text-align: justify;">
                                        <div class="list-single-main-item-title fl-wrap">
                                            <h3>Terms and Conditions</h3>
                                        </div>
                                    
                                          <!-- Category section -->
                          
                                <div class="row">
                                <p><b>MODEL 1 :-      (M1)  STATE</b>
<p>. PRODUCTS :- All types of Earth parts support to M2, M3 services centers.</p>
<p>. INVESTMENT REFINED :- 50000/- to 1 lakh including 10000/-  security amount, you will get spare of 40000/- .</p>
<p>.REQUIREMENT:- Technician in all districts in the state.</p>
.RETURN ON  INVESTMENT:- 
 1.Rs 500 on installation .<br/>
2.Rs 250 on service visit .<br/>
3.Parts charge rs. 250 on only Earth parts on company rate.<br/>
4.Out of warrenty Earth parts on customer rate.<br/>
5.Service centre will get all warrenty parts.<br/>
.SERVICE CENTER FACTS :-
Technician required 5 to 10 .
Investment : minimum  rs. 50000 .</p>      


<p>If you feel you have above qualities and want to join the revolution, then contact us today and apply for the service center .</p>




<p>
<b>MODEL 2  :- (M2 )District</b><br/>
. PRODUCTS:- All types of Earth spare parts.<br/>
.Investment Required:- rs. 10000 & 5000 for opening code and security amount . 5000 /- you will get parts .<br/>
.REQUIREMENT ON INVESTMENT :-<br/>
. 400 per installation.<br/>
Rs.200 on service visit<br/>
Parts charge rs. 200 on only  Earth parts on company rate.<br/>
Out of warranty Earth parts on customer rate.<br/>
Service center will get all warrenty parts.<br/>

SERVICE CENTER REQUIREMENT:-<br/>
Technician should do services  in 48 hrs.<br/>
Life of agreement is 11 months and then it will renew for every 11 months.<br/>
Customer should be satisfy and give 5 star ratings to product.<br/>
Technician should be trend and good behavior with
 Customers.<br/>
Earth Ro system have all rights to change or modify the terms & conditions .<br/>
If company found any kind of dishonesty then company can terminate the agreement and seize the security.

Thanks & Regards 
EARTH RO SYSTEM
</p>
                           
        <!-- Category section end --> 
                    <!-- section end -->
                    <div class="limit-box fl-wrap"></div>
                   </div>
                </div>
                </div>
                <!-- contentend -->
            </div>
            <!-- wrapper end -->
            <!--footer -->
               <?php include 'footer.php'; ?>
            <!--footer end  -->
            <!--register form -->
          	 <?php include 'inquiry-form.php'; ?>
            <!--register form end -->
        </div>
        <!-- Main end -->
        <!--=============== scripts  ===============-->
        <script type="text/javascript" src="js/jquery.min.js"></script>
        <script type="text/javascript" src="js/plugins.js"></script>
        <script type="text/javascript" src="js/scripts.js"></script>   
        <script type="text/javascript" src="js/map_infobox.js"></script>
        <script type="text/javascript" src="js/markerclusterer.js"></script>  
        <script type="text/javascript" src="js/maps.js"></script>
</html>