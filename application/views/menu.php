<?php
function isMobileDevice() {
    return preg_match("/(android|avantgo|blackberry|bolt|boost|cricket|docomo|fone|hiptop|mini|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i", $_SERVER["HTTP_USER_AGENT"]);
}
?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<header class="main-header dark-header fs-header sticky" style="height: 90px; ">
    <div class="header-inner">
        <div class="logo-holder" style="height:200px; top:6px;">
            <!--<a href="index.php"><img src="images/logo.png" alt=""></a>-->
            <a href="/"><img src="<?= base_url() ?>assets/images/logo2.png"  alt=" logo" style="margin-top:-10px; <?php if(isMobileDevice()) { ?>height: 100px; <?php } ?> "/></a>
               <!-- <a href="/"><img src="https://i.ibb.co/wJZtQj3/logo-apart-01.png"  alt="AYoola logo" style="width: 167px;"/></a> -->
           <!-- <a href="/"><img src="https://static.hotel-imagenes.info/uploads/web-configuration/73691/resource/logo-1577981984.png"  alt="AYoola logo" style=""/></a> -->

        </div>
        <div class="header-search vis-header-search">                        
            <div id="google_translate_element" style="display: none"></div><script type="text/javascript">
                function googleTranslateElementInit() {
                    new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.InlineLayout.SIMPLE}, 'google_translate_element');
                }
            </script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>                                         
            <div class="nav-holder main-menu dropdown" style="margin-top: -19px;">
                <nav>
                    <li style="display:block;">
                        <a href="#"  id="dLabel" class="act-link notranslate btn dropdown-toggle" style="border: 1px solid;color: #17aaa7; "><img src="https://i.ibb.co/82r7d30/united-states-of-america-1.png" class="flag flag-en"> English  <i class="fa fa-caret-down"></i></a>
                        <!--second level -->
                        <ul class="dropdown-menu language_dropdown" style="background-color: #f8f8f8;" >
                            <li style="text-align: center;" class="notranslate"><a href="javascript:;" id="Spanish" style="color: black;" ><img class="flag flag-es">Spanish </a></li> 
                            <li style="text-align: center;" class="notranslate"><a href="javascript:;" id="Hindi" style="color: black;"><img class="flag flag-hi">Hindi </a></li>
                            <li style="text-align: center;" class="notranslate"><a href="javascript:;" id="English" style="color: black;"><img src="https://i.ibb.co/82r7d30/united-states-of-america-1.png" class='flag'> English </a> </li>
                            <li style="text-align: center;" class="notranslate"><a href="javascript:;" id="French" style="color: black;"><img class="flag flag-fr">French</a></li>                           
                            <li style="text-align: center;" class="notranslate"><a href="javascript:;" id="German" style="color: black;"><img class="flag flag-de">German</a></li>                               
                            <li style="text-align: center;" class="notranslate"><a href="javascript:;" id="Japanese"style="color:black;" ><img class="flag flag-ja"> Japanese</a></li>                       
                        </ul>             
                    </li>
                </nav>
            </div>

            <script>
                $(".dropdown-menu li a").click(function () {
                    var selText = $(this).text();
                    var img = $(this).parent().find("img").attr("class");
                    $(this).parents('.dropdown').find('.dropdown-toggle').html('<img class="' + img + '">' + selText + ' <span class="caret"></span>');
                });
                function translate(lang) {
                    var $frame = $('.goog-te-menu-frame:first');
                    if (!$frame.size()) {
                        alert("Error: Could not find Google translate frame.");
                        return false;
                    }
                    $frame.contents().find('.goog-te-menu2-item span.text:contains(' + lang + ')').get(0).click();
                    return false;
                }
                $(".dropdown-menu.language_dropdown").on("click", "a", function () {
                    translate($(this).attr("id"));
                });
            </script>                     
        </div>
           <!-- <div class="show-search-button"><i class="fa fa-search"></i> <span>Search</span></div>
<a href="#" class="add-list show-reg-form modal-open">Book Now <span><i class="fa fa-plus"></i></span></a>-->                   
        <div class="show-reg-form modal-open" style="color:#18aaa7;border: 1px solid;height: 47px;width: 100px;padding-top: 12px;border-radius: 6px;margin-top: -15px;font-size: 15px;">Book Now 

        </div>

        <!-- nav-button-wrap--> 
        <div class="nav-button-wrap color-bg">
            <div class="nav-button">
                <span></span><span></span><span></span>
            </div>
        </div>
        <!-- nav-button-wrap end-->
        <!--  navigation --> 
        <div class="nav-holder main-menu">
            <nav>
                <ul>
                    <li>
                        <a href="/" class="act-link">Home</a>

                    </li>
                    <li>
                        <a href="<?= base_url() ?>service-appartments">Our Dream Place to Stay</a>

                    </li>
                    <li>
                        <a href="<?= base_url() ?>about-us">About Us</a>

                    </li>
                    <li>
                        <a href="<?= base_url() ?>contact-us">Contact us</a>

                    </li>
                    <!-- <li>                                      
                         <a   href=""  class="dropbtn">More<i class="fa fa-caret-down"></i> </a>
                         <ul style="background-color:#f8f8f8; ">
                             <a href="business.html" >Business Travel</a>
                             <a href="things-to-do.html" >Things To Do</a>
                         </ul>                                    
                     </li>-->

                </ul>
            </nav>
        </div>
        <!-- navigation  end -->
    </div>
</header>           
<!--  header Menu end --> 