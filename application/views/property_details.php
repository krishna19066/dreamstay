
<!DOCTYPE HTML>
<html lang="en">
    <head>
        <?php include 'header.php'; ?>
        <?php
        foreach ($property_data as $prop) {
            $propID = $prop->id;
        }

        $get_images = $this->UserModel->getPropertyImg($propID);
        foreach ($get_images as $img) {
            $feature_img = $img->img;
        }
        ?>
        <style >

            @media only screen and (min-width: 367px) and (max-width: 434px){
                .show-reg-form {/*
                    margin-right: 58px!important;*/
                }
                .nav-button-wrap{
                    /*margin-top: -103px;*/
                }
            }

            @media only screen and (max-width: 366px){
                .nav-button-wrap{
                    /*margin-top: -66px;*/
                }    
            }
            @media only screen and (max-width: 540px){
                .show-reg-form {
                    /*   margin-top:-112px !important;*/
                }
                .para{
                    display: none;
                }
            }


            .lang_btn_mobile_li{
                border: 1px solid #1a1a1b8c;
                padding: 7px;
                font-size: 12px;
                border-radius: 6px;
                font-weight: 500;
                width: 115px;
                margin-left: 61px;
                margin-bottom: 5px;
                color: #1a1a1b8c;
            }

            .imagebottom{
                width: 100%;
                height: auto;
                margin-bottom: 20px;
            }

        </style>

        <style type="text/css">

            .nice-select-search-box{
                display: none;
            }
            .custom-form .nice-select .list{
                padding:10px 12px 10px;
            }
            .main-search-input-item .nice-select .list{
                height: 181px !important;
                padding: 10px 12px 10px !important;
            }

            .fb-messengermessageus{
                position: fixed!important;
                bottom: 10px!important;
                right: 10px!important;
                z-index: 3;
            }
        </style>

        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBWTUjQQpfGQ8vWfn7qc7Qw4q_mbtJh-kY"></script>
        <script>
            function initMap() {
                var map;
                var bounds = new google.maps.LatLngBounds();
                var mapOptions = {
                    mapTypeId: 'roadmap'
                };
                // Display a map on the web page
                map = new google.maps.Map(document.getElementById("mapCanvas"), mapOptions);
                map.setTilt(100);

                // Multiple markers location, latitude, and longitude
                //var addresses = ['Norway', 'Africa', 'Asia','North America','South America'];

                var markers = [
                    ["<?php echo $prop->propertyName ?>", <?php echo $prop->lat ?>, <?php echo $prop->long ?>], ];
                // Info window content
                var infoWindowContent = [
                    [
                        '<div class="card mb-6">' +
                                '<img class="card-img-top" src="https://res.cloudinary.com/dx5wgzxxo/image/upload/v1592120382/dreamstay/property/<?php echo $feature_img; ?>.jpg" alt="Card image cap">' +
                                '<div class="card-body"">' +
                                '<h5 class="card-title" style="font-size: 16px;color: #2196F3;"><?php echo $prop->propertyName ?></h5>' +
                                '<p class="card-text" style="font-size:12px;color: #666;"><i class="fa fa-map-marker" aria-hidden="true" style="color: #2196f3;"></i> <?php echo $prop->address; ?></p>' +
                                '</div>' +
                                '</div>'
                    ],
                ];
                // Add multiple markers to map
                var infoWindow = new google.maps.InfoWindow(), marker, i;
                // Place each marker on the map  
                for (i = 0; i < markers.length; i++) {
                    var position = new google.maps.LatLng(markers[i][1], markers[i][2]);
                    var mar_icon = new google.maps.MarkerImage("https://i.ibb.co/CJ1v5tt/red.png", null, null, null, new google.maps.Size(32, 46)); // Create a variable for our marker image.

                    bounds.extend(position);

                    marker = new google.maps.Marker({
                        position: position,
                        map: map,
                        icon: mar_icon,
                        animation: google.maps.Animation.DROP,
                        title: markers[i][0]
                    });
                    // Add info window to marker    
                    google.maps.event.addListener(marker, 'click', (function (marker, i) {
                        return function () {
                            infoWindow.setContent(infoWindowContent[i][0]);
                            infoWindow.open(map, marker);
                        }
                    })(marker, i));

                    // Center the map to fit all markers on the screen
                    map.fitBounds(bounds);
                }
                // Set zoom level
                var boundsListener = google.maps.event.addListener((map), 'bounds_changed', function (event) {
                    this.setZoom(12);
                    google.maps.event.removeListener(boundsListener);
                });
            }
            // Load initialize function
            google.maps.event.addDomListener(window, 'load', initMap);
        </script>
        <style>
            #mapCanvas {
                width: 100%;
                height: 400px;   
            }
            del{
                text-decoration: line-through !important;
                color: gray!important;
            }

            .icon-wrap{
                float: left;
            }


            .nice-select-search-box{
                display: none;
            }
            .custom-form .nice-select .list{
                padding:10px 12px 10px;
            }
        </style>      

    </head>
    <body>
        <!--loader-->
        <div class="loader-wrap">
            <div class="pin"></div>
            <div class="pulse"></div>
        </div>
        <!--loader end-->
        <!-- Main  -->

        <div id="main">
            <!-- header-->
            <style>
                .skiptranslate { display:none; }

                @media only screen and (max-width: 450px){
                    .logo-img{
                        width:163px!important;
                    }
                }
            </style>

            <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
            <?php include 'menu.php'; ?>  
            <!--  header end -->	
            <!-- wrapper -->	
            <div id="wrapper">
                <!--  content  --> 
                <div class="content">
                    <!--  section  --> 
                    <section class="parallax-section single-par list-single-section" data-scrollax-parent="true" id="sec1">
                        <div class="slideshow-container" data-scrollax="properties: { translateY: '200px' }" >
                            <!-- slideshow-item -->	
                            <div class="slideshow-item">
                                <?php
                                $get_images = $this->UserModel->getPropertyImg($propID);
                                foreach ($get_images as $img) {
                                    ?>
                                    <div class="bg" data-bg="https://res.cloudinary.com/dx5wgzxxo/image/upload/v1592120382/dreamstay/property/<?php echo $img->img; ?>.jpg"></div>
                                <?php } ?> 
                            </div>
                            <!--  slideshow-item end -->	                        
                        </div>
                        <div class="overlay"></div>
                        <div class="bubble-bg"></div>
                        <div class="list-single-header absolute-header fl-wrap">
                            <div class="container">
                                <div class="list-single-header-item">
                                    <div class="list-single-header-item-opt fl-wrap">
                                        <div class="list-single-header-cat fl-wrap">
                                            <a href="#" style="font-size: 15px;">Property</a>											
                                            <span style="font-size: 15px;">Feature<i class="fa fa-check"></i></span>
                                        </div>
                                    </div>
                                    <h2><?php echo $prop->propertyName; ?><!--<span> - Hosted By </span><a href="author-single.html">Alisa Noory</a>--> </h2>
                                    <span class="section-separator"></span>                                                                
                                    <div class="clearfix"></div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="list-single-header-contacts fl-wrap">
                                               <!-- <ul>
                                                    <li><i class="fa fa-phone"></i><a href="tel:<?php echo $prop->propertyMob; ?>"><?php echo $prop->propertyMob; ?></a></li>
                                                    <br>
                                                    <li><i class="fa fa-envelope-o"></i><a href="mail:airbnbofmalik@gmail.com">airbnbofmalik@gmail.com</a></li>
                                                    <li style="text-align: left;width:80%;"><i class="fa fa-map-marker" style="margin-right: 5px;float: left;margin-bottom: 25px;" ></i><a href="#"><?php echo $prop->address; ?></a></li>
                                                </ul>-->
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="fl-wrap list-single-header-column">
                                                <!-- <div class="share-holder hid-share">
                                                     <div class="showshare"><span>Share </span><i class="fa fa-share"></i></div>
                                                     <div class="share-container  isShare"></div>
                                                 </div>-->
                                                <!--<span class="viewed-counter"><i class="fa fa-eye"></i> Viewed -  156 </span>-->											
                                                <a class="custom-scroll-link" href="#sec6"><i class="fa fa-hand-o-right"></i>View Photo </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>

                    <!--  section end --> 
                    <div class="scroll-nav-wrapper fl-wrap">
                        <div class="container">
                            <nav class="scroll-nav scroll-init">
                                <ul>
                                    <li><a class="act-scrlink" href="#sec1">Top</a></li>
                                    <li><a href="#sec3">Overview</a></li>
                                    <li><a href="#sec4">Amenities</a></li>
                                    <li><a href="#sec5">Location</a></li>
                                    <li><a href="#sec6">Gallery</a></li>

                                </ul>
                            </nav>
                        </div>
                    </div>
                    <style>
                        .team-box > .team-photo >.list-single-gallery{
                            float:none;
                        }
                        .search_hide{
                            color:white;
                        }
                    </style>
                    <!-- Room section  --> 
                    <section class="gray-section no-top-padding">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-8">
                                    <div class="list-single-main-wrapper fl-wrap" id="sec1">
                                        <div class="breadcrumbs gradient-bg  fl-wrap"><a href="#">Home</a><a href="#">Listings</a><span>Property</span></div>
                                        <!-- list-single-main-item -->
                                        <div class="list-single-main-item fl-wrap" id="sec2">

                                            <!--                                        <div class="team-holder fl-wrap">
                                                                                        <div class="list-single-main-item-title fl-wrap">
                                                                                            <h3>Room Details</h3>
                                                                                        </div>
                                            select `14` from manage_rate where room_id='444' and property_id='5533' and month='06' and year='2020'                                                                                                 team-item 
                                                    
                                                    
                                                                                                    <div class="team-box ">
                                                                                                        <div class="team-photo">
                                                                                                                <img src="https://res.cloudinary.com//image/upload/reputize/room/.jpg" alt="" title="" alt="" class="respimg">
                                                    
                                                                                                            <div class="gallery-items grid-small-pad  list-single-gallery three-coulms lightgallery" style="height=auto!important; float:none;">
                                                                                                                 1 
                                                    
                                                    
                                                                                                                                                                                       <img src="images/icon/washing-machine.png" alt="Washing Machine" title="Washing Machine"/>
                                                                                                                                                                               <a href="https://res.cloudinary.com/dx8xpz8co/image/upload/reputize/room/2020-05-04_15-45-12.jpg"  alt=""  class="search_hide  popup-image" ><i class="fa fa-search"></i></a>
                                                                                                                                                                                    <a href="https://res.cloudinary.com/dx8xpz8co/image/upload/reputize/room/2020-05-12_16-18-48.jpg"  alt=""  class="search_hide  popup-image" ><i class="fa fa-search"></i></a>
                                                                                                                                                                                    <a href="https://res.cloudinary.com/dx8xpz8co/image/upload/reputize/room/2020-05-12_16-10-30.jpg"  alt=""  class="search_hide  popup-image" ><i class="fa fa-search"></i></a>
                                                                                                                                                                                    <a href="https://res.cloudinary.com/dx8xpz8co/image/upload/reputize/room/2020-05-07_19-19-29.jpg"  alt=""  class="search_hide  popup-image" ><i class="fa fa-search"></i></a>
                                                                                                                         <div class="pricerange fl-wrap">
                                                    
                                                <del><i class="fa fa-usd" aria-hidden="true"></i> 101</del> <span>Price : </span><i class="fa fa-usd"></i>101                                                                                                                            </div>
                                                    
                                                                                                                            </div>						 
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                            box-widget-item end 
                                                                                                        </div>
                                                                                                    </div>
                                                                                                                                 team-item  end                                             
                                                                                    </div>-->
                                            <style>
                                                .morecontent span {
                                                    display: none;
                                                }
                                                .morelink {
                                                    display: block;
                                                }
                                            </style>
                                            <script>
            $(document).ready(function () {
                // Configure/customize these variables.
                var showChar = 300;  // How many characters are shown by default
                var ellipsestext = "";
                var moretext = "Read More";
                var lesstext = "Read Less";
                $('.pro_overview > p').each(function () {
                    var content = $(this).html();
                    if (content.length > showChar) {
                        var c = content.substr(0, showChar);
                        var h = content.substr(showChar, content.length - showChar);
                        var html = c + '<span class="moreellipses">' + ellipsestext + '&nbsp;</span><span class="morecontent"><span>' + h + '</span>&nbsp;&nbsp;<a href="" class="morelink" style="color: #2792c1;">' + moretext + '</a></span>';
                        $(this).html(html);
                    }
                });

                $(".morelink").click(function () {
                    if ($(this).hasClass("less")) {
                        $(this).removeClass("less");
                        $(this).html(moretext);
                    } else {
                        $(this).addClass("less");
                        $(this).html(lesstext);
                    }
                    $(this).parent().prev().toggle();
                    $(this).prev().toggle();
                    return false;
                });
            });
                                            </script>

                                            <div class="list-single-main-item-title fl-wrap" id="sec3">
                                                <h3>Property Overview</h3>
                                            </div>
                                            <div class="pro_overview">
                                                <p><?php echo $prop->propertyInfo; ?><p>
                                            </div>
                                            <span class="fw-separator"></span>
                                            <div class="list-single-main-item-title fl-wrap" id='sec4'>
                                                <h3>Amenities</h3>
                                            </div>
                                            <style type="text/css">
                                                .icon-label{
                                                    height: 60px;
                                                }
                                                .amenity{

                                                }

                                                .no-gutters{

                                                }
                                                .ml-1{
                                                    width: 50%;
                                                    float: left;
                                                    margin-top: 10px;
                                                    text-align: left;
                                                }
                                                }
                                                .icon-wrap{
                                                    float: left;
                                                    width: 50%;
                                                }


                                                .amenity img{
                                                    width: 25px;
                                                    margin-right: 10px;
                                                    margin-top: 5px;
                                                }
                                            </style>
                                            <div class=" fl-wrap ">

                                                <div class="row no-gutters">
                                                    <div class="amenity col-md-4 col-sm-4 mb-4" style="">
                                                        <div class="icon-label">
                                                            <div class="icon-wrap">
                                                                <img src="<?= base_url() ?>assets/images/icon/elevator.png" alt="elevator">
                                                            </div>
                                                            <div class="ml-1">Elevator</div>
                                                        </div>
                                                    </div>

                                                    <div class="amenity col-md-4 col-sm-6 mb-3"><div class="icon-label d-flex align-items-center flex-row"><div class="icon-wrap"><img src="<?= base_url() ?>assets/images/icon/wheelchair-190.png" alt="wheelchair"></div><div class="ml-1">Wheelchair Accessible</div></div></div><div class="amenity col-md-4 col-sm-6 mb-3"><div class="icon-label d-flex align-items-center flex-row"><div class="icon-wrap"><img src="<?= base_url() ?>assets/images/icon/gym (1).png" alt="gym"></div><div class="ml-1">Gym</div></div></div><div class="amenity col-md-4 col-sm-6 mb-3"><div class="icon-label d-flex align-items-center flex-row"><div class="icon-wrap"><img src="<?= base_url() ?>assets/images/icon/heating.png" alt="heating"></div><div class="ml-1">Heating</div></div></div><div class="amenity col-md-4 col-sm-6 mb-3"><div class="icon-label d-flex align-items-center flex-row"><div class="icon-wrap"><img src="<?= base_url() ?>assets/images/icon/Wireless.png" alt="wifi"></div><div class="ml-1">Wireless Internet</div></div></div><div class="amenity col-md-4 col-sm-6 mb-3"><div class="icon-label d-flex align-items-center flex-row"><div class="icon-wrap"><img src="<?= base_url() ?>assets/images/icon/internet.png" alt="internet"></div><div class="ml-1">Internet</div></div></div><div class="amenity col-md-4 col-sm-6 mb-3"><div class="icon-label d-flex align-items-center flex-row"><div class="icon-wrap"><img src="<?= base_url() ?>assets/images/icon/laptopFriendly.png" alt="laptop"></div><div class="ml-1">Laptop Friendly Workspace</div></div></div><div class="amenity col-md-4 col-sm-6 mb-3"><div class="icon-label d-flex align-items-center flex-row"><div class="icon-wrap"><img src="<?= base_url() ?>assets/images/icon/Kitchen.png" alt="kitchen"></div><div class="ml-1">Kitchen</div></div></div><div class="amenity col-md-4 col-sm-6 mb-3"><div class="icon-label d-flex align-items-center flex-row"><div class="icon-wrap"><img src="<?= base_url() ?>assets/images/icon/dishwasher-2170173.png" alt="washer"></div><div class="ml-1">Washer</div></div></div><div class="amenity col-md-4 col-sm-6 mb-3"><div class="icon-label d-flex align-items-center flex-row"><div class="icon-wrap"><img src="<?= base_url() ?>assets/images/icon/Dryer.png" alt="dryer"></div><div class="ml-1">Dryer</div></div></div><div class="amenity col-md-4 col-sm-6 mb-3"><div class="icon-label d-flex align-items-center flex-row"><div class="icon-wrap"><img src="<?= base_url() ?>assets/images/icon/hair-dryer-.png" alt="hair_dryer"></div><div class="ml-1">Hair Dryer</div></div></div>
                                                    <div class="amenity col-md-4 col-sm-6 mb-3"><div class="icon-label d-flex align-items-center flex-row"><div class="icon-wrap"><img src="<?= base_url() ?>assets/images/icon/iron-2402056.png" alt="iron"></div><div class="ml-1">Iron</div></div></div>

                                                    <span class="amenitydisplay" style="display: none;">

                                                        <div class="amenity col-md-4 col-sm-6 mb-3"><div class="icon-label d-flex align-items-center flex-row"><div class="icon-wrap"><img src="<?= base_url() ?>assets/images/icon/TV.png" alt="tv"></div><div class="ml-1">TV</div></div></div><div class="amenity col-md-4 col-sm-6 mb-3"><div class="icon-label d-flex align-items-center flex-row"><div class="icon-wrap"><img src="<?= base_url() ?>assets/images/icon/Essentials.png" alt="essentials"></div><div class="ml-1">Essentials</div></div></div><div class="amenity col-md-4 col-sm-6 mb-3"><div class="icon-label d-flex align-items-center flex-row"><div class="icon-wrap"><img src="<?= base_url() ?>assets/images/icon/FirstAid.png" alt="first_aid"></div><div class="ml-1">First Aid Kit</div></div></div><div class="amenity col-md-4 col-sm-6 mb-3"><div class="icon-label d-flex align-items-center flex-row"><div class="icon-wrap"><img src="<?= base_url() ?>assets/images/icon/Shampoo.png" alt="shampoo"></div><div class="ml-1">Shampoo</div></div></div><div class="amenity col-md-4 col-sm-6 mb-3"><div class="icon-label d-flex align-items-center flex-row"><div class="icon-wrap"><img src="<?= base_url() ?>assets/images/icon/hanger.png" alt="hangers"></div><div class="ml-1">Hangers</div></div></div><div class="amenity col-md-4 col-sm-6 mb-3"><div class="icon-label d-flex align-items-center flex-row"><div class="icon-wrap"><img src="<?= base_url() ?>assets/images/icon/carbon monoxide.png" alt="smoke_detector"></div><div class="ml-1">Smoke Detector</div></div></div><div class="amenity col-md-4 col-sm-6 mb-3"><div class="icon-label d-flex align-items-center flex-row"><div class="icon-wrap"><img src="<?= base_url() ?>assets/images/icon/carbon monoxide.png" alt="CM_detector"></div><div class="ml-1">Carbon Monoxide Detector</div></div></div><div class="amenity col-md-4 col-sm-6 mb-3"><div class="icon-label d-flex align-items-center flex-row"><div class="icon-wrap"><img src="<?= base_url() ?>assets/images/icon/FireExtinguisher.png" alt="fire_extinguisher"></div><div class="ml-1">Fire Extinguisher</div></div></div><div class="amenity col-md-4 col-sm-6 mb-3"><div class="icon-label d-flex align-items-center flex-row"><div class="icon-wrap"><img src="<?= base_url() ?>assets/images/icon/familyfriednly.png" alt="family_friendly"></div><div class="ml-1">Family/Kid Friendly</div></div></div><div class="amenity col-md-4 col-sm-6 mb-3"><div class="icon-label d-flex align-items-center flex-row"><div class="icon-wrap"><img src="<?= base_url() ?>assets/images/icon/cutlery-1454869.png" alt="breakfast"></div><div class="ml-1">Breakfast</div></div></div><div class="amenity col-md-4 col-sm-6 mb-3"><div class="icon-label d-flex align-items-center flex-row"><div class="icon-wrap"><img src="<?= base_url() ?>assets/images/icon/bathtub-2115011.png" alt="bath"></div><div class="ml-1">Bathtub</div></div></div><div class="amenity col-md-4 col-sm-6 mb-3"><div class="icon-label d-flex align-items-center flex-row"><div class="icon-wrap"><img src="<?= base_url() ?>assets/images/icon/bedlinen.png" alt="bed"></div><div class="ml-1">Bed linens</div></div></div><div class="amenity col-md-4 col-sm-6 mb-3"><div class="icon-label d-flex align-items-center flex-row"><div class="icon-wrap"><img src="<?= base_url() ?>assets/images/icon/clearance to bedrooml.png" alt="bed"></div><div class="ml-1">Wide clearance to bed</div></div></div><div class="amenity col-md-4 col-sm-6 mb-3"><div class="icon-label d-flex align-items-center flex-row"><div class="icon-wrap"><img src="<?= base_url() ?>assets/images/icon/outletcover.png" alt="guard"></div><div class="ml-1">Outlet covers</div></div></div><div class="amenity col-md-4 col-sm-6 mb-3"><div class="icon-label d-flex align-items-center flex-row"><div class="icon-wrap"><img src="<?= base_url() ?>assets/images/icon/widehallway.png" alt="accessibility"></div><div class="ml-1">Wide hallway clearance</div></div></div><div class="amenity col-md-4 col-sm-6 mb-3"><div class="icon-label d-flex align-items-center flex-row"><div class="icon-wrap"><img src="<?= base_url() ?>assets/images/icon/stepfreeaccess.png" alt="accessibility"></div><div class="ml-1">Step-free access</div></div></div><div class="amenity col-md-4 col-sm-6 mb-3"><div class="icon-label d-flex align-items-center flex-row"><div class="icon-wrap"><img src="<?= base_url() ?>assets/images/icon/widedoorway.png" alt="accessibility"></div><div class="ml-1">Wide doorway</div></div></div>

                                                        <div class="amenity col-md-4 col-sm-6 mb-3"><div class="icon-label d-flex align-items-center flex-row"><div class="icon-wrap"><img src="<?= base_url() ?>assets/images/icon/EntranceWellLit.png" alt="accessibility"></div><div class="ml-1">Path to entrance lit at night</div></div></div><div class="amenity col-md-4 col-sm-6 mb-3"><div class="icon-label d-flex align-items-center flex-row"><div class="icon-wrap"><img src="<?= base_url() ?>assets/images/icon/microwave-1617124.png" alt="microwave"></div><div class="ml-1">Microwave</div></div></div><div class="amenity col-md-4 col-sm-6 mb-3"><div class="icon-label d-flex align-items-center flex-row"><div class="icon-wrap"><img src="<?= base_url() ?>assets/images/icon/refrigerator.png" alt="refrigerator"></div><div class="ml-1">Refrigerator</div></div></div><div class="amenity col-md-4 col-sm-6 mb-3"><div class="icon-label d-flex align-items-center flex-row"><div class="icon-wrap"><img src="<?= base_url() ?>assets/images/icon/dishwasher-2170173.png" alt="dishwasher"></div><div class="ml-1">Dishwasher</div></div></div><div class="amenity col-md-4 col-sm-6 mb-3"><div class="icon-label d-flex align-items-center flex-row"><div class="icon-wrap"><img src="<?= base_url() ?>assets/images/icon/food-beverage-restarunt-service-fork-3.png" alt="dishes"></div><div class="ml-1">Dishes and silverware</div></div></div><div class="amenity col-md-4 col-sm-6 mb-3"><div class="icon-label d-flex align-items-center flex-row"><div class="icon-wrap"><img src="<?= base_url() ?>assets/images/icon/oven-1751765.png" alt="kitchen"></div><div class="ml-1">Cookware</div></div></div><div class="amenity col-md-4 col-sm-6 mb-3"><div class="icon-label d-flex align-items-center flex-row"><div class="icon-wrap"><img src="<?= base_url() ?>assets/images/icon/oven-1751765 (1).png" alt="oven"></div><div class="ml-1">Oven</div></div></div><div class="amenity col-md-4 col-sm-6 mb-3"><div class="icon-label d-flex align-items-center flex-row"><div class="icon-wrap"><img src="<?= base_url() ?>assets/images/icon/stove-1617118.png" alt="stove"></div><div class="ml-1">Stove</div></div></div>

                                                        <div class="amenity col-md-4 col-sm-6 mb-3"><div class="icon-label d-flex align-items-center flex-row"><div class="icon-wrap"><img src="<?= base_url() ?>assets/images/icon/toll-1817253.png" alt="parking"></div><div class="ml-1">Free Parking on Premises</div></div></div>

                                                        <div class="amenity col-md-4 col-sm-6 mb-3"><div class="icon-label d-flex align-items-center flex-row"><div class="icon-wrap"><img src="<?= base_url() ?>assets/images/icon/pathway.png" alt="accessibility"></div><div class="ml-1">Flat smooth pathway to front door</div></div>
                                                        </div>
                                                    </span>
                                                    <div class="col-md-12 col-sm-12">
                                                        <button onclick="amenityfun()" 
                                                                class="btn  big-btn  color-bg flat-btn btnamenity">See More</button>
                                                    </div>
                                                </div>

                                                <script type="text/javascript">
                                                    var ctnam = 0;
                                                    function amenityfun() {
                                                        $('.amenitydisplay').toggle();

                                                        if (ctnam === 0) {
                                                            $('.btnamenity').html(' See Less');
                                                            ctnam = 1;
                                                        }
                                                        else {
                                                            ctnam = 0;
                                                            $('.btnamenity').html(' See More');
                                                        }
                                                    }
                                                </script>

                                                <style>
                                                    .list-single-main-item p {
                                                        text-align: left;
                                                        padding-left: 11px;
                                                    }
                                                    .inline-facts p{
                                                        text-align: center;
                                                        color:white;
                                                    }
                                                </style>
                                                <div class="list-single-facts fl-wrap gradient-bg" style="display: none;">
                                                    <div class="inline-facts-wrap">
                                                        <div class="inline-facts">
                                                            <i class="fa fa-male"></i>
                                                            <div class="milestone-counter">
                                                                <div class="stats animaper">
                                                                    <div style="font-size: 19px; padding: 18px; font-weight: 700;">Hotel Features</div>
                                                                </div>
                                                            </div>
                                                            <p style="color: white;"><p>Internet</p>

                                                            <p>Step Free Access</p>

                                                            <p>Family & Kid Friendly</p>

                                                            <p>Laptop Friendly Workspace</p>

                                                            <p>Smooth Pathway to Entrance</p>

                                                            <p>Well Lit Pathways at night</p>
                                                            </p>
                                                        </div>
                                                    </div>
                                                    <div class="inline-facts-wrap">
                                                        <div class="inline-facts">
                                                            <i class="fa fa-hand-peace-o"></i>
                                                            <div class="milestone-counter">
                                                                <div class="stats animaper">
                                                                    <div style="font-size: 19px; padding: 18px; font-weight: 700;">Services </div>
                                                                </div>
                                                            </div>
                                                            <p style="color: white;"><p>Washer</p>

                                                            <p>Iron</p>

                                                            <p>Dryer</p>

                                                            <p>Breakfast</p>
                                                            </p>
                                                        </div>
                                                    </div>
                                                    <div class="inline-facts-wrap">
                                                        <div class="inline-facts">
                                                            <i class="fa fa-trophy"></i>
                                                            <div class="milestone-counter">
                                                                <div class="stats animaper">
                                                                    <div style="font-size: 19px; padding: 18px; font-weight: 700;">Safety & Security</div>
                                                                </div>
                                                            </div>
                                                            <p style="color: white!important;"><p>First Aid Kit</p>

                                                            <p>Smoke Detector</p>

                                                            <p>Carbon Monoxide Detector</p>

                                                            <p>Fire Extinguisher</p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="list-single-facts fl-wrap gradient-bg" style="display: none;">
                                                    <div class="inline-facts-wrap">
                                                        <div class="inline-facts">
                                                            <i class="fa fa-male"></i>
                                                            <div class="milestone-counter">
                                                                <div class="stats animaper">
                                                                    <div style="font-size: 19px; padding: 18px; font-weight: 700;">In-Room Facilities</div>
                                                                </div>
                                                            </div>
                                                            <p style="color: white;"><p>Bed Linens</p>

                                                            <p>Outlet Covers</p>

                                                            <p>Wide Doorways</p>

                                                            <p>Shampoo</p>

                                                            <p>Hangars</p>
                                                            </p>
                                                        </div>
                                                    </div>
                                                    <div class="inline-facts-wrap">
                                                        <div class="inline-facts">
                                                            <i class="fa fa-hand-peace-o"></i>
                                                            <div class="milestone-counter">
                                                                <div class="stats animaper">
                                                                    <div style="font-size: 19px; padding: 18px; font-weight: 700;">Kitchen Features</div>
                                                                </div>
                                                            </div>
                                                            <p style="color: white;"><p>Microwave</p>

                                                            <p>Dishes & Silverware</p>

                                                            <p>Stove</p>

                                                            <p>Refrigerator</p>

                                                            <p>Cookware</p>

                                                            <p>Dishwasher</p>
                                                            </p>
                                                        </div>
                                                    </div>
                                                    <div class="inline-facts-wrap">
                                                        <div class="inline-facts">
                                                            <i class="fa fa-trophy"></i>
                                                            <div class="milestone-counter">
                                                                <div class="stats animaper">
                                                                    <div style="font-size: 19px; padding: 18px; font-weight: 700;">Entertainment &amp; Leisure</div>
                                                                </div>
                                                            </div>
                                                            <p style="color: white!important;"><p>Internet</p>

                                                            <p>TV</p>

                                                            <p>Kid Friendly</p>
                                                        </div>
                                                    </div>
                                                    <!-- inline-facts end -->                            
                                                </div>
                                                <span class="fw-separator"></span>
                                                <div class="list-single-main-item-title fl-wrap">
                                                    <h3>Near By</h3>
                                                </div>
                                                <p><?php echo $prop->near_by; ?></p>
                                              
                                            </div>
                                            <div class="box-widget-item fl-wrap" id="sec5">
                                                <div class="box-widget-item-header">
                                                    <h3>Location: </h3>
                                                </div>
                                                <div class="box-widget">
                                                    <div class="map-container99">
                                                        <div id="mapCanvas"></div>
                                                    </div>
                                                    <div class="box-widget-content">
                                                        <div class="list-author-widget-contacts list-item-widget-contacts">
                                                            <ul>
                                                                <li><span><i class="fa fa-map-marker"></i>City :</span><a href="#"><?php echo $prop->city; ?></a></li>
                                                                <li><span><i class="fa fa-map-marker"></i>Country :</span><a href="#"><?php echo $prop->country; ?></a></li>
                                                                                                                            
                                                            </ul>
                                                        </div>                                              
                                                    </div>
                                                </div>
                                            </div>
                                            <!--                                    <div class="list-single-main-item-title fl-wrap" id="sec7">
                                                                                    <h3>Q & A</h3>
                                                                                </div>-->
                                            <div class="accordion">

                                            </div>
                                            <div class="list-single-main-item fl-wrap" id="sec6">
                                                <div class="list-single-main-item-title fl-wrap">
                                                    <h3>Gallery - Photos</h3>
                                                </div>
                                                <!-- gallery-items   -->
                                                <div class="gallery-items grid-small-pad  list-single-gallery three-coulms lightgallery">
                                                    <!-- 1 -->
                                                    <?php
                                                    $get_all_images = $this->UserModel->getAllPropertyImg($propID);
                                                    foreach ($get_all_images as $image) {
                                                        ?>
                                                        <div class="gallery-item">
                                                            <div class="grid-item-holder">
                                                                <div class="box-item">
                                                                    <img  src="https://res.cloudinary.com/dx5wgzxxo/image/upload/v1592120382/dreamstay/property/<?php echo $image->img; ?>.jpg"  alt=""   alt="" style="height: 150px;object-fit: cover;">
                                                                    <a href="https://res.cloudinary.com/dx5wgzxxo/image/upload/v1592120382/dreamstay/property/<?php echo $image->img; ?>.jpg"  alt="" class="gal-link popup-image"><i class="fa fa-search"  ></i></a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    <?php } ?> 

                                                    <!-- 1 end -->
                                                </div>
                                                <!-- end gallery items -->                                 
                                            </div>                                                              
                                        </div>
                                    </div>
                                </div>
                                <!--box-widget-wrap -->
                                <div class="col-md-4">
                                    <div class="box-widget-wrap"> 
                                        <!--box-widget-item -->
                                        <div class="box-widget-item fl-wrap">
                                            <div class="box-widget-item-header">
                                                                                            <!-- <h3> Starts From : <span style="color:#ff9800">
                                                        <span> </span><i class="fa fa-usd"></i>101/night</span>
                                                </h3> -->
                                            </div>
                                            <div class="box-widget opening-hours">
                                                <div class="box-widget-content">
                                                    <form  action="<?php echo $prop->airbnb_link; ?>" method="get" class="add-comment custom-form" target="_blank" formtarget="_blank">
                                                        <fieldset>
                                                            <!-- <div class="listsearch-input-text" id="autocomplete-container">
                                                            <label><i class="mbri-map-pin"></i> Select No. Guest </label>
                                                            <select data-placeholder="All Categories" class="chosen-select" name="guests" required >
                                                                <option value="1"> Select No. Guest </option> 
                                                               <option value="1"> 1 </option>
                                                               <option value="2"> 2 </option>
                                                               <option value="3"> 3 </option>
                                                               <option value="4"> 4 </option>
                                                           </select>
                                                        </div>
                                                              
                                                     <div class="listsearch-input-text" id="autocomplete-container">
                                                        <label><i class="mbri-map-pin"></i> Check IN </label>
                                                        <input placeholder="Check In" type="text" name="startDate" id="Check_in" onfocus="blur();" >
                                                        
                                                    </div>
                                                     <div class="listsearch-input-text" id="autocomplete-container">
                                                        <label><i class="mbri-map-pin"></i> Check OUT </label>
                                                        <input value="" placeholder="Check Out"  type="text" name="endDate" id="Check_out" onfocus="blur();" >
                                                         <a href="#" class="loc-act qodef-archive-current-location"> <i class="fa fa-dot-circle-o"></i></a>
                                                    </div>  -->


                                                        <!--<label><i class="fa fa-tag"></i></label>
<input type="text" placeholder="Promo Code *" name="Promo_code" value="" required=""/>-->

                                                        <!-- <label><i class="fa fa-user-o"></i></label>
                                                        <input type="text" placeholder="Your Name *" oninput="this.value = this.value.replace(/^\s+/, ''); this.value = this.value.replace(/[^a-zA-Z.]/g, ' ')" name="name" value="" required=""/>
                                                        <div class="clearfix"></div>
                                                        <label><i class="fa fa-envelope-o"></i>  </label>
                                                        <input type="text" placeholder="Email Address *" name="email" value="" required=""/>
                                                        <label><i class="fa fa-phone"></i>  </label>
                                                        <input type="text" placeholder="Phone *" onkeyup="if (/\D/g.test(this.value))
                                                                    this.value = this.value.replace(/\D/g, '')" name="phone" value="" required=""/> -->
                                                       <!--  <input type="text" name="spcheck2" style="display:none;" />   -->  

<!--  <textarea cols="20" rows="3" placeholder="Message" name="message" style="height:100px;"></textarea>-->
                                                        </fieldset>
                                                        <button type="submit" name="" style="width: 100%;height: 50px;" class="btn  big-btn  color-bg flat-btn">Check Availibility<i class="fa fa-angle-right"></i></button>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                        <!--box-widget-item end --> 
                                        <!--checkIN checkOUT-widget-item -->
                                        <div class="box-widget-item fl-wrap">
                                            <div class="box-widget-item-header">
                                               
                                            </div>
                                            <div class="box-widget opening-hours">
                                                <div class="box-widget-content">
                                                    <span class="current-status"><i class="fa fa-clock-o"></i>CheckIN/CheckOUT</span>
                                                    <ul>
                                                        <li><span class="opening-hours-day">Check IN </span><span class="opening-hours-time"><?php echo $prop->checkIN ?> PM </span></li>
                                                        <li><span class="opening-hours-day">Check OUT</span><span class="opening-hours-time"><?php echo $prop->checkOut ?> AM </span></li>                                                        
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <!--checkIN checkOUT end -->  										
                                    </div>
                                </div>
                                </section>
                                <!--  section end --> 
                                <div class="limit-box fl-wrap"></div>
                                <!--section Property listing -->
                                <section class="gray-section">
                                    <div class="container">
                                        <div class="section-title">
                                            <h2>Our Dream Place to Stay choices at Dream Stay vacation Rental New York</h2>
                                            <!-- <div class="section-subtitle">Best Listings</div> -->
                                            <span class="section-separator"></span>
                                            <p></p>

                                        </div>
                                    </div>
                                    <!-- carousel --> 
                                    <div class="list-carousel fl-wrap card-listing">                            
                                        <!--listing-carousel-->                            
                                        <div class="listing-carousel fl-wrap">                                


                                            <?php foreach ($all_property_data as $pro) { ?>
                                                <div class="slick-slide-item">                                            
                                                    <!-- listing-item -->                                            
                                                    <div class="listing-item">                                                
                                                        <article class="geodir-category-listing fl-wrap">                                                    
                                                            <div class="geodir-category-img">
                                                                <?php
                                                                $propID = $pro->id;
                                                                $get_images = $this->UserModel->getPropertyImg($propID);
                                                                foreach ($get_images as $img) {
                                                                    ?>
                                                                    <img src="https://res.cloudinary.com/dx5wgzxxo/image/upload/v1592120382/dreamstay/property/<?php echo $img->img; ?>.jpg"  alt="" alt="" style="height:204px;">                                                           
                                                                <?php } ?>                                                       
                                                            </div>                                                    
                                                            <div class="geodir-category-content fl-wrap">                                                        
                                                                <a class="listing-geodir-category" href="<?= base_url() ?>property/<?php echo htmlentities($pro->propertyURL); ?>">Visit </a>                                                        
                                                                <h3>
                                                                    <a href="<?= base_url() ?>property/<?php echo htmlentities($pro->propertyURL); ?>"><?php echo $pro->propertyName; ?></a>                                                                                                                
                                                                </h3>                                                        
                                                                <p>
                                                                    <?php echo strip_tags(trim(ucfirst(substr($pro->propertyInfo, 0, 85)))); ?>
                                                                </p>                                                                                                                                              
                                                            </div>                                                
                                                        </article>                                            
                                                    </div>                                            
                                                    <!-- listing-item end-->                                                                 
                                                </div>
                                            <?php } ?> 
                                            <!-- listing-item end-->                                                                 

                                        </div>                            
                                        <!--listing-carousel end-->                            
                                        <div class="swiper-button-prev sw-btn">
                                            <i class="fa fa-long-arrow-left"> </i>                               
                                        </div>                            
                                        <div class="swiper-button-next sw-btn">
                                            <i class="fa fa-long-arrow-right">  </i>                              
                                        </div>                        
                                    </div>						                        
                                    <!--  carousel end--> 
                                </section><hr/>			
                                <!-- Similar places section end -->
                            
                            <!--  content end  --> 
                       
                        <!--register form -->

                      
                        <!--section -->
                      
                        <!-- Content end --> 
                        <!--register form end -->
                        <?php include 'footer.php'; ?>

                        <!--footer end  -->
                        <!--schema start------------>                           
                        <!-- Main end -->
                        <!--=============== scripts  ===============-->
                        <script type="text/javascript" src="<?= base_url() ?>assets/js/jquery.min.js"></script>
                        <script type="text/javascript" src="<?= base_url() ?>assets/js/plugins.js"></script>
                        <script type="text/javascript" src="<?= base_url() ?>assets/js/scripts.js"></script>      
                        </body>
                        </html>