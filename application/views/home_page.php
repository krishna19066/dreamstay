
<!DOCTYPE HTML>
<html lang="en">
    <?php
    $table = 'cloudnary';
    $cloudnary = $this->UserModel->getAllData($table);
    foreach ($cloudnary as $res) {
        $cloud_cdnName = $res->cloud_name;
        $cloud_cdnKey = $res->api_key;
        $cloud_cdnSecret = $res->api_secret;
    }
    foreach ($home_page_data as $home) {
        
    }        
    ?>
    <head>
        <?php include 'header.php'; ?>

    </head>

    <style>

        /* this is media query used for the align the our hotel image*/
        @media only screen and (min-width: 1064px){
            #search_bar{
                width: 80%;
                left: 11%;
                right: 25%;
            }
            .card-post-content{
                height: 180px;
            }
        }
        .process-wrap p {
            text-align: center;
        }
        .main-search-input-item .nice-select{
            min-width: 200px;
        }
        .process-item{
            height: auto;
        }

    </style>
    <!-- offer -->
    <style type="text/css">
        .topBedge {
            position: fixed;
            top: 460px;
            right: 18px;
        }
        .topBedge i{
            color:#009688;
            font-size:50px;
        }

        .bedgShape{
            cursor:pointer;
            text-align:center;
            color:#fff;
            position:relative;
            width:100px;
            height:30px;
        }
        .topBedgeContnt{
            overflow:hidden;
        }
        .bedgShape:hover{
            border-radius:0px;
        }
        .topBedgeContnt{
            transition:0.5s;
            width:0px;
            right:100%;
            top:0;
            position:absolute;
            height:auto;
            overflow:hidden;
        }

        .topBedgeContnt p{
            border-bottom: 1px dotted #fff;
            padding: 5px;
        }

        .topBedgeContnt h2{
            border-bottom: 2px solid #fff;
        }
        .addEffect{
            display:block;
            width:300px;    
            background:#896633;
            font-size:16px;
        }

        @media only screen and (max-width: 404px)
        {
            .addEffect{
                width: 200px;
            }
            .topBedge{
                top: 101px;
            }
        }
        #headerPopup{
          width:75%;
          margin:0 auto;
        }
        
        #headerPopup iframe{
          width:100%;
          margin:0 auto;
        }

    </style>
    <script>
     $( document ).ready(function() {
         $('#headerVideoLink').magnificPopup({
          type:'inline',
          midClick: true // Allow opening popup on middle mouse click. Always set it to true if you don't provide alternative source in href.
        });
          
        });
    </script>
    <!-- offer end -->
    <body>

        <!--loader-->
        <div class="loader-wrap">
            <div class="pin"></div>
            <div class="pulse"></div>
        </div>
        <!--loader end-->
        <!-- Main  -->          
        <div id="main">
            <!-- header Menu-->
           
            <style>
                .skiptranslate { display:none; } 
            </style>

            <?php include 'menu.php'; ?>  
            <!-- wrapper -->    
            <div id="wrapper">
                <!-- Content-->   
                <div class="content">
                    <!--section -->
                    <section class="scroll-con-sec hero-section" data-scrollax-parent="true" id="sec1" style="margin-top:12px;padding:150px 0 221px;">
                        <div class="slideshow-container" data-scrollax="properties: { translateY: '200px' }" >
                            <?php
                            $table = 'slider';
                            $results = $this->UserModel->getAllData($table);
                            foreach ($results as $res) {
                                ?>
                                <div class="slideshow-item">
                                    <div class="bg" data-bg="https://res.cloudinary.com/<?php echo $cloud_cdnName; ?>/image/upload/v1591951954/dreamstay/slider/<?php echo $res->img ?>.jpg"></div>
                                </div>

                            <?php } ?>  

                        </div>
                        <div class="overlay" style="background: #00000040; opacity: 0.3;"></div>
                        <div class="hero-section-wrap fl-wrap">
                            <div class="container">
                                <div class="intro-item fl-wrap">
                                    <h2> Welcome to Dream Stay Vacation Rental</h2>
                                </div>
                                
                                <div class="main-search-input-wrap" style="width: auto;"  >
                                    <form  method="post" action="<?= base_url() ?>home/check_availability" >
                                        <div class="main-search-input fl-wrap"  id="search_bar" >
                                            <!-- CHECK OUT DATE -->
                                            <!-- END CHECK OUT DATE CODE -->
                                             
                                            <div class="main-search-input-item" id="autocomplete-container" style="width: 100%;">
                                               
                                                <select data-placeholder="All Categories" class="chosen-select" name="propertyLink" required="">
                                                    <option value="">Select Property</option>
                                                    <?php
                                                    $table = 'property';
                                                    $results = $this->UserModel->getAllData($table);
                                                    foreach ($results as $res) {
                                                        ?>
                                                        <option value="<?php echo $res->airbnb_link; ?>"><?php echo $res->propertyName; ?> </option>
                                                    <?php } ?>  
                                                </select>
                                            </div>

                                            
                                            <input type="text" name="spcheck2" style="display:none;" /> 
                                            <input type="submit" name="index_form" class="main-search-button" value="Check Availability">
                                            <!--<button class="main-search-button" onclick="window.location.href='listing.html'">Search</button>-->
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="bubble-bg"> </div>
                        <div class="header-sec-link">
                            <div class="container"><a href="#sec2" class="custom-scroll-link">Let's Start</a></div>
                        </div>
                    </section>
                    <!-- section end -->
                    <!--section -->
                    <section id="sec2">
                        <div class="container">
                             <!-- Vide section
           
                          <div id="headerPopup" class="mfp-hide embed-responsive embed-responsive-21by9">
                            <iframe class="embed-responsive-item" width="854" height="480" src="https://www.youtube.com/embed/o_O6L5IiGEU?autoplay=1" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                          </div>
                       
 <!-- Vide section end--->
                            <div class="section-title" style="text-align: justify;">
                                <h2><?php echo $home->h1_heading; ?></h2>
                                <!-- <div class="section-subtitle"></div> -->
                                <span class="section-separator"></span>
                                <p><?php echo $home->h1_content; ?></p>

                            </div>
                            <!-- portfolio start -->
                            <div class="gallery-items fl-wrap mr-bot spad">
                                <!-- gallery-item-->
                                <div class="gallery-item">
                                    <div class="grid-item-holder">
                                        <div class="listing-item-grid" style="height: 350px;">
                                            <img  src="https://res.cloudinary.com/dx5wgzxxo/image/upload/v1591955464/dreamstay/home/18_vb2i4j.jpg"   alt="" style="height: inherit;object-fit: cover;">
                                             <!-- <div class="listing-counter"><span>1 </span> Service</div>-->
                                            <div class="listing-item-cat" style="">
                                                <h3><a >Beautiful Interior  </a></h3>
                                                <p ><p>Alluring indoors with ample natural light to lift your spirits.</p>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- gallery-item end-->
                                <!-- gallery-item-->
                                <div class="gallery-item ">
                                    <div class="grid-item-holder">
                                        <div class="listing-item-grid" style="height: 350px;">
                                            <img  src="https://res.cloudinary.com/dx5wgzxxo/image/upload/v1592118811/dreamstay/property/2020-06-14_03-13-30.jpg"   alt="" style="height: inherit;object-fit: cover;">
                                           <!-- <div class="listing-counter"><span>2 </span> Service</div>-->
                                            <div class="listing-item-cat" style="">
                                                <h3><a >Quality Service</a></h3>
                                                <p>On-time Housekeeping, super clean apartments, and comfy beds!</p>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- gallery-item end-->    
                                <!-- gallery-item-->
                                <div class="gallery-item">
                                    <div class="grid-item-holder">
                                        <div class="listing-item-grid" style="height: 350px;">
                                            <img  src="https://res.cloudinary.com/dx5wgzxxo/image/upload/v1591955278/dreamstay/home/06_hkziqa.jpg"   alt="" style="height: inherit;object-fit: cover;">
                                           <!-- <div class="listing-counter"><span>1 </span> Service</div>-->
                                            <div class="listing-item-cat">
                                                <h3><a>Swimming Pool</a></h3>
                                                <p><p>State of the art equipment to always keep you fit and healthy.</p>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- gallery-item end-->                            
                            </div>
                            <!-- portfolio end -->                       
                        </div>
                    </section>
                    <!-- section end -->
                    <!--section Property listing -->
                    <!--section -->
                    <section class="gray-section">
                        <div class="container">
                            <div class="section-title">
                                <h2>Enjoy the beautifully designed house for your stay- NY</h2>
                                <!-- <div class="section-subtitle"> Rooms </div> -->
                                <span class="section-separator"></span>
                                <p> </p>
                            </div>
                        </div>
                        <div class="list-carousel fl-wrap card-listing">                            
                            <!--listing-carousel-->                            
                            <div class="listing-carousel fl-wrap">                                                                 
                                <?php foreach ($property_data as $pro) { ?>
                                    <div class="slick-slide-item">                                            
                                        <!-- listing-item -->                                            
                                        <div class="listing-item">                                                
                                            <article class="geodir-category-listing fl-wrap">                                                    
                                                <div class="geodir-category-img">
                                                    <?php
                                                    $propID = $pro->id;
                                                    $get_images = $this->UserModel->getPropertyImg($propID);
                                                    foreach ($get_images as $img) {
                                                        ?>
                                                        <img src="https://res.cloudinary.com/dx5wgzxxo/image/upload/v1592120382/dreamstay/property/<?php echo $img->img; ?>.jpg"  alt="" alt="" style="height:204px;">                                                           
                                                    <?php } ?>                                                       
                                                </div>                                                    
                                                <div class="geodir-category-content fl-wrap">                                                        
                                                    <a class="listing-geodir-category" href="<?= base_url() ?>property/<?php echo htmlentities($pro->propertyURL); ?>">Visit </a>                                                        
                                                    <h3>
                                                        <a href="<?= base_url() ?>property/<?php echo htmlentities($pro->propertyURL); ?>"><?php echo $pro->propertyName; ?></a>                                                                                                                
                                                    </h3>                                                        
                                                    <p>
                                                        <?php echo strip_tags(trim(ucfirst(substr($pro->propertyInfo, 0, 85)))); ?>
                                                    </p>                                                                                                                                              
                                                </div>                                                
                                            </article>                                            
                                        </div>                                            
                                        <!-- listing-item end-->                                                                 
                                    </div>
                                <?php } ?> 


                            </div>                           
                            <div class="swiper-button-prev sw-btn">
                                <i class="fa fa-long-arrow-left"> </i>                               
                            </div>                            
                            <div class="swiper-button-next sw-btn">
                                <i class="fa fa-long-arrow-right">  </i>                              
                            </div>
                        </div>
                </div>
                </section>
                <!-- section end -->
                <!-- section end -->
                <!--section -->
                <section class="color-bg">
                    <div class="shapes-bg-big"></div>
                    <div class="container">
                        <div class="row">
                            <div class="col-md-6">
                                <img  src="https://res.cloudinary.com/dx5wgzxxo/image/upload/v1592119018/dreamstay/property/2020-06-14_03-16-58.jpg"   alt="" style="height:300px;">

                            </div>
                            <div class="col-md-6">
                                <div class="color-bg-text">
                                    <h3>Give us a Chance to Host You</h3>
                                    <p>Indulge in the serenity of a place designed with peace and tranquility in mind serving as the perfect base for exploring NYC. Offering easy access to the city this hidden gem is suitable for both short and long-term stays.</p>
                                    <a href="#" class="color-bg-link modal-open">Book Now</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <!--section   end -->  
                <!--section -->  
                <section>
                    <div class="container">
                        <div class="section-title">
                            <h2>Highlights</h2>
                            <!-- <div class="section-subtitle">Features </div> -->
                            <span class="section-separator"></span>
                            <p></p>
                        </div>
                        <!--process-wrap  -->
                        <div class="process-wrap fl-wrap">
                            <ul>
                                <li>
                                    <div class="process-item">
                                        <span class="process-count">01 . </span>
                                        <div class="time-line-icon"><img style="max-width: 150px" src="https://res.cloudinary.com/dx8xpz8co/image/upload/v1590148177/THINGS-TO-DO/YOU_zb0ekl.png"> <!-- <i class="fa fa-map-o"></i> --></div>
                                        <h4> Priority - YOU</h4>
                                        <p><p>&nbsp;Your Wellness</p>

                                        <p>&nbsp;Your &nbsp;Relaxation&nbsp;</p>

                                        <p>&nbsp;Your Calmness</p>
                                        </p>
                                    </div>
                                    <span class="pr-dec"></span>
                                </li>
                                <li>
                                    <div class="process-item">
                                        <span class="process-count">02 .</span>
                                        <div class="time-line-icon"><img style="max-width: 150px" src="https://res.cloudinary.com/dx8xpz8co/image/upload/v1590148177/THINGS-TO-DO/award-2030694_zs55nz.png"><!-- <i class="fa fa-envelope-open-o"></i> --></div>
                                        <h4> Quality In Tourism</h4>
                                        <p><p>Safe</p>

                                        <p>Clean</p>

                                        <p>Legal</p>
                                        </p>
                                    </div>
                                    <span class="pr-dec"></span>
                                </li>
                                <li>
                                    <div class="process-item">
                                        <span class="process-count">03 .</span>
                                        <div class="time-line-icon"><img style="max-width: 150px" src="https://res.cloudinary.com/dx8xpz8co/image/upload/v1590148177/THINGS-TO-DO/PPL_k80afv.png"><!-- <i class="fa fa-hand-peace-o"></i> --></div>
                                        <h4>PPE - Supplied</h4>
                                        <p><p>Facemasks</p>

                                        <p>Hand sanitiser</p>

                                        <p>Gloves</p>
                                        </p>
                                    </div>
                                    <span class="pr-dec"></span>
                                </li>                                     
                            </ul>
                            <div class="process-end"><i class="fa fa-check"></i></div>
                        </div>
                        <!--process-wrap   end-->
                    </div>
                </section>
                <!-- section end -->
                <!--section Testimonials -->
                <section>
                    <div class="container">
                        <div class="section-title">
                            <h2>Testimonials</h2>
                            <!-- <div class="section-subtitle">Testimonials</div> -->
                            <span class="section-separator"></span>
                            <p></p>
                        </div>
                    </div>
                    <!-- testimonials-carousel --> 
                    <div class="carousel fl-wrap">
                        <!--testimonials-carousel-->
                        <div class="testimonials-carousel single-carousel fl-wrap">
                            <!--slick-slide-item-->
                            <?php foreach ($testimonial_data as $test) { ?>
                                <div class="slick-slide-item">
                                    <div class="testimonilas-text" style="height: 274px;">
                                        <div class="listing-rating card-popup-rainingvis" data-starrating2="5"> </div>
                                        <p><?php echo $test->content ?></p>

                                    </div>
                                    <div class="testimonilas-avatar-item">
                                        <div class="testimonilas-avatar"><img src="https://res.cloudinary.com/dx5wgzxxo/image/upload/v1592037092/dreamstay/testimonial/<?php echo $test->img; ?>.jpg" alt="Charley" style="object-fit: cover;"></div>
                                        <h4><?php echo $test->name ?> </h4>
                                        <span></span>
                                    </div>
                                </div>
                            <?php } ?>

                            <!--slick-slide-item end-->                                                               
                        </div>
                        <!--testimonials-carousel end-->
                        <div class="swiper-button-prev sw-btn"><i class="fa fa-long-arrow-left"></i></div>
                        <div class="swiper-button-next sw-btn"><i class="fa fa-long-arrow-right"></i></div>
                    </div>
                    <!-- carousel end-->


                    <script>

                        $(document).ready(function () {
                            // Hide the div
                            $(".topBedge").hide();
                            // Show the div in 5s
                            $(".topBedge").delay(2000).fadeIn(3000);
                        });

                        $("#fa").click(function (e) {
                            if ($('#fa').hasClass('fa-times')) {
                                $('#fa').removeClass('fa-times');
                                $('#fa').addClass('fa-gift');
                                $('.topBedge').click(function () {
                                    $('.topBedgeContnt').removeClass('addEffect');
                                });
                            } else {
                                $('#fa').removeClass('fa-gift');
                                $('#fa').addClass('fa-times');
                                $('.topBedge').click(function () {
                                    $('.topBedgeContnt').addClass('addEffect');

                                });
                            }
                            //   $('.fa').addClass('fa-gift');
                            //   $('.topBedgeContnt').removeClass('addEffect');                
                            // });
                        });
                    </script>
                    <!-- end the code html code for offer -->
                </section>
                <!-- section end -->
                <!--about-wrap -->
                <!-- about-wrap end  -->                   
            </div>
            <style type="text/css">
                #mask {
                    position:absolute;
                    left:0;
                    top:0;
                    z-index:9000;
                    background-color:#000;
                    display:none;
                }  
                #boxes .window {
                    position:fixed;
                    left:0;
                    top:0;
                    width:440px;
                    height:200px;
                    display:none;
                    z-index:9999;
                    padding:20px;
                    border-radius: 15px;
                    text-align: center;
                }
                #boxes #dialog {
                    width:100%; 
                    height:auto;
                    padding:10px;
                    background-color:#ffffff;
                    font-family: 'Segoe UI Light', sans-serif;
                    font-size: 15pt;
                }
                .maintext{
                    text-align: center;
                    font-family: "Segoe UI", sans-serif;
                    text-decoration: none;
                }
                body{
                    background: url('bg.jpg');
                }
                #lorem{
                    font-family: "Segoe UI", sans-serif;
                    font-size: 12pt;
                    text-align: left;
                }
                #popupfoot{
                    font-family: "Segoe UI", sans-serif;
                    font-size: 16pt;
                    padding: 10px 20px;
                }
                #popupfoot a{
                    text-decoration: none;
                }
                .agree:hover{
                    background-color: #D1D1D1;
                }
                .popupoption:hover{
                    background-color:#D1D1D1;
                    color: green;
                }
                .popupoption2:hover{

                    color: red;
                }
            </style>
            <!-- use this for popup-->
            <div id="boxes">
              <div style="top:auto; display: none;border: 1px solid #18aaa7;bottom: 3px;" id="dialog" class="window"> <!-- <span style="font-weight: 600">Ayoola SA</span> -->
                    <div id="lorem">
                        <p style="text-align: center;"> This site uses cookies to assist with understanding your use of it, assist with promotional and marketing efforts, and provide content from third parties.Privacy Policy</p>
                    </div>
                    <div id="popupfoot"> <a href="#"  class="close agree" style="color: #18aaa7">I agree</a> | <a class="agree"style="color:black;" href="">I do not agree</a> </div>
                </div>
                <!-- <div style="width: 1478px; font-size: 32pt; color:white; height: 602px; display: none; opacity: 0.8;" id="mask"></div> -->
            </div>
            <!-- use this for popup-->
            <!-- wrapper end -->
            <!--footer -->



            <!--register form -->
            <!-- <style>
                .footer-contacts li {
                    font-size:15px;
                }
                /*.tab-content section {
                    float: none;
            width: 100%;
            position: relative;
            padding: 0px;
            overflow: visible;
            background: #fff;
            z-index: auto;
                }*/
                .guesty-root-element.small-size.guesty-widget__container .guesty-widget__item #guesty-search-widget__datepicker .lightpick{
                    top:0px!important;
                }
            </style> -->
            <!--register form -->


            <!--register form end -->
            <!--section -->
            <section class="gradient-bg">
                <div class="cirle-bg">
                    <div class="bg" data-bg="images/bg/circle.png"></div>
                </div>
                <div class="container">
                    <div class="join-wrap fl-wrap">
                        <div class="row">
                            <div class="col-md-8">
                                <h3>Business Traveller?</h3>
                                <p>For best offers contact us</p>
                            </div>
                            <div class="col-md-4"><a href="<?= base_url() ?>contact-us" class="join-wrap-btn">Contact us<i class="fa fa-envelope-o"></i></a></div>
                        </div>
                    </div>
                </div>
            </section>

            <!-- Content end --> 
            <!--register form end -->
            <?php include 'footer.php'; ?>
            <!--register form -->          
            <!-- <a class="to-top"><i class="fa fa-angle-up"></i></a> -->
        </div>
        <!-- Main end -->
        <!--=============== scripts  ===============-->
        <!-- Google Autocomplete -->
        <script type="text/javascript" src="<?= base_url() ?>assets/js/jquery.min.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>assets/js/plugins.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>assets/js/scripts.js"></script>   
        <script type="text/javascript">
                        $(document).ready(function () {

                            var id = '#dialog';

                            //Get the screen height and width
                            var maskHeight = $(document).height();
                            var maskWidth = $(window).width();

                            //Set heigth and width to mask to fill up the whole screen
                            $('#mask').css({'width': maskWidth, 'height': maskHeight});

                            //transition effect     
                            $('#mask').fadeIn(500);
                            $('#mask').fadeTo("slow", 0.9);

                            //Get the window height and width
                            var winH = $(window).height();
                            var winW = $(window).width();

                            //Set the popup window to center
                            // $(id).css('top',  winH/2-$(id).height()/2);
                            // $(id).css('left', winW/2-$(id).width()/2);

                            //transition effect
                            $(id).fadeIn(2000);

                            //if close button is clicked
                            $('.window .close').click(function (e) {
                                //Cancel the link behavior
                                //   e.preventDefault();

                                $('#mask').hide();
                                $('.window').hide();
                                localStorage.setItem('cookieSeen', 'shown');
                            });

                            //if mask is clicked
                            $('#mask').click(function () {
                                $(this).hide();
                                $('.window').hide();
                            });

                        });

                        if (localStorage.getItem('cookieSeen') == 'shown') {

                            $('#boxes').hide();
                            $('#mask').hide();
                            $('.window').hide();
                        }
        </script>   


      <!-- <script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/latest/jquery.min.js"></script> -->
        <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
        <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
        <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />

        <script>
                        $("#Check_in").focus(function () {
                            $('input[name="startDate"]').daterangepicker({
                                singleDatePicker: true,
                                showDropdowns: true,
                                //maxYear: parseInt(moment().format('YYYY'),10)
                            });
                        });
                        $("#Check_out").focus(function () {
                            $('input[name="endDate"]').daterangepicker({
                                singleDatePicker: true,
                                showDropdowns: true,
                                //maxYear: parseInt(moment().format('YYYY'),10)
                            });
                        });

                        $("#Check_in1").focus(function () {
                            $('input[name="startDate"]').daterangepicker({
                                singleDatePicker: true,
                                showDropdowns: true,
                                //maxYear: parseInt(moment().format('YYYY'),10)
                            });
                        });
                        $("#Check_out1").focus(function () {
                            $('input[name="endDate"]').daterangepicker({
                                singleDatePicker: true,
                                showDropdowns: true,
                                //maxYear: parseInt(moment().format('YYYY'),10)
                            });
                        });
        </script>


    </body>
</html>