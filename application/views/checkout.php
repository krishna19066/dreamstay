<?php

if(strcasecmp($_SERVER['REQUEST_METHOD'], 'POST') == 0){
	//Request hash
	$contentType = isset($_SERVER["CONTENT_TYPE"]) ? trim($_SERVER["CONTENT_TYPE"]) : '';	
	if(strcasecmp($contentType, 'application/json') == 0){
		$data = json_decode(file_get_contents('php://input'));
		$hash=hash('sha512', $data->key.'|'.$data->txnid.'|'.$data->amount.'|'.$data->pinfo.'|'.$data->fname.'|'.$data->email.'|||||'.$data->udf5.'||||||'.$data->salt);
		$json=array();
		$json['success'] = $hash;
    	echo json_encode($json);
	
	}
	exit(0);
}
 
function getCallbackUrl()
{
	$protocol = ((!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off') || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://";
	return $protocol . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'] . 'response.php';
}

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>

<!-- this meta viewport is required for BOLT //-->
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" >
<!-- BOLT Sandbox/test //-->
<script id="bolt" src="https://sboxcheckout-static.citruspay.com/bolt/run/bolt.min.js" bolt-
color="e34524" bolt-logo="http://boltiswatching.com/wp-content/uploads/2015/09/Bolt-Logo-e14421724859591.png"></script>
<!-- BOLT Production/Live //-->
<!--// script id="bolt" src="https://checkout-static.citruspay.com/bolt/run/bolt.min.js" bolt-color="e34524" bolt-logo="http://boltiswatching.com/wp-content/uploads/2015/09/Bolt-Logo-e14421724859591.png"></script //-->

</head>
<?php include 'header.php'; ?>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script>
        /* Update item quantity */
        function updateCartItem(obj, rowid){
        	$.get("<?php echo base_url('cart/updateItemQty/'); ?>", {rowid:rowid, qty:obj.value}, function(resp){
        		if(resp == 'ok'){
        			location.reload();
        		}else{
        			alert('Cart update failed, please try again.');
        		}
        	});
        }
		
function show_select()
{
  var main_select = document.getElementById("main_select");
  //alert(main_select);
  var desired_box = main_select.options[main_select.selectedIndex].value;
 // alert(desired_box);
  if(desired_box == 'Online') {
     $(".Online").show();
	 $(".COD").hide();
  } else {
    $(".Online").hide();
	 $(".COD").show();
  }
}
        </script>
  <body>
  <div id="main" style="opacity: 1;">
 <?php include 'menu.php'; ?>
   <div id="wrapper">
    <div class="content">
                   
                    <!-- section end -->
                    <!--section -->  
                    <section id="sec1">
 <div class="container">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="list-single-main-item fl-wrap">
                                        <div class="list-single-main-item-title fl-wrap">
                                            <h3>Checkout <span> </span></h3>
                                        </div>
                                        <div class="list-single-main-media fl-wrap">
                                           <!-- <img src="https://i.ibb.co/KmxjWP8/contact-page-banner.png" class="respimg" alt="">-->
                                        </div>
                                    
                                     <h2>Order Preview</h2>
                                            <div class="row checkout">
                                                <!-- Order items -->
                                                <table class="table">
                                                <thead>
                                                    <tr>
                                                        <th width="13%"></th>
                                                        <th width="34%">Product</th>
                                                        <th width="18%">Price</th>
                                                        <th width="13%">Quantity</th>
                                                        <th width="22%">Subtotal</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php if($this->cart->total_items() > 0){ foreach($cartItems as $item){ ?>
                                                    <tr>
                                                        <td>
                                                           
                                                            <img src="https://res.cloudinary.com/ds89q0bap/image/upload/v1579079829/earthwater/product/<?php echo $item['image'] ?>.jpg" width="75"/>
                                                        </td>
                                                        <td><?php echo $item["name"]; ?></td>
                                                        <td><?php echo '₹'.$item["price"].'INR'; ?></td>
                                                        <td><?php echo $item["qty"]; ?></td>
                                                        <td><?php echo '₹'.$item["subtotal"].'INR'; ?></td>
                                                    </tr>
                                                    <?php } }else{ ?>
                                                    <tr>
                                                        <td colspan="5"><p>No items in your cart...</p></td>
                                                    </tr>
                                                    <?php } ?>
                                                </tbody>
                                                <tfoot>
                                                    <tr>
                                                        <td colspan="4"></td>
                                                        <?php if($this->cart->total_items() > 0){ ?>
                                                        <td class="text-center">
                                                            <strong>Total <?php echo '₹'.$this->cart->total().'INR'; ?></strong>
                                                        </td>
                                                        <?php } ?>
                                                    </tr>
                                                </tfoot>
                                                </table>
                                       
                                    </div>
                                </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="box-widget opening-hours">
                                                <div class="box-widget-content">
                                                   <!-- <form class="add-comment custom-form"  action='<?=base_url()?>checkout' method='POST'>-->
												  
													<select id="main_select" class="form-control" name="method" style="height:45px;width:320px;" onchange='show_select()'>
                                                                <option selected="">Choose...</option>
                                                                 
                                                                    <option value="COD">COD</option>
																	<option value="Online">Online</option>
                                                                   
                                                              </select>
												<div class="Online" style='display:none;'>	
											<form action="#" id="payment_form" class="add-comment custom-form">
												<input type="hidden" id="udf5" name="udf5" value="BOLT_KIT_PHP7" />
												<input type="hidden" id="surl" name="surl" value="<?php echo getCallbackUrl(); ?>" />																							
												<input type="hidden" id="key" name="key"  value="1iLZCHhT" />
												<input type="hidden" id="salt" name="salt" value="PeDeMGR8zr" />
												<input type="hidden" id="txnid" name="txnid" value="<?php echo  "Txn" . rand(10000,99999999)?>" />										
												 <?php if($this->cart->total_items() > 0){ foreach($cartItems as $item){ ?>
												<input type="hidden" id="amount" name="amount" value="<?php echo $this->cart->total(); ?>" />								
												<input type="hidden" id="pinfo" name="pinfo" value="<?php echo $item["name"]; ?>" />
												  <?php } } ?>	
												  
												<div class="dv">
												<span class="text"><label>Your Name:</label></span>
												<span><input type="text" id="fname" name="fname" placeholder="Your Name" value="" /></span>
												</div>												
												<div class="dv">
												<span class="text"><label>Email ID:</label></span>
												<span><input type="text" id="email" name="email" placeholder="Email ID" value="" /></span>
												</div>												
												<div class="dv">
												<span class="text"><label>Mobile/Cell Number:</label></span>
												<span><input type="text" id="mobile" name="mobile" placeholder="Mobile/Cell Number" value="" /></span>		
												</div>		
												<textarea cols="40" rows="3" name ='address' placeholder="Address:" stype='margin-top: 12px;'></textarea>
												
												<input type="hidden" id="hash" name="hash" value="" />	
														
												<div><input type="submit" class="btn  big-btn  color-bg flat-btn" value="Go to Payments" onclick="launchBOLT(); return false;" /></div>
												 <a href="<?php echo base_url('cart/'); ?>" class="btn btn-warning"><i class="glyphicon glyphicon-menu-left"></i> Back to Cart</a>
												</div>
												</form>
												</div>
												<div class="COD" style='display:none;'>
												<form class="add-comment custom-form"  action='<?=base_url()?>checkout' method='POST'>
												 <fieldset>
												<label><i class="fa fa-user-o"></i></label>
												<input type="text" name='name'id='name' placeholder="Your Name *" value="" required/>
												<input type="hidden" name='product_id' value="<?php echo $item["id"]; ?>" />
												 <input type="hidden" name='product_name' value="" />
												<div class="clearfix"></div>
												<label><i class="fa fa-envelope-o"></i></label>
												<input type="text" placeholder="Email Address*" id='email' name='email' value="<?php echo $item["email"]; ?>" required/>
												 <label><i class="fa fa-phone"></i>  </label>
												<input type="text" placeholder="Phone/Mobile*" id='mobile' name='mobile' value="<?php echo $item["mobile"]; ?>" required />
												 <input type="hidden" name="pincode" value="<?php echo $item["pincode"]; ?>" />	
												<textarea cols="40" rows="3" name ='address' placeholder="Address:" stype='margin-top: 12px;'></textarea>
												
											</fieldset>
									
												<button type="submit" name="placeOrder" class="btn  big-btn  color-bg flat-btn">Place Order <i class="glyphicon glyphicon-menu-right"></i></button>
                                                 <a href="<?php echo base_url('cart/'); ?>" class="btn btn-warning"><i class="glyphicon glyphicon-menu-left"></i> Back to Cart</a>
												</div>
												</form>
                                                </div>
                                            </div> 
                                                                       
                                </div>
	</div>
	
</div>
<script type="text/javascript"><!--
$('#payment_form').bind('keyup blur', function(){
	$.ajax({
          url: '<?=base_url()?>checkout',
          type: 'post',
          data: JSON.stringify({ 
            key: $('#key').val(),
			salt: $('#salt').val(),
			txnid: $('#txnid').val(),
			amount: $('#amount').val(),
		    pinfo: $('#pinfo').val(),
            fname: $('#fname').val(),
			email: $('#email').val(),
			mobile: $('#mobile').val(),
			udf5: $('#udf5').val()
          }),
		  contentType: "application/json",
          dataType: 'json',
          success: function(json) {
            if (json['error']) {
			 $('#alertinfo').html('<i class="fa fa-info-circle"></i>'+json['error']);
            }
			else if (json['success']) {	
				$('#hash').val(json['success']);
            }
          }
        }); 
});
//-->
</script>
<script type="text/javascript"><!--
function launchBOLT()
{
	bolt.launch({
	key: $('#key').val(),
	txnid: $('#txnid').val(), 
	hash: $('#hash').val(),
	amount: $('#amount').val(),
	firstname: $('#fname').val(),
	email: $('#email').val(),
	phone: $('#mobile').val(),
	productinfo: $('#pinfo').val(),
	udf5: $('#udf5').val(),
	surl : $('#surl').val(),
	furl: $('#surl').val(),
	mode: 'dropout'	
},{ responseHandler: function(BOLT){
	console.log( BOLT.response.txnStatus );		
	if(BOLT.response.txnStatus != 'CANCEL')
	{
		//Salt is passd here for demo purpose only. For practical use keep salt at server side only.
		var fr = '<form action=\"'+$('#surl').val()+'\" method=\"post\">' +
		'<input type=\"hidden\" name=\"key\" value=\"'+BOLT.response.key+'\" />' +
		'<input type=\"hidden\" name=\"salt\" value=\"'+$('#salt').val()+'\" />' +
		'<input type=\"hidden\" name=\"txnid\" value=\"'+BOLT.response.txnid+'\" />' +
		'<input type=\"hidden\" name=\"amount\" value=\"'+BOLT.response.amount+'\" />' +
		'<input type=\"hidden\" name=\"productinfo\" value=\"'+BOLT.response.productinfo+'\" />' +
		'<input type=\"hidden\" name=\"firstname\" value=\"'+BOLT.response.firstname+'\" />' +
		'<input type=\"hidden\" name=\"email\" value=\"'+BOLT.response.email+'\" />' +
		'<input type=\"hidden\" name=\"udf5\" value=\"'+BOLT.response.udf5+'\" />' +
		'<input type=\"hidden\" name=\"mihpayid\" value=\"'+BOLT.response.mihpayid+'\" />' +
		'<input type=\"hidden\" name=\"status\" value=\"'+BOLT.response.status+'\" />' +
		'<input type=\"hidden\" name=\"hash\" value=\"'+BOLT.response.hash+'\" />' +
		'</form>';
		var form = jQuery(fr);
		jQuery('body').append(form);								
		form.submit();
	}
},
	catchException: function(BOLT){
 		alert( BOLT.message );
	}
});
}
//--
</script>	
 </div>
            <!-- wrapper end -->
            <!--footer -->
               <?php include 'footer.php'; ?>
            <!--footer end  -->
            <!--register form -->
          	 <?php include 'inquiry-form.php'; ?>
            <!--register form end -->
        </div>
        <!-- Main end -->
        <!--=============== scripts  ===============-->
        <script type="text/javascript" src="js/jquery.min.js"></script>
        <script type="text/javascript" src="js/plugins.js"></script>
        <script type="text/javascript" src="js/scripts.js"></script>   
        <script type="text/javascript" src="js/map_infobox.js"></script>
        <script type="text/javascript" src="js/markerclusterer.js"></script>  
        <script type="text/javascript" src="js/maps.js"></script>
</body>
</html>
	
