 <div class="main-register-wrap modal">
                                    <div class="main-overlay"></div>
                                    <div class="main-register-holder">
                                        <div class="main-register fl-wrap">
                                            <div class="close-reg"><i class="fa fa-times"></i></div>
                                            <h3> <span>Book<strong> Now</strong></span></h3>                                         
                                            <div id="tabs-container">
                                                <div class="tab">
                                                    <div id="tab-1" class="tab-content">
                                                        <div class="custom-form">
                                                            <form action="" method="post" name="registerform">

                                                                <div class="listsearch-input-text" id="autocomplete-container">
                                                                    <label><i class="mbri-map-pin"></i> Select Property </label>

                                                                    <select data-placeholder="All Categories" class="chosen-select" name="guests" required="">
                                                                        <?php
                                                                        $table = 'property';
                                                                        $results = $this->UserModel->getAllData($table);
                                                                        foreach ($results as $res) {
                                                                            ?>
                                                                            <option value="<?php echo $res->propertyName; ?>"><?php echo $res->propertyName; ?> </option>
                                                                        <?php } ?>  
                                                                    </select>
                                                                </div>
                                                                <div class="col-md-12">
                                                                    <label>Name *</label>
                                                                    <input type="text"  oninput="this.value = this.value.replace(/^\s+/, ''); this.value = this.value.replace(/[^a-zA-Z.]/g, ' ')" name="guest_name" required="">
                                                                </div>
                                                                <div class="col-md-12">
                                                                    <label>Email Address *</label>
                                                                    <input type="email"  name="guest_mail" required="">
                                                                </div>

                                                                <!-- <div class="col-md-12">
                                                                    <label>Phone *</label>
                                                                    <input type="text"  onkeyup="if (/\D/g.test(this.value))this.value = this.value.replace(/\D/g, '')" name="phone" required="">
                                                                </div> -->



                                                                <!-- <div class="listsearch-input-text" id="autocomplete-container">
                                                                    <label><i class="mbri-map-pin"></i> Check IN </label>
                                                                    <input placeholder="Check In" type="text" name="startDate" onfocus="blur();"  id="Check_in1" >
                                                                    <a href="#" class="loc-act qodef-archive-current-location"><i class="fa fa-dot-circle-o"></i></a>
                                                                </div>
                                                                <div class="listsearch-input-text" id="autocomplete-container">
                                                                    <label><i class="mbri-map-pin"></i> Check OUT </label>
                                                                    <input value="" placeholder="Check Out"  type="text" name="endDate" onfocus="blur();" id="Check_out1">
                                                                    <a href="#" class="loc-act qodef-archive-current-location"><i class="fa fa-dot-circle-o"></i></a>
                                                                </div> -->   


                                                                <input type="text" name="spcheck2" style="display:none;" /> 
                                                                <div class="col-md-12">
                                                                    <button type="submit" name="instantbookfrm"  class="log-submit-btn"><span>Search</span>
                                                                    </button>
                                                                </div>

                                                                <div class="clearfix"></div>
                                                            </form>
                                                        </div>
                                                    </div> 
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>


<footer class="main-footer dark-footer">

    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <div class="footer-widget fl-wrap">
                    <h3>About Us</h3>
                    <div class="footer-contacts-widget fl-wrap">
                            <p style="color:#2d2e2f">Welcome to Dream Stay from home - we would love to go the extra mile so you enjoy this home as much as we do!.</p>
                       <!-- <ul  class="footer-contacts fl-wrap" style="padding-top:0px;margin-top: 0px;border-top: none;">
                            <li><span><i class="fa fa-envelope-o"></i></span><a href="mailto:airbnbofmalik@gmail.com" target="_blank">airbnbofmalik@gmail.com</a></li>
                            <li> <span style="float: left;margin-bottom: 15px;"><i class="fa fa-map-marker"></i></span><a href="#" target="_blank">215-40 27TH AVE, FLUSHING, NY 11360, USA</a></li>
                            <li><span><i class="fa fa-phone"></i></span><a href="tel:+1 (212) 300-5200">+1 (212) 300-5200</a></li>
                           <!-- <li><span><i class="fa fa-clock-o"></i></span><a href="#">24-Hour Front Desk</a></li>
                        </ul>-->
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="footer-widget fl-wrap">
                    <h3>Quick Links</h3>
                    <div class="widget-posts fl-wrap">
                        <ul>

                            <li class="clearfix">
                                <div class="widget-posts-descr">
                                    <a href="<?= base_url() ?>about-us" >About Us</a>                                      
                                </div>
                            </li>

                            <li class="clearfix">
                                <div class="widget-posts-descr">
                                    <a href="<?= base_url() ?>contact-us" >Contact Us</a>                                      
                                </div>
                            </li>

                            <li class="clearfix">
                                <div class="widget-posts-descr">
                                    <a href="#" >Privacy Policy</a>                                      
                                </div>
                            </li>

                            <li class="clearfix">
                                <div class="widget-posts-descr">
                                    <a href="#" >Terms & Conditions</a>                                      
                                </div>
                            </li>

                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="footer-widget fl-wrap">
                    <h3> Our Property </h3>
                    <div class="widget-posts fl-wrap">
                        <ul>
                            <?php
                            $table = 'property';
                            $results = $this->UserModel->getAllData($table);
                            foreach ($results as $res) {
                                ?>
                                <li class="clearfix">
                                    <div class="widget-posts-descr">
                                        <a href="<?=base_url()?>property/<?php echo htmlentities($res->propertyURL); ?>" ><?php echo $res->propertyName; ?></a>                                      
                                    </div>
                                </li>
                            <?php } ?>  

                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="footer-widget fl-wrap">
                    <h3>Subscribe</h3>
                    <div class="subscribe-widget fl-wrap">
                        <p>Want to be notified when we have new offer or an update. Just put and we'll send you a notification by email.</p>
                        <div class="subcribe-form">
                            <form  action="#" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank">
                                <input type="email" value="" name="EMAIL" class="email" id="mce-EMAIL" placeholder="email address" required style="width: 100%; padding: 12px; border-radius: 5px; ">
                                <button type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="subscribe-button" style="color: white;background: #009688;
                                        float: right;
                                        border: none;
                                        border-radius: 4px;
                                        width: 100%;
                                        height: 40px;
                                        line-height: 40px;margin-top: 10px;
                                        font-weight: 600;
                                        cursor: pointer;"><i class="fa fa-rss"></i> Subscribe</button>
                                <label for="subscribe-email" class="subscribe-message"></label>
                            </form>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <div class="sub-footer fl-wrap">
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <div class="about-widget">
                      <!--  <img src="https://res.cloudinary.com/dx8xpz8co/image/upload/c_fill/reputize/logo/2020-04-29_13-46-26.png"  alt="AYoola logo" alt="" style="width: auto;">-->
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="copyright" style="color: black;"> &#169; Dream Stay Vacation 2020 .  All rights reserved.Design By <a href="https://www.bkpinfo.in" target="_blank" style="color: black;">BKP-Infotech</a></div>
                </div>
                <div class="col-md-4">
                    <div class="footer-social">
                        <ul>
                            <li><a href="#" target="_blank" ><i class="fa fa-facebook-official"></i></a></li>
                            <li><a href="#" target="_blank"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#" target="_blank" ><i class="fa fa-linkedin"></i></a></li>
                            <li><a href="#" target="_blank" ><i class="fa fa-instagram"></i></a></li>
<!--                                                        <li><a href="" target="_blank" ><i class="fa fa-google-plus"></i></a></li>
0-->
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>

<!--footer end  -->