<!DOCTYPE HTML>
<html lang="en">
<?php include 'header.php'; ?>
<?php
$table = 'cloudnary';
$cloudnary = $this->UserModel->getAllData($table);
foreach($cloudnary as $res){
   $cloud_cdnName = $res->cloud_name;
   $cloud_cdnKey = $res->api_key;
   $cloud_cdnSecret = $res->api_secret;
}
?>
    <body>
        <!--loader-->
        <div class="loader-wrap">
            <div class="pin"></div>
            <div class="pulse"></div>
        </div>
        <!--loader end-->
        <!-- Main  -->
        <div id="main">
            <!-- header-->
               <?php include 'menu.php'; ?>
            <!--  header end -->
            <!-- wrapper -->
            <div id="wrapper">
                <div class="content">
                    <!--  section  -->
                    <section class="parallax-section" data-scrollax-parent="true">
                        <div class="bg par-elem "  data-bg="<?=base_url()?>assets/images/bg/24.jpg" data-scrollax="properties: { translateY: '30%' }"></div>
                        <div class="overlay"></div>
                        <div class="container">
                            <div class="section-title center-align">
                                <h2><span>Listings Without Map</span></h2>
                                <div class="breadcrumbs fl-wrap"><a href="#">Home</a><a href="#">Blog</a><span> Blog Listings</span></div>
                                <span class="section-separator"></span>
                            </div>
                        </div>
                        <div class="header-sec-link">
                            <div class="container"><a href="#sec1" class="custom-scroll-link">Let's Start</a></div>
                        </div>
                    </section>
                    <!--  section  end-->
<section>
                        <div class="container">
                            <div class="section-title">
                                <h2>Tips &amp; Articles</h2>
                                <div class="section-subtitle">From the blog.</div>
                                <span class="section-separator"></span>
                                <p>Browse the latest articles from our blog.</p>
                            </div>
                            <div class="row home-posts">
                                <?php foreach($results as $res){ ?>
                                <div class="col-md-4">
                                    <article class="card-post">
                                        <div class="card-post-img fl-wrap">
                                            <a href="<?=base_url()?>blog/<?php echo $res->url_slug ?>">
                                                <img src="https://res.cloudinary.com/<?php echo $cloud_cdnName; ?>/image/upload/v1579079829/earthwatery/blog/<?php echo $res->img ?>.jpg" alt="" style="height: 250px;">
                                                </a>
                                        </div>
                                        <div class="card-post-content fl-wrap">
                                            <h3><a href="<?=base_url()?>blog/<?php echo $res->url_slug ?>"><?php echo $res->title ?></a></h3>
                                            <p><?php echo $res->content ?> </p>
                                           
                                          <!--  <div class="post-opt">
                                                <ul>
                                                    <li><i class="fa fa-calendar-check-o"></i> <span>25 April 2018</span></li>
                                                    <li><i class="fa fa-eye"></i> <span>264</span></li>
                                                </ul>
                                            </div>-->
                                        </div>
                                    </article>
                                </div>
                                <?php } ?>
                               
                            </div>
                            
                        </div>
                    </section>
                        </div>
                <!-- content end-->
            </div>
            <!-- wrapper end -->
            <!--footer -->
            <?php include 'footer.php'; ?>
            <!--footer end  -->
            <!--register form -->
            <?php include 'inquiry-form.php'; ?>
            <!--register form end -->
            <a class="to-top"><i class="fa fa-angle-up"></i></a>
        </div>
        <!-- Main end -->
        <!--=============== scripts  ===============-->
        <script type="text/javascript" src="<?=base_url()?>assets/js/jquery.min.js"></script>
        <script type="text/javascript" src="<?=base_url()?>assets/js/plugins.js"></script>
        <script type="text/javascript" src="<?=base_url()?>assets/js/scripts.js"></script>			
    </body>
</html>