<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Checkout extends CI_Controller{
    
    function  __construct(){
        parent::__construct();
        
        // Load form library & helper
        $this->load->library('form_validation');
        $this->load->helper('form');
         $this->load->helper(array('form', 'url'));
        
        // Load cart library
        $this->load->library('cart');
        
        // Load product model
        $this->load->model('UserModel');
        
        $this->controller = 'checkout';
    }
    
    function index(){
        // Redirect if the cart is empty
        if($this->cart->total_items() <= 0){
            redirect('home/');
        }
        
        $custData = $data = array();
		
		
        // If order request is submitted
        $submit = $this->input->post('placeOrder');
        if(isset($submit)){
            // Form field validation rules
            $this->form_validation->set_rules('name', 'Name', 'required');
            $this->form_validation->set_rules('email', 'Email', 'required|valid_email');
            $this->form_validation->set_rules('mobile', 'Mobile', 'required');
            $this->form_validation->set_rules('address', 'Address', 'required');
            $this->form_validation->set_rules('address', 'Address', 'required');
			$this->form_validation->set_rules('address', 'pincode', 'required');
            
            // Prepare customer data
            $custData = array(
                'name'     => strip_tags($this->input->post('name')),
                'email'     => strip_tags($this->input->post('email')),
                'contactno'     => strip_tags($this->input->post('mobile')),
                'shippingAddress'=> strip_tags($this->input->post('address')),
                'product_id'=> strip_tags($this->input->post('product_id')),
				'pincode'=> strip_tags($this->input->post('product_id')),
            );
            
            // Validate submitted form data
            if($this->form_validation->run() == true){
                // Insert customer data
             $insert = $this->UserModel->insertCustomer($custData);
                
                // Check customer data insert status
                if($insert){
                    // Insert order
                    $order = $this->placeOrder($insert);
                    
                    // If the order submission is successful
                    if($order){
                        $this->session->set_userdata('success_msg', 'Order placed successfully.');
                        redirect($this->controller.'/orderSuccess/'.$order);
                    }else{
                        $data['error_msg'] = 'Order submission failed, please try again.';
                    }
                }else{
                    $data['error_msg'] = 'Some problems occured, please try again.';
                }
            }
        }
        
        // Customer data
        $data['custData'] = $custData;
        
        // Retrieve cart data from the session
        $data['cartItems'] = $this->cart->contents();
        
        // Pass products data to the view
        $this->load->view('checkout', $data);
    }
    
    function placeOrder($custID){
        // Insert order data
        $ordData = array(
            'userId' => $custID,
            'grand_total' => $this->cart->total()
        );
        $insertOrder = $this->UserModel->insertOrder($ordData);
        
        if($insertOrder){
            // Retrieve cart data from the session
            $cartItems = $this->cart->contents();
            
            // Cart items
            $ordItemData = array();
            $i=0;
            foreach($cartItems as $item){
                $ordItemData[$i]['order_id']     = $insertOrder;
                $ordItemData[$i]['product_id']     = $item['id'];
                $ordItemData[$i]['quantity']     = $item['qty'];
                $ordItemData[$i]['sub_total']     = $item["subtotal"];
                $i++;
            }
            
            if(!empty($ordItemData)){
                // Insert order items
                $insertOrderItems = $this->UserModel->insertOrderItems($ordItemData);
                
                if($insertOrderItems){
                    // Remove items from the cart
                    $this->cart->destroy();
                    
                    // Return order ID
                    return $insertOrder;
                }
            }
        }
        return false;
    }
    
    function orderSuccess($ordID){
        // Fetch order data from the database
        $data['order'] = $this->UserModel->getOrder($ordID);
        
        // Load order details view
        $this->load->view('order-success', $data);
    }
	
	function QuickOrder(){
		$product_id = $this->input->post('product_id');
		$pincode = $this->input->post('pincode');
		$email = $this->input->post('email');
		$mobile = $this->input->post('mobile');
        
		$product1 = $this->UserModel->getproductWithID($product_id);
        $product = $product1[0];
        // Add product to the cart
        $data = array(
            'id'    => $product->id,
            'qty'    => 1,
            'price'    => $product->productPrice,
            'name'    => $product->productName,
            'image' => $product->cloudnary_img,
			'email' => $email,
			'pincode'=>$pincode,
			'mobile'=>$mobile
        );
       
        $this->cart->insert($data);
		  
		  
        // Retrieve cart data from the session
        $data['cartItems'] = $this->cart->contents();
        print_r($data);
        // Pass products data to the view
         redirect('Checkout/');
	}
    
}