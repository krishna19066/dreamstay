<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Blog extends CI_Controller {
	function __construct() {
        parent::__construct();
        $this->load->model('UserModel');
        // sets up the session
        $this->load->library('form_validation');            // Loading form validation library
        $this->load->helper(array('form', 'url'));
        $this->load->library('email');
          $this->load->library('cart');
        //header('Access-Control-Allow-Origin : http://localhost:3000');   
    }
	public function index()
	{
	    $table = 'blog';
		$data['results'] = $this->UserModel->getAllData($table);
		$this->load->view('blog',$data);		
	}
	 function Product($id){
	      $product_id =  $id;
	      $data['product'] = $this->UserModel->getproductWithID($product_id);
	      //print_r($product);
	      $this->load->view('product_details',$data); 
	 }
	 
public function details($url_slug){
	     $url_slug;
        //get the post data
        $data['blog_details'] = $this->UserModel->getblogWithURL($url_slug);
        //print_r($blog_details);
        //load the view
        $this->load->view('blog_details', $data);
    }
    
   
	
	
	  


}
