<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Product extends CI_Controller {
	function __construct() {
        parent::__construct();
        $this->load->model('UserModel');
        // sets up the session
        $this->load->library('form_validation');            // Loading form validation library
        $this->load->helper(array('form', 'url'));
        $this->load->library('email');
        $this->load->library('cart');
        //header('Access-Control-Allow-Origin : http://localhost:3000');   
    }
	public function index()
	{
	    $table = 'blog';
		$data['results'] = $this->UserModel->getAllData($table);
		$this->load->view('blog',$data);		
	}
	 function Product($id){
	      $product_id =  $id;
	      $data['product'] = $this->UserModel->getproductWithID($product_id);
	      //print_r($product);
	      $this->load->view('product_details',$data); 
	 }
	 
	public function details($url_slug){
	    $url_slug;
       $data['product'] = $this->UserModel->getproductWithURL($url_slug);
	      //print_r($product);
	      $this->load->view('product_details',$data); 
    }
     function addToCart($proID){
        // Fetch specific product by ID
        $product1 = $this->UserModel->getproductWithID($proID);
        $product = $product1[0];
        // Add product to the cart
        $data = array(
            'id'    => $product->id,
            'qty'    => 1,
            'price'    => $product->productPrice,
            'name'    => $product->productName,
            'image' => $product->cloudnary_img
        );
       
        $this->cart->insert($data);
        // Redirect to the cart page
        redirect('cart/');
    }
    function addToCartANDCheckout($proID){
        // Fetch specific product by ID
        $product1 = $this->UserModel->getproductWithID($proID);
        $product = $product1[0];
        // Add product to the cart
        $data = array(
            'id'    => $product->id,
            'qty'    => 1,
            'price'    => $product->productPrice,
            'name'    => $product->productName,
            'image' => $product->cloudnary_img,
			'email' => '',
			'pincode'=>'',
			'mobile'=>''
        );
       
        $this->cart->insert($data);
        // Redirect to the cart page
        redirect('Checkout/');
    }
	
	
    
   
	
	
	  


}
