<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('UserModel');
        // sets up the session
        $this->load->library('form_validation');            // Loading form validation library
        $this->load->helper(array('form', 'url'));
        $this->load->library('email');
        $this->load->library('cart');
        //header('Access-Control-Allow-Origin : http://localhost:3000');   
    }

    public function index() {
        $table = "home_page";
        $table2 = "property";
        $table3 = "testimonial";
        $data['home_page_data'] = $this->UserModel->getAllData($table);
        $data['property_data'] = $this->UserModel->getAllData($table2);
        $data['testimonial_data'] = $this->UserModel->getAllData($table3);
        //print_r($data);
        //die;
        $this->load->view('home_page', $data);
    }

    function PropertyListing() {
        $table2 = "property";
        $data['property_data'] = $this->UserModel->getAllData($table2);
        $this->load->view('listing_page',$data);
    }

    function AboutUs() {
        $this->load->view('aboutus_page');
    }

    function ContactUs() {
        $this->load->view('contactus_page');
    }

    function SubmitInquiry() {
        $name = $this->input->post('name');
        $email = $this->input->post('email');
        $mobile = $this->input->post('phone');
        $message = $this->input->post('message');
       $this->SendMail($name,$email,$mobile,$message);
        $data = array(
            'name' => $name,
            'email' => $email,
            'mobile' => $mobile,
            'message' => $message,
        );
       // print_r($data);
        $this->db->insert('inquiry', $data);
        echo '<script type="text/javascript">
               alert("Thank You! Your Request has been successfuly submited");              
            </script>';
          redirect('home');
    }
     function check_availability() {
       $propertyLink =  $this->input->post('propertyLink');
      echo '<script type="text/javascript" language="Javascript">window.location.href="'.$propertyLink.'";</script>';
       //redirect($propertyLink);
         
     }

    function Search() {
        $city = $this->input->post('city');
        $category = $this->input->post('category_id');
        $data['results'] = $this->UserModel->getAllProducts($category);
        $this->load->view('header');
        $this->load->view('listing_page', $data);
    }

    function order_process() {
        $name = $this->input->post('name');
        $email = $this->input->post('email');
        $mobile = $this->input->post('mobile');
        $address = $this->input->post('address');
        $method = $this->input->post('method');
        $product_id = $this->input->post('product_id');
        $product_name = $this->input->post('product_name');
        $to_email = 'balkrishn@theperch.in';
        $subject = 'New Inquiry from website';
        $mess = "Regarding :- Type :- Inquiry <br/><br/> Name :- " . $name . "<br/><br/> Email : " . $email . "<br/><br/>Mobile : " . $mobile . "<br/><br/>City : " . $city . "<br/><br/>Product Name : " . $product_name . "";
        $headers = 'From:info@earthwaterpurifier.com';
        $headers = "From: " . strip_tags($email) . "\r\n";
        $headers .= "Reply-To: " . strip_tags($to_email) . "\r\n";
        //$headers .= "CC: susan@example.com\r\n";
        $headers .= "MIME-Version: 1.0\r\n";
        $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
        mail($to_email, $subject, $mess, $headers);
        $data = array(
            'name' => $name,
            'email' => $email,
            'contactno' => $mobile,
            'product_id' => $product_id,
            'payment_method' => $method,
            'shippingAddress' => $address,
            'billingAddress' => $address
        );
        $this->db->insert('users', $data);
        redirect('Home');
    }

    function SubmitOderHistory() {
        $order_id = $this->input->post('order_id');
        $contact_number = $this->input->post('contact_number');
        $contact_name = $this->input->post('contact_name');
        $source = $this->input->post('source');
        $status = $this->input->post('status');
        $order_date = $this->input->post('order_date');
        $payment_mode = $this->input->post('payment_mode');
        $comments = $this->input->post('comments');
        $city = $this->input->post('city');
        $state = $this->input->post('state');
        $pincode = $this->input->post('pincode');
        $data = array(
            'order_id' => $order_id,
            'contact_number' => $contact_number,
            'contact_name' => $contact_name,
            'source' => $source,
            'status' => $status,
            'payment_mode' => $payment_mode,
            'comments' => $comments,
            'city' => $city,
            'state' => $state,
            'pincode' => $pincode,
        );
        $this->db->insert('order_history', $data);
        redirect('Home/order_history');
    }

    function Submit_Technician_form() {
        $order_id = $this->input->post('order_id');
        $contact_number = $this->input->post('contact_number');
        $contact_name = $this->input->post('contact_name');
        $source = $this->input->post('source');
        $status = $this->input->post('status');
        $order_date = $this->input->post('order_date');
        $payment_mode = $this->input->post('payment_mode');
        $comments = $this->input->post('comments');
        $city = $this->input->post('city');
        $state = $this->input->post('state');
        $pincode = $this->input->post('pincode');
        $data = array(
            'order_id' => $order_id,
            'contact_number' => $contact_number,
            'contact_name' => $contact_name,
            'source' => $source,
            'status' => $status,
            'payment_mode' => $payment_mode,
            'comments' => $comments,
            'city' => $city,
            'state' => $state,
            'pincode' => $pincode,
        );
        $this->db->insert('order_history', $data);
        redirect('Home/order_history');
    }
    
    function SendMail($name,$email,$mobile,$message){
    $guest_name = $name;
    $property_name = 'Dream stay';
    $page = 'Contact US';
    $guest_mail = $email;
    $phone = $mobile;
    $hotel_name = 'Dream Stay';
    $to_email = 'airbnbofmalik@gmail.com';
    $from_email = 'support@bkpinfo.in';
    $subject = "You Have Recieved New Inquire From" . ' ' . $guest_name;
    $mess = "Regarding :- " . $property_name . " - From " . $page . " <br /><br /> Name :- " . $guest_name . "<br /><br /> Email : " . $guest_mail . "<br /><br /> Mobile : " . $phone . "<br /><br />Message : " . $message . "<br /><br />";
    $to = $to_email;
    $cc = '';
    $from = $from_email;
    $url = 'https://api.sendgrid.com/';
    $user = 'balkrishn2sharma@gmail.com';
    $pass = '0108it101010';
    $json_string = array('to' => array($to), 'category' => 'inquiry');
    $params = array(
        'api_user' => 'balkrishn2sharma@gmail.com',
        'api_key' => '0108it101010',
        'x-smtpapi' => json_encode($json_string),
        'to' => $to,
        'subject' => $subject,
        'html' => $mess,
        'replyto' => $to,
        'cc' => $cc,
        'fromname' => $hotel_name,
        'text' => 'testing body',
        'from' => $from,
    );
    $request = $url . 'api/mail.send.json';
    $session = curl_init($request);
    curl_setopt($session, CURLOPT_POST, true);
    curl_setopt($session, CURLOPT_POSTFIELDS, $params);
    curl_setopt($session, CURLOPT_HEADER, false);
    curl_setopt($session, CURLOPT_SSLVERSION, CURL_SSLVERSION_TLSv1_2);
    curl_setopt($session, CURLOPT_RETURNTRANSFER, true);
    $response = curl_exec($session);
    curl_close($session);
   // print_r($response);
    }

}
