<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Category extends CI_Controller {
	function __construct() {
        parent::__construct();
        $this->load->model('UserModel');
        // sets up the session
        $this->load->library('form_validation');            // Loading form validation library
        $this->load->helper(array('form', 'url'));
        $this->load->library('email');
          $this->load->library('cart');
        //header('Access-Control-Allow-Origin : http://localhost:3000');   
    }
	public function index($slug_url)
	{
	    $table = 'category';
	    $data = $this->UserModel->getCategoryWithGroupBY($table,$slug_url);
	    //print_r($data);
	  //  die;
	  foreach($data as $dt){
	      $db_url = $dt->url;
	      $category = $dt->id;
	       if($slug_url == $db_url){
	           	$category;
	       }
	       
	  }
	    /* if($slug_url == "water-purifier"){
	         	$category = 3;
	     }elseif($slug_url == "industrial-plants"){
	         	$category = 4;
	     }
	     elseif($slug_url == "service-kit"){
	         	$category = 7;
	     }
	     elseif($slug_url == "electric-chimneys"){
	         	$category = 7;
	     }
	     elseif($slug_url == "geyser"){ 
	         	$category = 5;
	     } elseif($slug_url == "immersion-rod"){  
	         	$category = 6;
	     }
	      elseif($slug_url == "water-dispenser"){  
	         	$category = 7;
	     }
	     */
	     	$data['sub_cate'] = $slug_url;
	     	$data['results'] = $this->UserModel->getAllProducts($category);	
	     	$this->load->view('listing_page',$data);
	   		
	}
	 function Product($id){
	      $product_id =  $id;
	      $data['product'] = $this->UserModel->getproductWithID($product_id);
	      //print_r($product);
	      $this->load->view('product_details',$data); 
	 }
	 	function waterpurifier($sub_cate = ''){
		$category = 3;
		$data['results'] = $this->UserModel->getAllProducts($category);		
    	$data['sub_cate'] = $sub_cate;
		$this->load->view('listing_page',$data);
	}
	
	function industrialplants($sub_cate = ''){
	    echo $sub_cate;
		$category = 4;
		$data['results'] = $this->UserModel->getAllProducts($category);		
		$data['sub_cate'] = $sub_cate;
		$this->load->view('listing_page',$data);
	}
	function geyser($sub_cate = ''){
		$category = 5;
		$data['results'] = $this->UserModel->getAllProducts($category);	
		$data['sub_cate'] = $sub_cate;
	//	$this->load->view('header');
		$this->load->view('listing_page',$data);
	}
	
		function immersionrod($sub_cate = ''){
		$category = 6;
		$data['results'] = $this->UserModel->getAllProducts($category);	
		$data['sub_cate'] = $sub_cate;
	//	$this->load->view('header');
		$this->load->view('listing_page',$data);
	}
		function waterdispenser($sub_cate = ''){
		$category = 7;
		$data['results'] = $this->UserModel->getAllProducts($category);	
		$data['sub_cate'] = $sub_cate;
	//	$this->load->view('header');
		$this->load->view('listing_page',$data);
	}
	function servicekit($sub_cate = ''){
	    $category = 7;
		$data['results'] = $this->UserModel->getAllProducts($category);	
		$data['sub_cate'] = $sub_cate;
	//	$this->load->view('header');
		$this->load->view('listing_page',$data);
	}
	 function electricchimneys($sub_cate = ''){
	    $category = 7;
		$data['results'] = $this->UserModel->getAllProducts($category);	
		$data['sub_cate'] = $sub_cate;
	//	$this->load->view('header');
		$this->load->view('listing_page',$data);
	 }
	 
	 function ContactUs(){
	    	$this->load->view('contactus_page'); 
	 }
	 function AboutUs(){
	    	$this->load->view('aboutus_page'); 
	 }
	  function orderhistory(){
	    	$this->load->view('order_history');  
	 }
	  function Termsconditions(){
	 	$this->load->view('terms_conditions'); 
	 }
	 function TechnicianForm(){
	    	$this->load->view('order_history');  
	 }
    
   
	
	
	  


}
