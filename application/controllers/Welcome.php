<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {
	function __construct() {
        parent::__construct();
        $this->load->model('UserModel');
        // sets up the session
        $this->load->library('form_validation');            // Loading form validation library
        $this->load->helper(array('form', 'url'));
        //header('Access-Control-Allow-Origin : http://localhost:3000');   
    }
	public function index()
	{
		$category = 4;
		$data['results'] = $this->UserModel->getProducts($category);
		//print_r($data);
		//die;
		$this->load->view('header');
		$this->load->view('home_page',$data);
		
	}
}
