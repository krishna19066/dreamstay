-- phpMyAdmin SQL Dump
-- version 4.9.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jan 13, 2021 at 07:28 AM
-- Server version: 10.3.27-MariaDB-log-cll-lve
-- PHP Version: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bkpihfdy_bkpinfo`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` int(11) NOT NULL,
  `customerID` varchar(200) NOT NULL DEFAULT '',
  `username` varchar(200) NOT NULL DEFAULT '',
  `password` varchar(250) NOT NULL DEFAULT '',
  `center_email` varchar(100) NOT NULL DEFAULT '',
  `reservationNo` varchar(40) NOT NULL DEFAULT '',
  `website` varchar(40) NOT NULL,
  `linkedin_link` varchar(100) NOT NULL DEFAULT '',
  `fb_link` varchar(100) NOT NULL DEFAULT '',
  `tw_link` varchar(100) NOT NULL DEFAULT '',
  `insta_link` varchar(100) NOT NULL DEFAULT '',
  `youtube` varchar(100) NOT NULL DEFAULT '',
  `timestamp` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `customerID`, `username`, `password`, `center_email`, `reservationNo`, `website`, `linkedin_link`, `fb_link`, `tw_link`, `insta_link`, `youtube`, `timestamp`) VALUES
(1, '1', 'bk', '123456', '', '', '', '', '', '', '', '', '2020-06-11 07:24:48');

-- --------------------------------------------------------

--
-- Table structure for table `cloudnary`
--

CREATE TABLE `cloudnary` (
  `id` int(11) NOT NULL,
  `email` varchar(200) NOT NULL DEFAULT '',
  `password` varchar(200) NOT NULL DEFAULT '',
  `cloud_name` varchar(250) NOT NULL DEFAULT '',
  `api_key` varchar(250) NOT NULL,
  `api_secret` varchar(250) NOT NULL,
  `customerID` varchar(200) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cloudnary`
--

INSERT INTO `cloudnary` (`id`, `email`, `password`, `cloud_name`, `api_key`, `api_secret`, `customerID`, `timestamp`) VALUES
(1, 'Airbnbofmalik@gmail.com', 'BKPInfo@123  ', 'dx5wgzxxo', '464169323428863', 'B9HimdAHJtRW-PzwD2rwhzwOazE', '1', '2020-06-12 08:30:32');

-- --------------------------------------------------------

--
-- Table structure for table `home_page`
--

CREATE TABLE `home_page` (
  `id` int(11) NOT NULL,
  `customerID` varchar(40) NOT NULL DEFAULT '',
  `h1_heading` text DEFAULT NULL,
  `h1_content` text DEFAULT NULL,
  `feature_heading` text DEFAULT NULL,
  `feature_content` text DEFAULT NULL,
  `property_heading` text DEFAULT NULL,
  `property_content` text DEFAULT NULL,
  `timestamp` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `home_page`
--

INSERT INTO `home_page` (`id`, `customerID`, `h1_heading`, `h1_content`, `feature_heading`, `feature_content`, `property_heading`, `property_content`, `timestamp`) VALUES
(1, '', 'Serviced Accommodation New York City.\r\n', 'Welcome to this beautiful urban house in a peaceful neighborhood boasting elegant design and homey ambiance. Decorated in subtle good taste this amazing getaway is taken care of by a super host who would love to go the extra mile to cater to your needs. You are in for an outstanding experience and lots of fun at this gorgeous NYC oasis.\r\nWell-kept landscaped garden with fruit trees and plenty of space for outdoor family fun is what greets you first.', NULL, NULL, NULL, NULL, '2020-06-12 09:31:49');

-- --------------------------------------------------------

--
-- Table structure for table `images`
--

CREATE TABLE `images` (
  `id` int(11) NOT NULL,
  `img` varchar(200) NOT NULL DEFAULT '',
  `customerID` varchar(80) NOT NULL DEFAULT '',
  `propertyID` varchar(90) NOT NULL DEFAULT '',
  `roomID` varchar(90) NOT NULL DEFAULT '',
  `type` varchar(90) NOT NULL DEFAULT '',
  `propertyF` varchar(80) NOT NULL DEFAULT '',
  `roomF` varchar(80) NOT NULL DEFAULT '',
  `timestamp` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `images`
--

INSERT INTO `images` (`id`, `img`, `customerID`, `propertyID`, `roomID`, `type`, `propertyF`, `roomF`, `timestamp`) VALUES
(3, '2020-06-13_14-53-29', '1', '4', '', 'property', 'N', '', '2020-06-13 18:53:30'),
(4, '2020-06-14_01-07-42', '1', '4', '', 'property', 'N', '', '2020-06-14 05:07:43'),
(5, '2020-06-14_01-08-41', '1', '4', '', 'property', 'N', '', '2020-06-14 05:08:43'),
(7, '2020-06-14_01-24-28', '1', '4', '', 'property', 'N', '', '2020-06-14 05:24:29'),
(9, '2020-06-14_01-25-30', '1', '4', '', 'property', 'N', '', '2020-06-14 05:25:32'),
(10, '2020-06-14_01-26-18', '1', '4', '', 'property', 'N', '', '2020-06-14 05:26:19'),
(12, '2020-06-14_01-32-24', '1', '4', '', 'property', 'N', '', '2020-06-14 05:32:25'),
(13, '2020-06-14_01-33-06', '1', '4', '', 'property', 'N', '', '2020-06-14 05:33:07'),
(14, '2020-06-14_01-33-32', '1', '4', '', 'property', 'Y', '', '2020-06-14 09:06:51'),
(15, '2020-06-14_01-33-58', '1', '4', '', 'property', 'N', '', '2020-06-14 05:33:59'),
(16, '2020-06-14_01-34-22', '1', '4', '', 'property', 'N', '', '2020-06-14 05:34:23'),
(17, '2020-06-14_01-34-53', '1', '4', '', 'property', 'N', '', '2020-06-14 05:34:54'),
(18, '2020-06-14_03-12-59', '1', '2', '', 'property', 'N', '', '2020-06-14 07:13:00'),
(19, '2020-06-14_03-13-30', '1', '2', '', 'property', 'N', '', '2020-06-14 07:13:31'),
(20, '2020-06-14_03-14-14', '1', '2', '', 'property', 'N', '', '2020-06-14 07:14:14'),
(21, '2020-06-14_03-15-09', '1', '2', '', 'property', 'N', '', '2020-06-14 07:15:10'),
(22, '2020-06-14_03-15-24', '1', '2', '', 'property', 'N', '', '2020-06-14 07:15:25'),
(23, '2020-06-14_03-15-41', '1', '2', '', 'property', 'Y', '', '2020-06-14 09:07:09'),
(24, '2020-06-14_03-15-59', '1', '2', '', 'property', 'N', '', '2020-06-14 07:16:00'),
(25, '2020-06-14_03-16-21', '1', '2', '', 'property', 'N', '', '2020-06-14 09:08:20'),
(26, '2020-06-14_03-16-42', '1', '2', '', 'property', 'N', '', '2020-06-14 07:16:43'),
(27, '2020-06-14_03-16-58', '1', '2', '', 'property', 'N', '', '2020-06-14 07:16:59'),
(28, '2020-06-14_03-17-14', '1', '2', '', 'property', 'N', '', '2020-06-14 07:17:15'),
(29, '2020-06-14_03-21-21', '1', '3', '', 'property', 'N', '', '2020-06-14 07:21:22'),
(30, '2020-06-14_03-21-39', '1', '3', '', 'property', 'N', '', '2020-06-14 07:21:40'),
(31, '2020-06-14_03-22-02', '1', '3', '', 'property', 'N', '', '2020-06-14 07:22:02'),
(32, '2020-06-14_03-22-15', '1', '3', '', 'property', 'N', '', '2020-06-14 07:22:16'),
(33, '2020-06-14_03-22-30', '1', '3', '', 'property', 'Y', '', '2020-06-14 09:05:57'),
(34, '2020-06-14_03-22-47', '1', '3', '', 'property', 'N', '', '2020-06-14 07:22:48'),
(35, '2020-06-14_03-23-04', '1', '3', '', 'property', 'N', '', '2020-06-14 07:23:04'),
(36, '2020-06-14_03-23-19', '1', '3', '', 'property', 'N', '', '2020-06-14 07:23:20'),
(37, '2020-06-14_03-23-34', '1', '3', '', 'property', 'N', '', '2020-06-14 07:23:34'),
(38, '2020-06-14_03-35-06', '1', '1', '', 'property', 'N', '', '2020-06-14 09:07:57'),
(39, '2020-06-14_03-35-29', '1', '1', '', 'property', 'N', '', '2020-06-14 09:08:04'),
(40, '2020-06-14_03-35-51', '1', '1', '', 'property', 'Y', '', '2020-06-14 07:35:52'),
(41, '2020-06-14_03-36-09', '1', '1', '', 'property', 'N', '', '2020-06-14 07:36:09'),
(42, '2020-06-14_03-36-48', '1', '1', '', 'property', 'N', '', '2020-06-14 07:36:49'),
(43, '2020-06-14_03-37-15', '1', '1', '', 'property', 'N', '', '2020-06-14 07:37:16'),
(44, '2020-06-14_03-37-35', '1', '1', '', 'property', 'N', '', '2020-06-14 07:37:36'),
(45, '2020-06-14_03-37-55', '1', '1', '', 'property', 'N', '', '2020-06-14 07:37:56'),
(46, '2020-06-14_03-38-40', '1', '1', '', 'property', 'N', '', '2020-06-14 07:38:41'),
(47, '2020-06-14_03-38-58', '1', '1', '', 'property', 'N', '', '2020-06-14 07:38:59'),
(48, '2020-06-14_03-39-20', '1', '1', '', 'property', 'N', '', '2020-06-14 07:39:22'),
(49, '2020-06-14_03-39-41', '1', '1', '', 'property', 'N', '', '2020-06-14 07:39:42');

-- --------------------------------------------------------

--
-- Table structure for table `inquiry`
--

CREATE TABLE `inquiry` (
  `id` int(11) NOT NULL,
  `email` varchar(20) NOT NULL DEFAULT '',
  `mobile` varchar(20) NOT NULL DEFAULT '',
  `name` varchar(40) NOT NULL DEFAULT '',
  `message` text NOT NULL DEFAULT '',
  `customerID` varchar(40) NOT NULL DEFAULT '',
  `timestamp` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `inquiry`
--

INSERT INTO `inquiry` (`id`, `email`, `mobile`, `name`, `message`, `customerID`, `timestamp`) VALUES
(1, 'testing@gmail.com', '9716435404', 'ajay sharma', 'safsf', '', '2020-07-09 09:09:56'),
(2, 'no-reply@hilkom-digi', '85923941759', 'Mike Jackson', 'Hi! \r\n \r\nI have just checked dreamstayvacationrentals.com for the ranking keywords and seen that your SEO metrics could use a boost. \r\n \r\nWe will improve your SEO metrics and ranks organically and safely, using only whitehat methods, while providing monthly reports and outstanding support. \r\n \r\nPlea', '', '2020-07-10 14:29:15'),
(3, 'atrixxtrix@gmail.com', '88452535795', 'HeatherSah', 'Dear Sir/mdm, \r\n \r\nHow are you? \r\n \r\nWe supply Professional surveillance & medical products: \r\n \r\nMoldex, makrite and 3M N95 1860, 9502, 9501, 8210 \r\n3ply medical, KN95, FFP2, FFP3, PPDS masks \r\nFace shield/medical goggles \r\nNitrile/vinyl/PP gloves \r\nIsolation/surgical gown lvl1-4 \r\nProtective PPE/O', '', '2020-07-10 21:22:50'),
(4, 'verajohn@fanclub.pm', '88573595541', 'Leonrad Garcia', 'Hi,  this is Leonrad. \r\n \r\nToday I have good news for you, witch you can get $30 free bonus in a minute. \r\n \r\nAll you have to do is to register Vera & John online casino link below and that\'s it. \r\nYou can register by free e-mail and no need kyc. \r\n \r\nRegistration form \r\nhttps://www3.samuraiclick.co', '', '2020-07-11 07:59:25'),
(5, 'no-replyamamy@google', '86121149491', 'Mike Mansfield\r\n', 'Hi! \r\nIf you want to get ahead of your competition, have a higher Domain Authority score. Its just simple as that. \r\nWith our service you get Domain Authority above 50 points in just 30 days. \r\n \r\nThis service is guaranteed \r\n \r\nFor more information, check our service here \r\nhttps://www.monkeydigita', '', '2020-07-23 11:54:19'),
(6, 'marktomson40@gmail.c', '89323962971', 'MichaelVarve', 'Want to have a fast growing and profitable business without competitors?! \r\nLooking for a new progressive direction in business?! \r\nWant to owe the high profits despite the market situation?! \r\nWe invite you to be a part of our successful Team. Become a dealer in your region.  We are manufacturers o', '', '2020-07-28 11:56:56'),
(7, 'wpdeveloperfiver@gma', '89352137213', 'Simon Schmidt', 'Hi friend! I found your website dreamstayvacationrentals.com in Google. I am highly reputed seller in Fiverr, from Bangladesh. The pandemic has severely affected our online businesses and the reason for this email is simply to inform you that I am willing to work at a very low prices (5$), without w', '', '2020-07-31 09:45:41'),
(8, 'turbomavro@gmail.com', '83341364136', 'Travisreate', 'The international Blockchain project TuRBo \"Maximum Make Money\" \r\nLeader in short-term investing in the cryptocurrency market. \r\nThe leader in payments for the affiliate program. \r\n \r\nThe investment period is 2 days. \r\nMinimum profit is 10%   \r\nDaily payments under the affiliate program. \r\n \r\nRegist', '', '2020-07-31 21:32:55'),
(9, 'daliborharald02@gmai', '87275644717', 'Dalibor Harald', 'Dear \r\n \r\nMy name is Dalibor Harald, Thank you for your time, my company offers project financing/Joint Ventures Partnership and lending services, do you have any projects that require funding/ Joint Ventures Partnership at the moment? We are ready to work with you on a more transparent approach. \r\n', '', '2020-08-06 16:14:15'),
(10, 'no-replyNerry@gmail.', '84571318881', 'Joshuacak', 'H?ll?!  dreamstayvacationrentals.com \r\n \r\nDid y?u kn?w th?t it is p?ssibl? t? s?nd ?pp??l p?rf??tly l?git? \r\nW? t?nd?r ? n?w l?g?l w?y ?f s?nding r?qu?st thr?ugh ??nt??t f?rms. Su?h f?rms ?r? l???t?d ?n m?ny sit?s. \r\nWh?n su?h r?qu?sts ?r? s?nt, n? p?rs?n?l d?t? is us?d, ?nd m?ss?g?s ?r? s?nt t? f?r', '', '2020-08-07 20:22:53'),
(11, 'jimmyscowley@gmail.c', '86262393123', 'Jimmy Scowley', 'Dear Sir/mdm, \r\n \r\nOur company Resinscales is looking for distributors and resellers for its unique product: ready-made tank models from the popular massively multiplayer online game - World of Tanks. \r\n \r\nSuch models are designed for fans of the game WoT and collectors of military models. \r\n \r\nWhat', '', '2020-08-14 05:21:40'),
(12, 'bee.pannell7184@gmai', '85231366434', 'Jameseloma', 'Are you overwhelmed by Google Analytics? \r\nWednesday at 1 PM (Pacific Time) I will teach you how to quickly navigate through Google Analytics to find important information about your audience. \r\nI will show you how to optimize your Analytics data to help make better-informed marketing decisions. \r\nS', '', '2020-08-14 07:22:20'),
(13, 'abelbreath456@gmail.', '84837932331', 'Davidmop', 'Looking for Facebook likes or Instagram followers? \r\nWe can help you. Please visit https://1000-likes.com/ to place your order.', '', '2020-08-21 08:06:42'),
(14, 'no-replyamamy@google', '84275498883', 'Mike ', 'G??d d?y! \r\nIf you want to get ahead of your competition, have a higher Domain Authority score. Its just simple as that. \r\nWith our service you get Domain Authority above 50 points in just 30 days. \r\n \r\nThis service is guaranteed \r\n \r\nFor more information, check our service here \r\nhttps://www.monkey', '', '2020-08-21 12:33:33'),
(15, 'atrixxtrix@gmail.com', '86872918392', 'RobertFut', 'Dear Sir/mdm, \r\n \r\nHow are you? \r\n \r\nWe supply Professional surveillance & medical products: \r\n \r\nMoldex, makrite and 3M N95 1860, 9502, 9501, 8210 \r\n3ply medical, KN95, FFP2, FFP3, PPDS masks \r\nFace shield/medical goggles \r\nNitrile/vinyl/Latex/PP gloves \r\nIsolation/surgical gown lvl1-4 \r\nProtective', '', '2020-08-26 17:26:49'),
(16, 'turbomavro@gmail.com', '82645474689', 'JimmyHar', 'Leader in short-term investing in the cryptocurrency market.   \r\nLeader in payments for the affiliate program.   \r\n \r\n \r\nInvestment program: \r\n \r\nInvestment currency: BTC. \r\nThe investment period is 2 days. \r\nMinimum profit is 10% \r\nThe minimum investment amount is 0.0025 BTC. \r\nThe maximum investme', '', '2020-08-28 10:35:11'),
(17, 'no-replyNerry@gmail.', '88964683573', 'Joshuacak', 'G??d d?y!  dreamstayvacationrentals.com \r\n \r\nDid y?u kn?w th?t it is p?ssibl? t? s?nd busin?ss ?ff?r t?t?lly l?wful? \r\nW? ?ff?r ? n?w m?th?d ?f s?nding l?tt?r thr?ugh f??db??k f?rms. Su?h f?rms ?r? l???t?d ?n m?ny sit?s. \r\nWh?n su?h r?qu?sts ?r? s?nt, n? p?rs?n?l d?t? is us?d, ?nd m?ss?g?s ?r? s?nt ', '', '2020-09-04 01:52:27'),
(18, 'contact1@theonlinepu', '86991417437', 'MarilynFom', 'Hello, we are The Online Publishers (TOP) and want to introduce ourselves to you.  TOP is an established comprehensive global online hub.  We connect clients to expert freelancers in all facets of the world of digital marketing such as writers, journalists, bloggers, authors, advertisers, publishers', '', '2020-09-10 18:26:54'),
(19, 'no-replyamamy@google', '82653322839', 'Mike Marlow\r\n', 'G??d d?y! \r\nIf you want to get ahead of your competition, have a higher Domain Authority score. Its just simple as that. \r\nWith our service you get Domain Authority above 50 points in just 30 days. \r\n \r\nThis service is guaranteed \r\n \r\nFor more information, check our service here \r\nhttps://www.monkey', '', '2020-09-17 23:40:29'),
(20, 'fsuxx1760@gmail.com', '83193495789', 'Bruce Johnson', 'Greetings, \r\n \r\nWe are brokers linked with high profile investors from the Gulf Region/Asia who are willing to; \r\n \r\nFund any company in any current project; \r\nFinancing/loan/Investment Opportunity \r\n \r\nThey are privately seeking means of expanding their investment portfolio. Should you find interes', '', '2020-09-18 04:55:13'),
(21, 'contact2@theonlinepu', '83476577163', 'Teresaagind', 'Grow Your Business with TOP Vlogger Experts \r\n \r\nHow would you like to have your company\'s story told in video clips?  What better way to do that than with a series of short videos?  Welcome to the world of vlogging.  Vlogging is essentially blogging, except it is in the form of videos instead of wr', '', '2020-09-19 04:32:42'),
(22, 'no-reply@hilkom-digi', '89893479831', 'James Keat\r\n', 'Hi there \r\nI have just checked dreamstayvacationrentals.com for the ranking keywords and seen that your SEO metrics could use a boost. \r\n \r\nWe will improve your SEO metrics and ranks organically and safely, using only whitehat methods, while providing monthly reports and outstanding support. \r\n \r\nPl', '', '2020-09-30 09:55:36'),
(23, 'writingbyb@gmail.com', '86277456975', 'Benjamin Ehinger', 'Hi! \r\nDo you struggle to find time to write fresh blog and website content? \r\nI am a highly-skilled blog and web content writer with more than 10 years of experience. Let me provide well-researched, 100 percent unique content specifically for you! \r\nContact Benjamin Today and Discover the Difference', '', '2020-10-07 19:52:23'),
(24, 'david@starkwoodmarke', '85661687619', 'Dave Stills', 'Hey dreamstayvacationrentals.com, \r\n \r\nCan I get you on the horn to discuss relaunching marketing? \r\n \r\nGet started on a conversion focused landing page, an automated Linkedin marketing tool, or add explainer videos to your marketing portfolio and boost your ROI. \r\n \r\nWe also provide graphic design ', '', '2020-10-11 13:19:49'),
(25, 'info@deoleodigitalpu', '82717146949', 'RobertCam', 'HOW TO SAVE YOUR BUSINESS & YOUR LIFE DURING  THIS PANDEMIC? \r\n \r\nWatch the Official Trailer : https://youtu.be/hb7qJmAIb9M \r\n \r\nUse this link below to Download your E-book copy Now! (50%off! )@ https://www.tonydeoleo.com \r\n---------- \r\nAmazon : https://www.amazon.com/gp/aw/d/B08CG7DYKX/ref=tmm_kin_', '', '2020-10-13 10:28:04'),
(26, 'no-replyamamy@google', '87744437154', 'Mike Gerald\r\n', 'Hi there \r\n \r\nIf you want to get ahead of your competition, have a higher Domain Authority score. Its just simple as that. \r\nWith our service you get Domain Authority above 50 points in just 30 days. \r\n \r\nThis service is guaranteed \r\n \r\nFor more information, check our service here \r\nhttps://www.monk', '', '2020-10-13 18:58:31'),
(27, 'mehdi@meloncbd.co', '84913537284', 'Madi Manager', 'Hi there, \r\n \r\nI wanted to reach out to share with you a promotion that might interest you: we’ll transform your website into a high-performance marketing machine that generates sales on autopilot. \r\n \r\nMadi from Auratonic here! We are a digital marketing agency. \r\n \r\nI will keep it short. \r\n \r\nOur ', '', '2020-10-15 08:15:09'),
(28, 'no-reply@hilkom-digi', '86846371115', 'James Owen\r\n', 'G??d d?y! \r\nI have just checked dreamstayvacationrentals.com for the ranking keywords and seen that your SEO metrics could use a boost. \r\n \r\nWe will improve your SEO metrics and ranks organically and safely, using only whitehat methods, while providing monthly reports and outstanding support. \r\n \r\nP', '', '2020-10-20 06:12:53'),
(29, 'linda.ray@theremarke', '89172379727', 'Linda Ray', 'I noticed that you are not running Google Remarketing on your website. \r\n \r\nThe most affordable advertising is marketing to previous web viewers who didn\'t convert. \r\n \r\nRemarketing via Google & Facebook ads means tracking and engaging these \'lost\' customers. \r\nThey were on your site once and may on', '', '2020-10-25 01:25:08'),
(30, 'abelbreath456@gmail.', '89949676463', 'WilliamknovA', 'Looking for Facebook likes or Instagram followers? \r\nWe can help you. Please visit https://1000-likes.com/ to place your order.', '', '2020-10-26 20:16:09'),
(31, 'no-replyNerry@gmail.', '86719872969', 'Mike Flannagan\r\n', 'H?ll?! \r\n \r\nIf you want to get ahead of your competition then you must have a higher Domain Authority score for your website. Its just as simple as that. \r\nWith our service your dreamstayvacationrentals.com DA score will get above 50 points in just 30 days. \r\n \r\nThis service is guaranteed \r\n \r\nFor m', '', '2020-11-10 18:21:29'),
(32, 'no-reply@google.com', '81625533368', 'James Salisburry\r\n', 'Hi! \r\nI have just checked dreamstayvacationrentals.com for the ranking keywords and seen that your SEO metrics could use a boost. \r\n \r\nWe will improve your SEO metrics and ranks organically and safely, using only whitehat methods, while providing monthly reports and outstanding support. \r\n \r\nPlease ', '', '2020-11-17 17:03:09'),
(33, 'jeromesmithiv@gmail.', '3474249302', 'Jerome smith', 'I am inquiring about booking the bayside mansion on December fifth for a photoshoot from the hours of two pm to two am for one hundred dollars per hour.', '', '2020-11-17 22:12:47'),
(34, 'no-replypiodo@gmail.', '86652562965', 'Peter WifKinson\r\n', 'G??d d?y! \r\n \r\nThis is Peter from Speed SEO Digital Agency \r\n \r\nLet us present you our newest addition: \r\nhttps://speed-seo.net/product/ahrefs-dr60/ \r\n \r\nWe all know how important is the ahrefs Domain Rating score, many believing its more important even than the Moz Domain Authority \r\n \r\n-	We are no', '', '2020-11-18 04:23:42'),
(35, 'sammyweiss93@gmail.c', '81293784226', 'Sam Weiss', 'Hi, I stumbled on dreamstayvacationrentals.com yesterday and love the design (I\'ve been making websites since 2005). What platform is it made with? WordPress? \r\n \r\nThe only thing I noticed was that you appeared a bit low on Google search results. \r\n \r\nI manage several eCommerce websites and utilize ', '', '2020-11-20 15:36:34'),
(36, 'no-replyNerry@gmail.', '89843573382', 'Robertfuh', 'Competition not playing the game fair and square? \r\nNow you can fight back. \r\n \r\nNegative SEO, to make ranks go down: \r\nhttps://blackhat.to/', '', '2020-12-01 23:47:49'),
(37, 'stacy@productsupply.', '81175766961', 'Stacy Williams', 'Hi, \r\n \r\nInterested in a No Hassle - Automated Client Thank You Gift Sending Service ? \r\n \r\nHave client or customers that you want to thank for their business? \r\n \r\nLet us do it for you!   Just send us their names, all at once or as they come in. \r\n \r\nWe will pick, pack and ship your client thank yo', '', '2020-12-08 02:50:07'),
(38, 'no-replyNerry@gmail.', '89142681255', 'Mike Taft\r\n', 'G??d d?y! \r\n \r\nDo you want a quick boost in ranks and sales for your dreamstayvacationrentals.com website? \r\nHaving a high DA score, always helps \r\n \r\nGet your dreamstayvacationrentals.com to have a 50+ points in Moz DA with us today and rip the benefits of such a great feat. \r\n \r\nSee our offers her', '', '2020-12-09 06:26:32'),
(39, 'no-replyNerry@gmail.', '85612695267', 'ContactHex', 'Hi!  dreamstayvacationrentals.com \r\n \r\nDid y?u kn?w th?t it is p?ssibl? t? s?nd busin?ss ?ff?r ?ntir?ly l?g?l? \r\nW? pr?s?nt?ti?n ? n?w m?th?d ?f s?nding pr?p?s?l thr?ugh ??nt??t f?rms. Su?h f?rms ?r? l???t?d ?n m?ny sit?s. \r\nWh?n su?h ??mm?r?i?l ?ff?rs ?r? s?nt, n? p?rs?n?l d?t? is us?d, ?nd m?ss?g?', '', '2020-12-11 04:23:50'),
(40, 'miraclesreport@gmail', '83423528496', 'CarolTop', 'Dropshipping business is a method of retail where the store owner never physically holds the products it sells.... You don’t need upfront investments to get the products that you sells.                            Instead, when the store owner sells one of the products it stocks on its website, the s', '', '2020-12-17 19:46:19'),
(41, 'no-replypiodo@gmail.', '85546834749', 'Peter Farmer\r\n', 'Hi! \r\n \r\nCompetition not playing the game fair and square? \r\nNow you can fight back. \r\n \r\nNegative SEO, to make ranks go down: \r\nhttps://blackhat.to/ \r\n \r\nContact us for any queries: \r\nsupport@blackhat.to', '', '2020-12-18 04:44:23'),
(42, 'no-reply@google.com', '82668156589', 'James Jackson\r\n', 'G??d d?y! \r\nI have just checked dreamstayvacationrentals.com for the ranking keywords and seen that your SEO metrics could use a boost. \r\n \r\nWe will improve your SEO metrics and ranks organically and safely, using only whitehat methods, while providing monthly reports and outstanding support. \r\n \r\nP', '', '2020-12-21 20:28:32'),
(43, 'nicolaslamm1978@gmai', '81199542939', 'Nicola Slamm', 'Get more Followers, Likes, Views to all your social media channels instantly. \r\n100% Safe, Real Human (No bots). \r\n \r\n250 Instagram followers @ $3.99: https://store.marketingchoice.com/buy-instagram-followers \r\n100 Facebook page followers @ $3.99: https://store.marketingchoice.com/facebook-page-foll', '', '2020-12-23 05:30:04'),
(44, 'ahmedkirillov5@gmail', '87293796594', 'Shawnanora', '[url=http://zrenieblog.ru/]Detail[/url]:  [url=http://zrenieblog.ru/]http://zrenieblog.ru/[/url]  http://zrenieblog.ru/ <a href=\"http://zrenieblog.ru/\">http://zrenieblog.ru/</a> \r\n?? \r\n?????????????', '', '2020-12-27 01:44:08'),
(45, 'see-email-in-message', '88631486695', 'Mike Wallace\r\n', 'Hi there \r\n \r\nDo you want a quick boost in ranks and sales for your dreamstayvacationrentals.com website? \r\nHaving a high DA score, always helps \r\n \r\nGet your dreamstayvacationrentals.com to have a 50+ points in Moz DA with us today and rip the benefits of such a great feat. \r\n \r\nSee our offers here', '', '2021-01-06 04:27:47');

-- --------------------------------------------------------

--
-- Table structure for table `property`
--

CREATE TABLE `property` (
  `id` int(11) NOT NULL,
  `customerID` varchar(40) NOT NULL DEFAULT '',
  `propertyURL` varchar(150) NOT NULL DEFAULT '',
  `propertyName` varchar(200) NOT NULL DEFAULT '',
  `propertyMob` varchar(100) NOT NULL DEFAULT '',
  `city` varchar(202) NOT NULL DEFAULT '',
  `state` varchar(200) NOT NULL DEFAULT '',
  `country` varchar(200) NOT NULL DEFAULT '',
  `address` varchar(200) NOT NULL DEFAULT '',
  `amenities` varchar(260) NOT NULL DEFAULT '',
  `price` varchar(200) NOT NULL DEFAULT '',
  `near_by` text NOT NULL DEFAULT '',
  `checkIN` varchar(100) NOT NULL DEFAULT '',
  `checkOut` varchar(100) NOT NULL DEFAULT '',
  `lat` varchar(100) NOT NULL DEFAULT '',
  `long` varchar(100) NOT NULL DEFAULT '',
  `airbnb_link` varchar(270) NOT NULL DEFAULT '',
  `propertyInfo` text NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `property`
--

INSERT INTO `property` (`id`, `customerID`, `propertyURL`, `propertyName`, `propertyMob`, `city`, `state`, `country`, `address`, `amenities`, `price`, `near_by`, `checkIN`, `checkOut`, `lat`, `long`, `airbnb_link`, `propertyInfo`, `timestamp`) VALUES
(1, '1', 'bell-charming-quiet-getaway', 'Bell Charming Quiet Getaway for NYC 82%', '+1 (212) 300-5200', 'New York', 'New York', 'US', '82-01 251st Bellerose Ny 11426', '', '16292', '', '3', '10', '40.7553922', '-73.8169579', 'https://www.airbnb.com/rooms/43118134?location=Bellerose%20Floral%20Park%2C%20Queens%2C%20NY%2C%20United%20States&adults=3&check_in=2020-08-02&check_out=2020-08-08&source_impression_id=p3_1591900724_0YwbpkeS4p81bnnf&guests=1', 'Indulge in the serenity of a place designed with peace and tranquility in mind serving as the perfect base for exploring NYC. Offering easy access to the city this hidden gem is suitable for both short and long-term stays. Charming two bedroom townhouse apartment, ideal for families and small groups, combining perfect location and stylish interior.\r\n\r\nThe space\r\nAfter a day of activities in NYC return to our city retreat. Our home is located in a quiet and safe neighborhood located in the exciting eclectic Queens. The area is a foodies dream and an art/music lovers destination of choice. The home offers a perfect base for exploring all of New York City and creating memories to last a lifetime.\r\nThe Space:\r\nOur townhouse has been furnished and designed for the busy traveler in mind. Offering a tranquil retreat from the hustle and bustle of a city vacation, with the convenience of proximity to transport hubs. The specifics of the space are as follows:\r\nBedrooms: Both rooms showcase comfortable beds and plush linens. Guaranteeing a good nights rest. Along with storage space and ample lighting.\r\n\r\nKitchen: The kitchen is fully equipped with the top of the line appliances and utensils. You will find everything you need to cook a full meal or a quick snack. Attached to the kitchen is a quaint dining space that seats four adults.\r\n\r\nBathroom: After a day exploring let the jet tub transport you to your own personal spa experience. If you forgot shampoo, no worries, we fully stock our bathroom with the needed toiletries.', '2020-06-17 07:12:30'),
(2, '1', 'private-heated', 'Private Heated Swimming Pool BBQ , Steam Spa', '+1 (212) 300-5200', 'New York', 'New York', 'US', '123 N King St, Elmont, NY 11003, USA', '', '40720', '', '3', '10', '40.6940141', '-73.7105528', 'https://abnb.me/ES4wYykSd7', 'Make sure you have right amount of guest in inquiries\r\n( For example if 12 people are visiting the property regardless of stay overnight or not your initial inquiry should say 12 people and number of the nights regardless of stay night or not no stop by are allowed ) We use Professional cleaners Treat body and soul in the steam room, spend quality time with friends. Modern urban retreat, perfect for families or small groups, offering great entertainment,\r\n\r\nThe space\r\nWE DO NOT HONOR RESERVATIONS OF PEOPLE WITH BAD REVIEWS\r\n\r\nPLEASE STATE TOTAL NUMBER OF GUESTS VISITING THE PROPERTY AT THE TIME OF INQUIRY/ BOOKING ( NOT ONLY STAYING OVERNIGHT BUT VISITING ALSO). For example, if you have a family gathering and 15 people will be attending, please send the inquiry for 15 people.\r\n\r\nWelcome to this beautiful urban house in a peaceful neighborhood boasting elegant design and homey ambiance. Decorated in subtle good taste this amazing getaway is taken care of by a super host who would love to go the extra mile to cater to your needs. You are in for an outstanding experience and lots of fun at this gorgeous NYC oasis.\r\nWell-kept landscaped garden with fruit trees and plenty of space for outdoor family fun is what greets you first. There is a private gated driveway and a garage with lots of parking space in the yard. Fire up the barbecue and open the marshmallows for a perfect start of an awesome evening or just kick back on the lounge chairs on the porch with a cooling drink on a warm night.', '2020-06-17 07:12:26'),
(3, '1', 'cozy-city-retreat', 'Cozy City Retreat 25 % OFF', '+1 (212) 300-5200', 'New York', 'New York', 'US', '147-12 45th Ave Flushing Ny 11355', '', '16292', '', '3', '10', '40.778766', '-73.7739867', 'https://www.airbnb.com/rooms/42915641?location=Flushing%2C%20Queens%2C%20NY%2C%20United%20States&adults=3&check_in=2020-08-02&check_out=2020-08-08&source_impression_id=p3_1591900724_0YwbpkeS4p81bnnf&guests=1', 'Indulge in the serenity of a place designed with peace and tranquility in mind serving as the perfect base for exploring NYC. Offering easy access to the city this hidden gem is suitable for both short and long-term stays. Charming two bedroom townhouse apartment, ideal for families and small groups, combining perfect location and stylish interior.\r\n\r\nThe space\r\nAfter a day of activities in NYC return to our city retreat. Our home is located in a quiet and safe neighborhood located in the exciting eclectic Queens. The area is a foodies dream and an art/music lovers destination of choice. The home offers a perfect base for exploring all of New York City and creating memories to last a lifetime.\r\nThe Space:\r\nOur townhouse has been furnished and designed for the busy traveler in mind. Offering a tranquil retreat from the hustle and bustle of a city vacation, with the convenience of proximity to transport hubs. The specifics of the space are as follows:\r\nBedrooms: Both rooms showcase comfortable beds and plush linens. Guaranteeing a good nights rest. Along with storage space and ample lighting.', '2020-06-17 07:12:20'),
(4, '1', 'dream-mansion', 'Mansion bayside · DREAM Mansion ,hot Pool, SteamSP...', '+1 (212) 300-5200', 'New York', 'New York', 'US', '215-40 27th Ave, Flushing, NY 11360, USA', '', '16292', '', '4', '10', '40.778766', '-73.7739867', ' https://www.airbnb.com/rooms/43098357?preview_for_ml=true&source_impression_id=p3_1591899586_lfnMnrGKE1Egq0k8&guests=1&adults=1', 'Indulge in the serenity of a place designed with peace and tranquility in mind serving as the perfect base for exploring NYC. Offering easy access to the city this hidden gem is suitable for both short and long-term stays. Charming two bedroom townhouse apartment, ideal for families and small groups, combining perfect location and stylish interior.\r\n\r\nThe space\r\nAfter a day of activities in NYC return to our city retreat. Our home is located in a quiet and safe neighborhood located in the exciting eclectic Queens. The area is a foodies dream and an art/music lovers destination of choice. The home offers a perfect base for exploring all of New York City and creating memories to last a lifetime.\r\nThe Space:\r\nOur townhouse has been furnished and designed for the busy traveler in mind. Offering a tranquil retreat from the hustle and bustle of a city vacation, with the convenience of proximity to transport hubs. The specifics of the space are as follows:\r\nBedrooms: Both rooms showcase comfortable beds and plush linens. Guaranteeing a good nights rest. Along with storage space and ample lighting.', '2020-06-17 07:12:15');

-- --------------------------------------------------------

--
-- Table structure for table `rooms`
--

CREATE TABLE `rooms` (
  `id` int(11) NOT NULL,
  `customerID` varchar(200) NOT NULL DEFAULT '',
  `propertyID` varchar(200) NOT NULL DEFAULT '',
  `roomName` varchar(200) NOT NULL DEFAULT '',
  `roomPrice` varchar(200) NOT NULL DEFAULT '',
  `description` text NOT NULL,
  `amenities` varchar(100) NOT NULL DEFAULT '',
  `size` varchar(200) NOT NULL DEFAULT '',
  `roomURL` varchar(100) NOT NULL DEFAULT '',
  `timestamp` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `slider`
--

CREATE TABLE `slider` (
  `id` int(11) NOT NULL,
  `customerID` varchar(40) NOT NULL DEFAULT '',
  `img` varchar(100) NOT NULL DEFAULT '',
  `heading` varchar(600) NOT NULL DEFAULT '',
  `timestamp` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `slider`
--

INSERT INTO `slider` (`id`, `customerID`, `img`, `heading`, `timestamp`) VALUES
(2, '1', '2020-06-12_10-51-39', 'Welcome to Dream Stay Vacation Rental', '2020-06-12 08:52:33'),
(3, '1', '2020-06-12_10-58-59', 'Welcome to Dream Stay Vacation Rental', '2020-06-12 08:59:42'),
(4, '1', '2020-06-14_05-10-10', '', '2020-06-14 09:10:12'),
(6, '1', '2020-06-17_08-34-43', 'Welcome to Dream Stay Vacation Rental', '2020-06-17 12:34:44');

-- --------------------------------------------------------

--
-- Table structure for table `testimonial`
--

CREATE TABLE `testimonial` (
  `id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL DEFAULT '',
  `content` text DEFAULT NULL,
  `img` varchar(260) NOT NULL DEFAULT '',
  `timestamp` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `testimonial`
--

INSERT INTO `testimonial` (`id`, `name`, `content`, `img`, `timestamp`) VALUES
(1, 'Gregory', 'I had a wonderful stay at this beautiful home with my and my significant other. When checking in It was very smooth transition. The accommodations were comfortable and private. Shujaat was very helpful in assisting anything we needed. This home is located in a quiet, well-kept neighborhood which is easily accessible by private or public transport. The house itself is brightly lit with natural light, spacious, Very clean, modernized ,neat and true to the pictures you see. I didn\'t have to worry about parking because the driveway is big enough for multiple cars lol. Shujaat was very generous ,friendly and humble. These simple human gestures like these go a long way to making the experience not just pleasant but memorable. My stay exemplified that great ideal of the hospitality industry: come as a stranger, leave as a friend. I\'ll Be Staying here again :)', 'Gregory_ilewq3', '2020-06-16 09:41:22'),
(2, 'Marc', 'The mansion is really nice and custom built with some world class finishings! Interacting with the host was easy and smooth ... and we had everything we needed! We would definitely return to the Mansion for another getaway in a very convenient location!!', 'Marc_bbbzmq', '2020-06-16 09:40:43'),
(3, 'Javonnie', 'Great host. The place was even more beautiful in person. So many cool touches and super comfortable. I would love to come back.', 'Javonnie_jyi4vz', '2020-06-16 09:41:40'),
(4, 'Tammy', 'I think the pool was everyone\'s favorite amenity. The most helpful amenity was the large parking area enclosed with fence. The house was very clean, BIG with plenty of space for everyone to spread out, and in a nice neighborhood. We had issue with the wifi when we arrived, but he was quick to respond and send someone right over to fix it.', 'Tammy_mb9uxg', '2020-06-16 09:41:57'),
(5, 'Gonzalo', 'This is definitely the best Airbnb I\'ve ever rented! It is spacious, everything you need is there! Bling BLING There are many rooms and everyone from our group (we were 7) was able to have their own private space. The sauna, the gym, and the massage chair are amazing, of course. We had a great time sitting outside next to the waterfall and the play basketball BBQ all the Time You have all the necessary things, by example, there is everything you need to make some food. We felt comfortable as soon as we got in and the Host was amazing. He is a great host, replies fast and makes sure everything is ok while you are there. Finally, the place is great too: it is a quiet neighborhood and it is quite easy to go to big places in New York like Times Square or Soho. I only have good comments about the place!', 'Gonzalo_xdwssg', '2020-06-16 09:40:13'),
(6, 'Nazhat Shirin', 'Good location and suitable for couples or small families.', 'Nazhat_hs18db', '2020-06-16 09:41:03');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cloudnary`
--
ALTER TABLE `cloudnary`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `home_page`
--
ALTER TABLE `home_page`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `images`
--
ALTER TABLE `images`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `inquiry`
--
ALTER TABLE `inquiry`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `property`
--
ALTER TABLE `property`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `rooms`
--
ALTER TABLE `rooms`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `slider`
--
ALTER TABLE `slider`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `testimonial`
--
ALTER TABLE `testimonial`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `cloudnary`
--
ALTER TABLE `cloudnary`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `home_page`
--
ALTER TABLE `home_page`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `images`
--
ALTER TABLE `images`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=50;

--
-- AUTO_INCREMENT for table `inquiry`
--
ALTER TABLE `inquiry`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;

--
-- AUTO_INCREMENT for table `property`
--
ALTER TABLE `property`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `rooms`
--
ALTER TABLE `rooms`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `slider`
--
ALTER TABLE `slider`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `testimonial`
--
ALTER TABLE `testimonial`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
